-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-03-2017 a las 20:00:05
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema-evaluacion-3.0`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compromisos`
--

CREATE TABLE IF NOT EXISTS `compromisos` (
`ID_COMPROMISO` int(255) NOT NULL,
  `ID_META` int(255) NOT NULL,
  `COMPROMISO` text COLLATE utf8_bin NOT NULL,
  `FECHA_COMP` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=41 ;

--
-- Volcado de datos para la tabla `compromisos`
--

INSERT INTO `compromisos` (`ID_COMPROMISO`, `ID_META`, `COMPROMISO`, `FECHA_COMP`) VALUES
(25, 12, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis p', '2017-02-26'),
(26, 12, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis p', '2017-02-28'),
(27, 12, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis p', '2017-02-28'),
(33, 18, 'Muy lejos, mÃ¡s allÃ¡ de las montaÃ±as de palabras, alejados de los paÃ­ses de las vocales y las consonantes, viven los textos simulados. Viven aislados e', '2017-02-28'),
(34, 18, 'Muy lejos, mÃ¡s allÃ¡ de las montaÃ±as de palabras, alejados de los paÃ­ses de las vocales y las consonantes, viven los textos simulados. Viven aislados e', '2017-02-28'),
(35, 19, 'Y, viÃ©ndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: SÃ¡bete, Sancho, que no es un hombre mÃ¡s que otro si no hace mÃ¡s qu', '2017-02-28'),
(36, 20, 'Y, viÃ©ndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: SÃ¡bete, Sancho, que no es un hombre mÃ¡s que otro si no hace mÃ¡s qu', '2017-02-28'),
(37, 20, 'Y, viÃ©ndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: SÃ¡bete, Sancho, que no es un hombre mÃ¡s que otro si no hace mÃ¡s qu', '2017-02-28'),
(38, 20, 'Y, viÃ©ndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: SÃ¡bete, Sancho, que no es un hombre mÃ¡s que otro si no hace mÃ¡s qu', '2017-02-28'),
(39, 21, 'segundo compromiso', '2017-03-04'),
(40, 22, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, ', '2017-03-31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuestionarios`
--

CREATE TABLE IF NOT EXISTS `cuestionarios` (
`ID_CUESTIONARIO` int(255) NOT NULL,
  `NOMBRE_CUES` varchar(200) COLLATE utf8_bin NOT NULL,
  `ESTADO_CUES` tinyint(1) NOT NULL,
  `FECHA_ING` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cuestionarios`
--

INSERT INTO `cuestionarios` (`ID_CUESTIONARIO`, `NOMBRE_CUES`, `ESTADO_CUES`, `FECHA_ING`) VALUES
(2, 'Evaluacion De DesempeÃ±o', 1, '2017-02-19'),
(3, 'Evaluacion De Competencias', 1, '2017-02-19'),
(4, 'Cuestionario Prueba', 0, '2017-02-19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE IF NOT EXISTS `departamentos` (
`ID_DEPARTAMENTO` int(255) NOT NULL,
  `DEPARTAMENTO` varchar(150) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`ID_DEPARTAMENTO`, `DEPARTAMENTO`) VALUES
(1, 'Direccion General'),
(2, 'Gerencia de Comercializacion'),
(3, 'Gerencia de AdministraciÃ³n y Finanzas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE IF NOT EXISTS `empleados` (
  `NO_EMPLEADO` varchar(10) COLLATE utf8_bin NOT NULL,
  `ID_PUESTO` int(255) NOT NULL,
  `NOMBRE_EMP` varchar(75) COLLATE utf8_bin NOT NULL,
  `APELLIDO_P` varchar(50) COLLATE utf8_bin NOT NULL,
  `APELLIDO_M` varchar(50) COLLATE utf8_bin NOT NULL,
  `RFC` varchar(13) COLLATE utf8_bin NOT NULL,
  `CURP` varchar(19) COLLATE utf8_bin NOT NULL,
  `FECHA_INGRESO` date NOT NULL,
  `CORREO` varchar(150) COLLATE utf8_bin NOT NULL,
  `TIPO_EMP` varchar(1) COLLATE utf8_bin NOT NULL,
  `ESTADO_EMP` varchar(1) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`NO_EMPLEADO`, `ID_PUESTO`, `NOMBRE_EMP`, `APELLIDO_P`, `APELLIDO_M`, `RFC`, `CURP`, `FECHA_INGRESO`, `CORREO`, `TIPO_EMP`, `ESTADO_EMP`) VALUES
('123456', 1, 'Oliver', 'Aparicio', 'Martinez', '1234567891234', '1234567891234567891', '2017-02-17', 'asdfa@sadkjfhjksa', '1', '0'),
('654321', 2, 'Luis Alfonso', 'Baeza', 'Sotelo', '1234567891234', '123456789123456789P', '2017-02-02', 'asdfa@sadkjfhjksa', '1', '1'),
('741852', 1, 'Israel', 'Lopez', 'Lujan', '0987654321123', '0987654321123456789', '2017-02-21', 'asdfa@sadkjfhjksa', '1', '0'),
('852741', 5, 'Prueba', 'Prueba', 'Pruebas', '0987654321124', '0987654321123456780', '2017-02-08', 'asdfa@sadkjfhjksa', '0', '0'),
('852963', 5, 'Prueba', 'Prueba', 'Prueba', 'QWERTYUIOPOIU', 'QWERTYUIOPOIUYTREWQ', '2017-02-21', 'asdfa@sadkjfhjksa', '0', '0'),
('963852', 5, 'Juana Itzel', 'Bengochea', 'Perez', 'ABCDEFGHI1234', 'ABCDEFGHI123457891A', '2017-02-16', 'asdfas@jdas', '0', '1'),
('987654', 3, 'Juana', 'Perez', 'Perez', '1234567891234', '12345678891QWERTYUI', '2017-02-03', 'asdfa@sadkjfhjksa', '1', '1'),
('poiuyt', 4, 'Luis Alfonso', 'Baeza', 'Sotello', 'ZXCVBNMASDFGH', 'ZXCVBNMASDFGHJKLPOI', '2017-02-16', 'asdfa@sadkjfhjksa', '1', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluaciones`
--

CREATE TABLE IF NOT EXISTS `evaluaciones` (
`ID_EVALUACION` int(255) NOT NULL,
  `NOMBRE_EVA` varchar(200) COLLATE utf8_bin NOT NULL,
  `FECHA_INICIO` date NOT NULL,
  `FECHA_FIN` date NOT NULL,
  `CUESTIONARIO_M` int(255) NOT NULL,
  `CUESTIONARIO_O` int(255) NOT NULL,
  `ESTADO_EVA` varchar(1) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `evaluaciones`
--

INSERT INTO `evaluaciones` (`ID_EVALUACION`, `NOMBRE_EVA`, `FECHA_INICIO`, `FECHA_FIN`, `CUESTIONARIO_M`, `CUESTIONARIO_O`, `ESTADO_EVA`) VALUES
(2, 'Evaluacion Prueba', '2017-01-25', '2017-01-31', 2, 2, '3'),
(4, 'Juan', '2017-02-27', '2017-02-28', 2, 2, '3'),
(7, 'Evaluacion la buena onda', '2017-02-22', '2017-02-23', 3, 2, '3'),
(8, 'Soy una super evaluacion', '2017-03-17', '2017-03-18', 3, 3, '0'),
(9, 'Soy un nombre muy largo para una evaluacion que nose que pedo', '2017-03-30', '2017-03-31', 2, 2, '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metas`
--

CREATE TABLE IF NOT EXISTS `metas` (
`ID_META` int(255) NOT NULL,
  `NO_EMPLEADO` varchar(10) COLLATE utf8_bin NOT NULL,
  `META` text COLLATE utf8_bin NOT NULL,
  `FECHA_INTRO` date NOT NULL,
  `ESTADO_META` varchar(1) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=23 ;

--
-- Volcado de datos para la tabla `metas`
--

INSERT INTO `metas` (`ID_META`, `NO_EMPLEADO`, `META`, `FECHA_INTRO`, `ESTADO_META`) VALUES
(12, '654321', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec', '2017-02-23', '1'),
(18, '654321', 'Muy lejos, mÃ¡s allÃ¡ de las montaÃ±as de palabras, alejados de los paÃ­ses de las vocales y las consonantes, viven los textos simulados. Viven aislados en casas de letras, en la costa de la semÃ¡ntica, un gran ocÃ©ano de lenguas. Un riachuelo llamado Pons fluye por su pueblo y los abastece con las normas', '2017-02-23', '1'),
(19, '654321', 'Y, viÃ©ndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: SÃ¡bete, Sancho, que no es un hombre mÃ¡s que otro si no hace mÃ¡s que otro. Todas estas borrascas que nos suceden son seÃ±ales de que presto ha de serenar el tiempo y han de sucedernos bien las cosas; porque no es posib', '2017-02-23', '1'),
(20, '654321', 'Reina en mi espÃ­ritu una alegrÃ­a admirable, muy parecida a las dulces alboradas de la primavera, de que gozo aquÃ­ con delicia. Estoy solo, y me felicito de vivir en este paÃ­s, el mÃ¡s a propÃ³sito para almas como la mÃ­a, soy tan dichoso, mi querido amigo, me sojuzga de tal modo la idea de reposar, que', '2017-02-23', '1'),
(21, '123456', 'primera meta', '2017-03-01', '0'),
(22, '654321', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.', '2017-03-06', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE IF NOT EXISTS `preguntas` (
`ID_PREGUNTA` int(255) NOT NULL,
  `ID_CUESTIONARIO` int(255) NOT NULL,
  `PREGUNTA_CUES` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`ID_PREGUNTA`, `ID_CUESTIONARIO`, `PREGUNTA_CUES`) VALUES
(3, 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. aenean commodo ligula eget dolor. aenean massa. cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. nulla consequat massa quis enim. donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. in enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. nullam dictum felis eu pede mollis pretium. integer tincidunt. cras dapibu'),
(4, 2, 'Y, viÃ©ndole don Quijote de aquella manera, con muestras de tanta tristeza, le dijo: SÃ¡bete, Sancho, que no es un hombre mÃ¡s que otro si no hace mÃ¡s que otro. Todas estas borrascas que nos suceden son seÃ±ales de que presto ha de serenar el tiempo y han de sucedernos bien las cosas; porque no es posible que el mal ni el bien sean durables, y de aquÃ­ se sigue que, habiendo durado mucho el mal, el bien estÃ¡ ya cerca. AsÃ­ que, no debes congojarte por las desgracias que a mÃ­ me suceden, pues a ti no t'),
(5, 2, 'Una maÃ±ana, tras un sueÃ±o intranquilo, Gregorio Samsa se despertÃ³ convertido en un monstruoso insecto. Estaba echado de espaldas sobre un duro caparazÃ³n y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo. Numerosas patas, penosamente delgadas en comparaciÃ³n con el grosor normal de sus piernas, se agitaban sin concierto. - Â¿QuÃ© me ha ocurrido? No estaba soÃ±ando. Su.'),
(6, 3, 'Una maÃ±ana, tras un sueÃ±o intranquilo, Gregorio Samsa se despertÃ³ convertido en un monstruoso insecto. Estaba echado de espaldas sobre un duro caparazÃ³n y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo. Numerosas patas, penosamente delgadas en comparaciÃ³n con el grosor normal de sus piernas, se agitaban sin concierto. - Â¿QuÃ© me ha ocurrido? No estaba soÃ±ando. Su.'),
(7, 3, 'Quiere la boca exhausta vid, kiwi, piÃ±a y fugaz jamÃ³n. Fabio me exige, sin tapujos, que aÃ±ada cerveza al whisky. Jovencillo emponzoÃ±ado de whisky, Â¡quÃ© figurota exhibes! La cigÃ¼eÃ±a tocaba cada vez mejor el saxofÃ³n y el bÃºho pedÃ­a kiwi y queso. El jefe buscÃ³ el Ã©xtasis en un imprevisto baÃ±o de whisky y gozÃ³ como un duque. ExhÃ­banse politiquillos zafios, con orejas kilomÃ©tricas y uÃ±as de gavilÃ¡n. El cadÃ¡ver de Wamba, rey godo de EspaÃ±a, fue exhumado y trasladado en una caja de zinc que pesÃ³ un kil'),
(9, 4, 'segunda pregunta vacia'),
(10, 4, 'tercera pregunta vacia'),
(11, 4, 'cuarta pregunta vacia'),
(12, 4, 'quinta pregunta vacia'),
(13, 4, 'sexta pregunta vacia'),
(14, 4, 'septima pregunta vacia'),
(15, 4, 'octava pregunta vacia'),
(16, 4, 'novena pregunta vacia'),
(18, 4, 'decima pregutna vacia'),
(19, 4, 'PRIMERA PREGUNTA VACIA'),
(20, 2, 'Hola');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puestos`
--

CREATE TABLE IF NOT EXISTS `puestos` (
`ID_PUESTO` int(255) NOT NULL,
  `ID_DEPARTAMENTO` int(255) NOT NULL,
  `PUESTO` varchar(150) COLLATE utf8_bin NOT NULL,
  `NIVEL_MANDO` int(20) NOT NULL,
  `PUESTO_SUP` varchar(150) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `puestos`
--

INSERT INTO `puestos` (`ID_PUESTO`, `ID_DEPARTAMENTO`, `PUESTO`, `NIVEL_MANDO`, `PUESTO_SUP`) VALUES
(1, 1, 'Director General', 1, 'Ninguno'),
(2, 1, 'Subgerente de Informatica', 3, 'Director General'),
(3, 2, 'Gerente de Comercializacion', 2, 'Director General'),
(4, 3, 'Gerente de AdministraciÃ³n y Finanzas', 1, 'Director General'),
(5, 1, 'Secretaria Ejecutiva BilingÃœe De Director General', 0, 'Director General');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas_metas`
--

CREATE TABLE IF NOT EXISTS `respuestas_metas` (
  `ID_RESULTADO_META` int(255) NOT NULL,
  `ID_META` int(255) NOT NULL,
  `CALIF_META` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `respuestas_metas`
--

INSERT INTO `respuestas_metas` (`ID_RESULTADO_META`, `ID_META`, `CALIF_META`) VALUES
(5, 12, 5),
(5, 18, 4),
(5, 19, 3),
(5, 20, 2),
(7, 22, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultados_cues`
--

CREATE TABLE IF NOT EXISTS `resultados_cues` (
`ID_RESULTADO_CUES` int(255) NOT NULL,
  `NO_EMPLEADO` varchar(10) COLLATE utf8_bin NOT NULL,
  `ID_EVALUACION` int(255) NOT NULL,
  `ID_CUESTIONARIO` int(255) NOT NULL,
  `ID_PUESTO` int(255) NOT NULL,
  `CALIF_CUES` double DEFAULT NULL,
  `FECHA_REA_CUES` date NOT NULL,
  `NO_EMP_EVA` varchar(10) COLLATE utf8_bin NOT NULL,
  `FECHA_CORREC` date DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `resultados_cues`
--

INSERT INTO `resultados_cues` (`ID_RESULTADO_CUES`, `NO_EMPLEADO`, `ID_EVALUACION`, `ID_CUESTIONARIO`, `ID_PUESTO`, `CALIF_CUES`, `FECHA_REA_CUES`, `NO_EMP_EVA`, `FECHA_CORREC`) VALUES
(20, '963852', 7, 2, 5, 7, '2017-02-23', '123456', NULL),
(21, '987654', 7, 3, 3, 3.5, '2017-02-23', '123456', NULL),
(22, '654321', 7, 3, 2, 8.5, '2017-02-23', '123456', NULL),
(23, '654321', 2, 2, 2, 7.25, '2017-03-06', '123456', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultados_metas`
--

CREATE TABLE IF NOT EXISTS `resultados_metas` (
`ID_RESULTADO_META` int(255) NOT NULL,
  `ID_EVALUACION` int(255) NOT NULL,
  `NO_EMPLEADO` varchar(10) COLLATE utf8_bin NOT NULL,
  `ID_PUESTO` int(255) NOT NULL,
  `NO_EMP_EVA` varchar(10) COLLATE utf8_bin NOT NULL,
  `CALIF_META` double DEFAULT NULL,
  `COMENTARIO_META` text COLLATE utf8_bin NOT NULL,
  `FECHA_CALIF` date NOT NULL,
  `FECHA_CORREC` date DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `resultados_metas`
--

INSERT INTO `resultados_metas` (`ID_RESULTADO_META`, `ID_EVALUACION`, `NO_EMPLEADO`, `ID_PUESTO`, `NO_EMP_EVA`, `CALIF_META`, `COMENTARIO_META`, `FECHA_CALIF`, `FECHA_CORREC`) VALUES
(5, 7, '654321', 2, '123456', 3.5, 'AQUI VALIENDO VERGA', '2017-02-23', NULL),
(6, 7, '987654', 3, '123456', 0, 'fsafasdf', '2017-02-24', NULL),
(7, 2, '654321', 2, '123456', 5, 'aqui haciendo la prueba', '2017-03-06', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultados_preguntas`
--

CREATE TABLE IF NOT EXISTS `resultados_preguntas` (
  `ID_RESULTADO_CUES` int(255) NOT NULL,
  `ID_PREGUNTA` int(255) NOT NULL,
  `RESULTADO` varchar(150) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `resultados_preguntas`
--

INSERT INTO `resultados_preguntas` (`ID_RESULTADO_CUES`, `ID_PREGUNTA`, `RESULTADO`) VALUES
(20, 3, '10'),
(20, 4, '6'),
(20, 5, '4'),
(20, 20, '8'),
(21, 6, '5'),
(21, 7, '2'),
(22, 6, '10'),
(22, 7, '7'),
(23, 3, '10'),
(23, 4, '7'),
(23, 5, '7'),
(23, 20, '5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `NO_EMPLEADO` varchar(10) COLLATE utf8_bin NOT NULL,
  `USUARIO` varchar(150) COLLATE utf8_bin NOT NULL,
  `PASSWORD` varchar(150) COLLATE utf8_bin NOT NULL,
  `ESTADO` varchar(1) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`NO_EMPLEADO`, `USUARIO`, `PASSWORD`, `ESTADO`) VALUES
('123456', 'prueba', '$2y$10$zxhP.Std9r8ALQa.2qZpheIFFd144JozDDvx2F10DwHOj4kuJaHnO', '1'),
('654321', 'prueba2', '$2y$10$5BgzOylXyr0QPv59geAIseAk96tGS1ewD.q/YgHDF9g8ZqZEvTkLS', '1'),
('987654', 'prueba3', '$2y$10$XWPAW0Cg9zp/p3FfzaJwYOx7UK1vuAt/YH6fB.8dkbJCaC0UsIxrq', '1'),
('963852', 'prueba4', '$2y$10$oaP2P5igQgDMZUgAVentk.tOlA6rWLT76dXjH9sbMS4L5UdDMxKuW', '1'),
('741852', 'prueba5', '$2y$10$uyLVVSOsaz5kx9EpvZh93eMIFHsUmQnyQ9PYbOIHk/NdQWhI1ZmCW', '0'),
('852741', 'prueba6', '$2y$10$wN6K7G3.lcZqcFJp1VyfQOT8c2UoVHtuiZK3az6hp0ECuFIQO9OtS', '1'),
('852963', 'prueba7', '$2y$10$YTMbp0UV/axBVITDszYWbOYbuZrX50FSkgcLJKhkqio5ZOoVIJGwy', '0'),
('poiuyt', 'prueba8', '$2y$10$Qs1j2uJ4UzjsrYsgOfWStOVLc81ekjArXHdzkrd0rbVKgvm.bWcIS', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_admin`
--

CREATE TABLE IF NOT EXISTS `usuarios_admin` (
  `USUARIO` varchar(100) COLLATE utf8_bin NOT NULL,
  `PASSWORD` varchar(150) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `usuarios_admin`
--

INSERT INTO `usuarios_admin` (`USUARIO`, `PASSWORD`) VALUES
('ADMIN', 'ADMINISTRADOR');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `compromisos`
--
ALTER TABLE `compromisos`
 ADD PRIMARY KEY (`ID_COMPROMISO`), ADD KEY `ID_META` (`ID_META`);

--
-- Indices de la tabla `cuestionarios`
--
ALTER TABLE `cuestionarios`
 ADD PRIMARY KEY (`ID_CUESTIONARIO`), ADD KEY `ID_CUESTIONARIO` (`ID_CUESTIONARIO`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
 ADD PRIMARY KEY (`ID_DEPARTAMENTO`), ADD KEY `ID_DEPARTAMENTO` (`ID_DEPARTAMENTO`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
 ADD PRIMARY KEY (`NO_EMPLEADO`), ADD UNIQUE KEY `RFC` (`RFC`,`CURP`), ADD KEY `NO_EMPLEADO` (`NO_EMPLEADO`), ADD KEY `ID_PUESTO` (`ID_PUESTO`), ADD KEY `RFC_2` (`RFC`), ADD KEY `CURP` (`CURP`), ADD KEY `RFC_3` (`RFC`), ADD KEY `CURP_2` (`CURP`);

--
-- Indices de la tabla `evaluaciones`
--
ALTER TABLE `evaluaciones`
 ADD PRIMARY KEY (`ID_EVALUACION`), ADD KEY `ID_EVALUACION` (`ID_EVALUACION`), ADD KEY `NOMBRE_EVA` (`NOMBRE_EVA`), ADD KEY `CUESTIONARIO_M` (`CUESTIONARIO_M`), ADD KEY `CUESTIONARIO_O` (`CUESTIONARIO_O`);

--
-- Indices de la tabla `metas`
--
ALTER TABLE `metas`
 ADD PRIMARY KEY (`ID_META`), ADD KEY `ID_META` (`ID_META`), ADD KEY `NO_EMPLEADO` (`NO_EMPLEADO`);

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
 ADD PRIMARY KEY (`ID_PREGUNTA`), ADD KEY `ID_PREGUNTA` (`ID_PREGUNTA`), ADD KEY `ID_CUESTIONARIO` (`ID_CUESTIONARIO`);

--
-- Indices de la tabla `puestos`
--
ALTER TABLE `puestos`
 ADD PRIMARY KEY (`ID_PUESTO`), ADD KEY `ID_PUESTO` (`ID_PUESTO`), ADD KEY `ID_DEPARTAMENTO` (`ID_DEPARTAMENTO`);

--
-- Indices de la tabla `respuestas_metas`
--
ALTER TABLE `respuestas_metas`
 ADD KEY `ID_RESULTADO_META` (`ID_RESULTADO_META`), ADD KEY `ID_META` (`ID_META`);

--
-- Indices de la tabla `resultados_cues`
--
ALTER TABLE `resultados_cues`
 ADD PRIMARY KEY (`ID_RESULTADO_CUES`), ADD KEY `ID_RESULTADO_CUES` (`ID_RESULTADO_CUES`), ADD KEY `NO_EMPLEADO` (`NO_EMPLEADO`), ADD KEY `ID_EVALUACION` (`ID_EVALUACION`), ADD KEY `ID_CUESTIONARIO` (`ID_CUESTIONARIO`), ADD KEY `ID_PUESTO` (`ID_PUESTO`), ADD KEY `NO_EMP_EVA` (`NO_EMP_EVA`);

--
-- Indices de la tabla `resultados_metas`
--
ALTER TABLE `resultados_metas`
 ADD PRIMARY KEY (`ID_RESULTADO_META`), ADD KEY `ID_RESULTADO_META` (`ID_RESULTADO_META`), ADD KEY `ID_EVALUACION` (`ID_EVALUACION`), ADD KEY `NO_EMPLEADO` (`NO_EMPLEADO`), ADD KEY `ID_PUESTO` (`ID_PUESTO`), ADD KEY `NO_EMP_EVA` (`NO_EMP_EVA`);

--
-- Indices de la tabla `resultados_preguntas`
--
ALTER TABLE `resultados_preguntas`
 ADD KEY `ID_RESULTADO_CUES` (`ID_RESULTADO_CUES`), ADD KEY `ID_PREGUNTA` (`ID_PREGUNTA`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD UNIQUE KEY `USUARIO` (`USUARIO`), ADD KEY `NO_EMPLEADO` (`NO_EMPLEADO`);

--
-- Indices de la tabla `usuarios_admin`
--
ALTER TABLE `usuarios_admin`
 ADD PRIMARY KEY (`USUARIO`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `compromisos`
--
ALTER TABLE `compromisos`
MODIFY `ID_COMPROMISO` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `cuestionarios`
--
ALTER TABLE `cuestionarios`
MODIFY `ID_CUESTIONARIO` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
MODIFY `ID_DEPARTAMENTO` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `evaluaciones`
--
ALTER TABLE `evaluaciones`
MODIFY `ID_EVALUACION` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `metas`
--
ALTER TABLE `metas`
MODIFY `ID_META` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `preguntas`
--
ALTER TABLE `preguntas`
MODIFY `ID_PREGUNTA` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `puestos`
--
ALTER TABLE `puestos`
MODIFY `ID_PUESTO` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `resultados_cues`
--
ALTER TABLE `resultados_cues`
MODIFY `ID_RESULTADO_CUES` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `resultados_metas`
--
ALTER TABLE `resultados_metas`
MODIFY `ID_RESULTADO_META` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compromisos`
--
ALTER TABLE `compromisos`
ADD CONSTRAINT `compromisos_ibfk_1` FOREIGN KEY (`ID_META`) REFERENCES `metas` (`ID_META`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `empleados`
--
ALTER TABLE `empleados`
ADD CONSTRAINT `empleados_ibfk_1` FOREIGN KEY (`ID_PUESTO`) REFERENCES `puestos` (`ID_PUESTO`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `evaluaciones`
--
ALTER TABLE `evaluaciones`
ADD CONSTRAINT `evaluaciones_ibfk_1` FOREIGN KEY (`CUESTIONARIO_M`) REFERENCES `cuestionarios` (`ID_CUESTIONARIO`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `evaluaciones_ibfk_2` FOREIGN KEY (`CUESTIONARIO_O`) REFERENCES `cuestionarios` (`ID_CUESTIONARIO`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `metas`
--
ALTER TABLE `metas`
ADD CONSTRAINT `metas_ibfk_1` FOREIGN KEY (`NO_EMPLEADO`) REFERENCES `empleados` (`NO_EMPLEADO`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `preguntas`
--
ALTER TABLE `preguntas`
ADD CONSTRAINT `preguntas_ibfk_1` FOREIGN KEY (`ID_CUESTIONARIO`) REFERENCES `cuestionarios` (`ID_CUESTIONARIO`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `puestos`
--
ALTER TABLE `puestos`
ADD CONSTRAINT `puestos_ibfk_1` FOREIGN KEY (`ID_DEPARTAMENTO`) REFERENCES `departamentos` (`ID_DEPARTAMENTO`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `respuestas_metas`
--
ALTER TABLE `respuestas_metas`
ADD CONSTRAINT `respuestas_metas_ibfk_1` FOREIGN KEY (`ID_RESULTADO_META`) REFERENCES `resultados_metas` (`ID_RESULTADO_META`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `respuestas_metas_ibfk_2` FOREIGN KEY (`ID_META`) REFERENCES `metas` (`ID_META`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `resultados_cues`
--
ALTER TABLE `resultados_cues`
ADD CONSTRAINT `resultados_cues_ibfk_1` FOREIGN KEY (`NO_EMPLEADO`) REFERENCES `empleados` (`NO_EMPLEADO`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `resultados_cues_ibfk_2` FOREIGN KEY (`ID_EVALUACION`) REFERENCES `evaluaciones` (`ID_EVALUACION`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `resultados_cues_ibfk_3` FOREIGN KEY (`ID_CUESTIONARIO`) REFERENCES `cuestionarios` (`ID_CUESTIONARIO`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `resultados_cues_ibfk_4` FOREIGN KEY (`ID_PUESTO`) REFERENCES `puestos` (`ID_PUESTO`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `resultados_cues_ibfk_5` FOREIGN KEY (`NO_EMP_EVA`) REFERENCES `empleados` (`NO_EMPLEADO`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `resultados_metas`
--
ALTER TABLE `resultados_metas`
ADD CONSTRAINT `resultados_metas_ibfk_1` FOREIGN KEY (`ID_EVALUACION`) REFERENCES `evaluaciones` (`ID_EVALUACION`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `resultados_metas_ibfk_2` FOREIGN KEY (`NO_EMPLEADO`) REFERENCES `empleados` (`NO_EMPLEADO`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `resultados_metas_ibfk_3` FOREIGN KEY (`ID_PUESTO`) REFERENCES `puestos` (`ID_PUESTO`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `resultados_metas_ibfk_4` FOREIGN KEY (`NO_EMP_EVA`) REFERENCES `empleados` (`NO_EMPLEADO`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `resultados_preguntas`
--
ALTER TABLE `resultados_preguntas`
ADD CONSTRAINT `resultados_preguntas_ibfk_1` FOREIGN KEY (`ID_RESULTADO_CUES`) REFERENCES `resultados_cues` (`ID_RESULTADO_CUES`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `resultados_preguntas_ibfk_2` FOREIGN KEY (`ID_PREGUNTA`) REFERENCES `preguntas` (`ID_PREGUNTA`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`NO_EMPLEADO`) REFERENCES `empleados` (`NO_EMPLEADO`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
