<!-- PRBOBADA Y FUNCIONANDO -->
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Sistema de Evaluación</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="css/estilos.css">
    <script src = "js/jquery-3.1.1.js"></script>
    <script src="js/main-query.js"></script>
    <script src = "js/main.js"></script>
    <script src = "js/ajax.js"></script>
</head>
<body>
    <div class="incorrecto-inde">
        <p></p>
        <span class="glyphicon glyphicon-remove"></span>
    </div>
    <header class="inicio-sistema">
        <div>
            <img src="img/Logo-SCT-Horizontal.png" alt="">
        </div>
        <div>
            <h1>Sistema de evaluación</h1>
        </div>
        <div>
            <img src="img/Logo-API-Salina-Cruz.png" alt="">
        </div>
    </header>
    <form role="form" class="col-lg-4 col-lg-offset-4 col-md-4 col-sm-6 col-sm-offset-3 col-md-offset-4 col-xs-10 col-xs-offset-1 inde" onsubmit="val_index(); return false">
        <h2 class="text-center">Iniciar sesión</h2>
        <div class="form-group">
            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
            <input class="form-control" id="usuario" placeholder="Usuario" type="text" required>
        </div>
        <div class="form-group">
            <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
            <input class="form-control" id="password" placeholder="Contraseña" type="password" required>
            <i class="ver-pass glyphicon glyphicon-eye-open"></i>
        </div>
        <button type = "submit" class="iniciar" >Iniciar</button>
        <a href="php/mail.php">Olvidastes tu contraseña?</a>
    </form>
    <div class="clearfix"></div>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
</body>
</html>
