/*
Finalidad: Modulo con funciones referentes a la creacion de DOM, asi como las respuestas de ajax

Implementacion: nave_admin.php, metas.php

Resumen: Contiene todas las funciones referentes a JavaScript
*/

var contador_pregunta = 1;
var contador_comp = 2;
var contador_eli = 2;
var contador_nombre = 2;

// GENERAL
/*
*@comparar_fecha
*Compara dos fechas, si la primera es mayor o igual regresa true si no, false
*Parametros: String 
*f_a, Primera fecha a comparar
*f_b, Segunda fecha a comparar
*Return: boolean
*/
function comparar_fecha(f_a,f_b){
    f_a = f_a.split("-");
    f_b = f_b.split("-");
    for (var i in f_a){
        f_a[i] = parseInt(f_a[i]);
    }
    for (var i in f_b){
        f_b[i] = parseInt(f_b[i]);
    }
    if (f_a[0] > f_b[0]){
        return true;
    }
    else if(f_a[0] == f_b[0]){
        if (f_a[1] > f_b[1]){
            return true;
        }
        else if (f_a[1] == f_b[1]){
            if (f_a[2] < f_b[2]){
                return false;
            }
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }
    else{
        return false;
    }
}

/*
*@hoy
*Da la fecha de hoy
*Parametros: Ninguno
*Return: String
*/
function hoy(){
    var hoy = new Date();
    hoy = hoy.getFullYear()+"-"+ (hoy.getMonth() +1)+ "-" + hoy.getDate();
    return hoy;
}

/*
*@cargador_datos
*carga la informacion para mostrar la respuesta de ajax.php
*Parametros: Ninguno
*Return: Ninguno
*/
function cargador_datos(){
    if(this.destino =="puesto1"){
        document.getElementById(this.destino).innerHTML= this.peticion.responseText+"<option>Ninguno</option>";
    }
    else {
        document.getElementById(this.destino).innerHTML= this.peticion.responseText;
    }
}

/*
*@pasar
*Pasa un valor a un elemento
*Parametros: String
*id, a quien se le pasara el valor
*v, valor
*Return: Ninguno
*/
function pasar(id,v){
    document.getElementById(id).value = v;
}

/*
*@val
*Obtiene el valor de un elemento
*Parametros: String
*x, a quien se le obtendra el valor
*Return: String, valor del elemento
*/
function val(x){
  return document.getElementById(x).value;
}
// FIN GENERAL
//--------------------------------------------------------------------------------------------
// INDEX.PHP
/*
*@inco_inde
*Mensaje de incorrecto en index.php
*Parametros: String
*mensaje, mensaje que se mostrara
*Return: Ninguno
*/
function inco_inde(mensaje){
    $('.incorrecto-inde').css({'top':'40px'});
    $('.incorrecto-inde > p').text(mensaje);
    setTimeout(function(){
        $('.incorrecto-inde').removeAttr('style');
    },3000)
}

/*
*@val_index
*manda la informacion de usuario por ajax
*Parametros: Ninguno
*Return: Ninguno
*/
function val_index(){
    var usu = val("usuario");
    var pass = val("password");
    if(usu != "" && pass != ""){
        var v = "acti=sesion&usuario="+usu+"&password="+pass;
        inicio(alerta,v);
    }
    else{
       inco_inde('Complete los campos');
    }
}

/*
*@alerta
*Muestra alertas dependiendo la respuesta de ajax
*Parametros: Ninguno
*Return: Ninguno
*/
function alerta(){
    switch(this.peticion.responseText){
        case "1":
            inco_inde('Usuario o contraseña incorrecta');
            break;
        case "2":
            location.href = "html/seleccion.php";
            break;
        case "3":
             location.href = "html/administrador.php";
            break;
    }
}
// FIN INDEX.PHP
//--------------------------------------------------------------------------
// NAVE_ADMIN.PHP
/*
*@val_fechas
*verifica las fechas
*Parametros: String
*valor, valor del input
*id, id del input
*Return: Ninguno
*/
function val_fechas(valor,id){
    if(!comparar_fecha(valor,hoy())){
        alert("Fecha incorrecta");
        act_cerrar(id.substr(0,1));
    }
    else{
      if(id.substr(0,1) == "m"){
        var f_fin = val("mFeFin");
        var f_inicio = val("mFeIn");
      }
      else{
        var f_fin = val("nFeFin");
        var f_inicio = val("nFeIn");
      }
        if(f_fin != "" && f_inicio != ""){
            if(!comparar_fecha(f_fin,f_inicio)){
                alert("Fecha incorrecta");
                act_cerrar(id.substr(0,1));
            }
            else{
                act_crear(id.substr(0,1));
            }
        }
    }
}

/*
*@act_crear
*revisa la informacion de los input para habilitar el boton crear
*Parametros: String
*x, "n"o "m" (nuevo o modificar)
*Return: Ninguno
*/
function act_crear(x){
    if(x == "n"){
        var f_fin = val("nFeFin");
        var f_inicio = val("nFeIn");
        var noEv = val("nNomEva");
        if(f_fin != "" && f_inicio != "" && noEv != ""){
            b_atri(x);
        }
    }
    if(x == "m"){
        b_atri(x);
    }
}

/*
*@b_atri
*habilita el boton crear en nueva evaluacion
*Parametros: String
*x, "n"o "m" (nuevo o modificar)
*Return: Ninguno
*/
function b_atri(x){
    var boton = document.getElementById("b"+x+"E");
    boton.className = "btn btn-primary";
    boton.setAttribute("data-dismiss","modal");
    if(x == "n"){
        boton.addEventListener("click",val_nEva,false);
    }
    if(x == "m"){
        boton.addEventListener("click",val_mEva,false);
    }
}

/*
*@act_cerrar
*Desabilita el boton crear
*Parametros: String
*x, "n"o "m" (nuevo o modificar)
*Return: Ninguno
*/
function act_cerrar(x){
    var boton = document.getElementById("b"+x+"E");
    boton.className = "btn btn-primary disabled";
    boton.setAttribute("data-dismiss","modall");
    if(x == "n"){
        boton.removeEventListener("click",val_nEva,false);
    }
    if(x == "m"){
        boton.removeEventListener("click",val_mEva,false);
    }
}

/*
*@val_nEva
*manda la informacion de la evaluacion por ajax
*Parametros: Ninguno
*Return: Ninguno
*/
function val_nEva(){
    var f_fin = val("nFeFin");
    var f_inicio = val("nFeIn");
    var noEv = val("nNomEva");
    if(f_fin != "" && f_inicio != "" && noEv != ""){
        var v = "acti=nu_eva&nom_eva="+noEv
            +"&fecha_inicio="+f_inicio
            +"&fecha_fin="+f_fin
            +"&cues_m="+val("nCueM")
            +"&cues_o="+val("nCueO");
            inicio(eva_alert,v);
    }
}

/*
*@eva_alerta
*Muestra alertas dependiendo la respuesta de ajax
*Parametros: Ninguno
*Return: Ninguno
*/
function eva_alert(){
    document.getElementById("nFeFin").value = "";
    document.getElementById("nFeIn").value = "";
    document.getElementById("nNomEva").value = "";
    switch(this.peticion.responseText){
        case "0":
            if(window.location.pathname == "/sistema-evaluacion/html/administrador.php"){
                cargar_evas();
            }
            else{
                location.href= "../html/administrador.php";
            }
            notifi('.bueno');
            break;
        case "1":
            notifi('.malo');
            break;
    }
}

/*
*@npdep
*manda la informacion de los puestos por ajax
*Parametros: Ninguno
*Return: Ninguno
*/
function npdep(){
  var v = "acti=Npuesto&valor="+val("npdepar");
  inicio(cargador_datos,v,"puesto1")

}
// FIN NAVE_ADMIN.PHP
//---------------------------------------------------------------------------
// ADMINISTRADOR.PHP
/*
*@cargar_evas
*Muestra todos las evaluaciones
*Parametros: Ninguno
*Return: Ninguno
*/
function cargar_evas(){
    var v = "acti=mos_evas";
    inicio(cargador_datos,v,"form_mos_eva");
}

/*
*@eli_eva
*manda la informacion de la evaluacion por ajax (eliminar)
*Parametros: String
*valor, id de la evaluacion
*Return: Ninguno
*/
function eli_eva(valor){
    var v = "acti=eli_eva&valor="+valor;
    inicio(compr_eva,v);
}

/*
*@compr_eva 
*Verifica la respuesta de ajax y carga las respuestas
*Parametros: Ninguno
*Return: Ninguno
*/
function compr_eva(){
    switch(this.peticion.responseText){
        case "0":
            cargar_evas();
            mostrar();
            break;
        case "1":
            mostrar2();
            break;
    }
}

/*
*@modi_eva
*manda la informacion de la evaluacion por ajax (ventana modificar)
*Parametros: String
*valor, id de la evaluacion
*Return: Ninguno
*/
function modi_eva(valor){
    var v = "acti=modi_eva&valor="+valor;
    inicio(cargador_datos,v,"papa_modi");
}

/*
*@val_mEva
*manda la informacion de la evaluacion por ajax (modificar)
*Parametros: Ninguno
*Return: Ninguno
*/
function val_mEva(){
    var v = "acti=m_eva&id_eva="+val("mId_eva")
        +"&nom_eva="+val("mNomEva")
        +"&fecha_inicio="+val("mFeIn")
        +"&fecha_fin="+val("mFeFin")
        +"&cues_m="+val("mCueM")
        +"&cues_o="+val("mCueO");
    inicio(compr_eva,v);
}

/*
*@modal_deta
*manda la informacion de la evaluacion por ajax (detalles)
*Parametros: String
*valor, id de la evaluacion
*Return: Ninguno
*/
function modal_deta(valor){
    var v = "acti=moda_deta"
        +"&valor="+valor
        +"&pag=hola";
    inicio(cargador_datos,v,"papa_detalles");
}

/*
*@modal_deta2
*manda la informacion de la evaluacion por ajax (detalles)
*Parametros: String
*valor, id de la evaluacion
*Return: Ninguno
*/
function modal_deta2(valor){
    var v = "acti=moda_deta"
        +"&valor="+valor
        +"&pag=0";
    inicio(grafica,v);
}

/*
*@grafica
*Muestra la grafica de detalles ( ver documentacion de chart.js)
*Parametros: Ninguno
*Return: Ninguno
*/
function grafica(){
    var info = this.peticion.responseText.split(",");
    var x = parseInt(info[0]);
    var y = parseInt(info[1]);
    var nodo = document.getElementById("papa_grafica");

        var circulito = new Chart(nodo,{
            type: 'pie',
            data: {
                labels:["Realizados", "Faltan"],
                datasets: [{
                    label: "Estado de la evaluacion",
                    data: [x,y],
                    backgroundColor :[
                        'greenyellow',
                        'crimson',
                    ],
                    borderColor : [
                        'greenyellow',
                        'crimson',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
}
// FIN ADMINISTRADOR.PHP
//---------------------------------------------------------------------------------------
// USUARIOS.PHP
/*
*@cargar_usu
*manda la informacion de los usuarios por ajax 
*Parametro: String
*valor, tipo de usuario a mostrar
*Return: Ninguno
*/
function cargar_usu(valor){
    if(window.location.pathname == "/sistema-evaluacion/html/usuarios.php"){
        var v = "acti=mos_usu&valor="+valor;
        var tit = document.getElementById("titulo");
        var bus = document.getElementById("buscar");
        var ues = document.getElementById("uestado");
        if(valor == "activo"){
            tit.innerHTML = "Usuarios activos"
            ues.value ="activo";
        }
        if(valor == "baja"){
            tit.innerHTML = "Usuarios de baja";
            ues.value ="baja"
        }
        if(valor == "todos"){
            tit.innerHTML = "Usuarios";
            ues.value ="todos"
        }
        inicio(cargador_datos,v,"papa_usuario");
        bus.value = "";
    }
    else{
       var naEst = document.getElementById("naEstado");
       naEst.value = valor;
       form_nav_usu.submit();
    }
}

/*
*@acti_usu
*Pasa los valores a la ventana modal(baja o activar)
*Parametro: String
*noemp, numero de empleado
*acti, actividad (baja o activar)
*Return: Ninguno
*/
function acti_usu(noemp,acti){
    var x = document.getElementById("cuNoemp");
    var y = document.getElementById("uacti");
    x.value = noemp;
    y.value = acti;
}

/*
*@b_a_usu
*manda la informacion de los usuarios por ajax (baja o activar)
*Parametro: Ninguno
*Return: Ninguno
*/
function b_a_usu(){
    var v = "acti="+val("uacti")
        +"&noemp="+val("cuNoemp")
        +"&pass="+val("pass");
        document.getElementById("pass").value = "";
    inicio(alert_usu,v);
}

/*
*@alert_usu
*Dependiendo de la respuesta de ajax, muestra la informacion
*Parametro: Ninguno
*Return: Ninguno
*/
function alert_usu(){
    switch(this.peticion.responseText){
        case "activo":
            notifi('.bueno');
            cargar_usu("activo");
            break;
        case "baja":
            notifi('.bueno');
            cargar_usu("baja");
            break;
        case "1":
            notifi('.malo');
            break;
    }
}

/*
*@busqueda_usu
*manda la informacion de los usuarios por ajax(buscar usuario)
*Parametro: Ninguno
*Return: Ninguno
*/
function busqueda_usu(){
    var v = "acti=bus_usu"
        +"&valor="+val("buscar")
        +"&estado="+val("uestado");
    inicio(cargador_datos,v,"papa_usuario");
}

/*
*@modi_usu
*manda la informacion de los usuarios por ajax (ventana modal modificar)
*Parametro: String
*valor, tipo de usuario a mostrar
*Return: Ninguno
*/
function modi_usu(valor){
    var v = "acti=modi_usu"
        +"&valor="+valor
    inicio(cargador_datos,v,"papa_modi");
}

/*
*@da_pe_modi_usu
*manda la informacion de los usuarios por ajax (modificar datos personales)
*Parametro: Ninguno
*Return: Ninguno
*/
function da_pe_modi_usu(){
    var v = "acti=modi_dp"
        +"&valor="+formu_modi_usu.valor.value
        +"&nombre="+formu_modi_usu.nombre.value
        +"&apellidoP="+formu_modi_usu.apellidoP.value
        +"&apellidoM="+formu_modi_usu.apellidoM.value
        +"&rfc="+formu_modi_usu.rfc.value
        +"&curp="+formu_modi_usu.curp.value
        +"&noemp="+formu_modi_usu.noemp.value
        +"&correo="+formu_modi_usu.correo.value;
    inicio(prueba10,v);
}

/*
*@prueba10
*Recibe la respuesta de ajax, y muestra alertas dependiendo de esta
*Parametro: Ninguno
*Return: Ninguno
*/
function prueba10(){
    switch(this.peticion.responseText){
        case "1":
            notifi('.malo');
            break;
        case "0":
            notifi('.bueno');
            cargar_usu("activo");
            break;
    }
}

/*
*@da_ta_modi_usu
*Manda la informacion del usuario por ajax(datos de trabajo)
*Parametro: Ninguno
*Return: Ninguno
*/
function da_ta_modi_usu(){
    var v = "acti=modi_dt"
        +"&valor="+formu_modi_usu.valor.value
        +"&fechaingre="+formu_modi_usudt.fechaingre.value
        +"&puesto="+formu_modi_usudt.puesto2.value;
    inicio(prueba10,v)
}
// FIN USUARIOS.PHP
//----------------------------------------------------------------------------------------
// REGISTRO.PHP
/*
*@valida_registro
*Manda la informacion del usuario por ajax(valida datos del registro)
*Parametro: String
*campo, campo a validar
*acti, actividad a realizar
*Return: Ninguno
*/
function valida_registro(campo,acti){
    var input = document.getElementById(campo);
    if (input.value != ""){
        var v = "acti="+acti;
        if(acti.substr(0,1) == "m"){

            v += "&noemp="+valor.value;
        }
        switch(campo){
            case "usuario":
                if(input.value.length >=6 && input.value.indexOf(" ") == -1){
                    if(acti)
                    v +="&valor="+input.value;
                    inicio(regis_alert,v);
                }
                else{
                    malo('.formu-reg > div > div:nth-child(2)  i');
                }
                break;
            case "rfc":
                if(input.value.length == 13 && input.value.indexOf(" ") == -1){
                    v +="&valor="+input.value;
                    inicio(regis_alert,v);
                }
                else{
                    if(window.location.pathname == "/sistema-evaluacion/html/usuarios.php"){
                        malo('.form-modal-mod > div > div:nth-child(4) > i');
                    }
                    else{
                        malo('.formu-reg > div:nth-child(2) > div:nth-child(5)  i');
                    }

                }
                break;
            case "curp":
                if(input.value.length == 18 && input.value.indexOf(" ") == -1){
                    v +="&valor="+input.value;
                    inicio(regis_alert,v);
                }
                else{
                    if(window.location.pathname == "/sistema-evaluacion/html/usuarios.php"){
                        malo('.form-modal-mod > div > div:nth-child(5) > i');
                    }
                    else{
                        malo('.formu-reg > div:nth-child(2) > div:nth-child(6)  i');
                    }
                }
                break;
            case "noemp":
                if(input.value.indexOf(" ") == -1){
                    v +="&valor="+input.value;
                    inicio(regis_alert,v);
                }
                else{
                    if(window.location.pathname == "/sistema-evaluacion/html/usuarios.php"){
                        malo('.form-modal-mod > div > div:nth-child(6) > i');
                    }
                    else{
                        malo('.formu-reg > div:nth-child(2) > div:nth-child(7)  i');
                    }
                }
                break;
            case "dep":
                v +="&valor="+input.value;
                inicio(cargador_datos,v,"puesto2")
                break;
        }
    }
}

/*
*@confirmar_pass
*Confirma que las dos contraseñas sean iguales
*Parametro: String
*valor, contraseña
*Return: Ninguno
*/
function confirmar_pass(valor){
    if(valor != ""){
        var ps = document.getElementById("contrasena");
        if (ps.value == valor && ps.value.length >=6)
            bien('.formu-reg > div > div:nth-child(3) > div  i');
        else
            malo('.formu-reg > div > div:nth-child(3) > div  i');
    }
}

/*
*@bien
*Muestra una paloma
*Parametro: String
*dire, direccion de donde quiere que se muestre
*Return: Ninguno
*/
function bien(dire){
    $(dire).removeClass('fa-spinner');
    $(dire).removeClass('fa-pulse');
    $(dire).addClass('fa-check-square');
}

/*
*@malo
*Muestra una X
*Parametro: String
*dire, direccion de donde quiere que se muestre
*Return: Ninguno
*/
function malo(dire){
    $(dire).removeClass('fa-spinner');
    $(dire).removeClass('fa-pulse');
    $(dire).addClass('fa-window-close');
}

/*
*@regis_alert
*Dependiendo de la respuesta de ajax, muestra alertas
*Parametro: Ninguno
*Return: Ninguno
*/
function regis_alert(){
    switch(this.peticion.responseText){
        case "usua0":
            bien('.formu-reg > div > div:nth-child(2) > i');
            break;
        case "usua1":
            malo('.formu-reg > div > div:nth-child(2) > i');
            break;
        case "curp0":
            if(window.location.pathname == "/sistema-evaluacion/html/usuarios.php"){
                bien('.form-modal-mod > div > div:nth-child(5) > i');
            }
            else{
                bien('.formu-reg > div:nth-child(2) > div:nth-child(6)  i');
            }
            break;
        case "curp1":
            if(window.location.pathname == "/sistema-evaluacion/html/usuarios.php"){
                malo('.form-modal-mod > div > div:nth-child(5) > i');
            }
            else{
                malo('.formu-reg > div:nth-child(2) > div:nth-child(6)  i');
            }
            break;
        case "rfc0":
            if(window.location.pathname == "/sistema-evaluacion/html/usuarios.php"){
                bien('.form-modal-mod > div > div:nth-child(4) > i');
            }
            else{
                bien('.formu-reg > div:nth-child(2) > div:nth-child(5)  i');
            }
            break;
        case "rfc1":
            if(window.location.pathname == "/sistema-evaluacion/html/usuarios.php"){
                malo('.form-modal-mod > div > div:nth-child(4) > i');
            }
            else{
                malo('.formu-reg > div:nth-child(2) > div:nth-child(5)  i');
            }
            break;
        case "emp0":
            if(window.location.pathname == "/sistema-evaluacion/html/usuarios.php"){
                bien('.form-modal-mod > div > div:nth-child(6) > i');
            }
            else{
                bien('.formu-reg > div:nth-child(2) > div:nth-child(7)  i');
            }
            break;
        case "emp1":
            if(window.location.pathname == "/sistema-evaluacion/html/usuarios.php"){
                malo('.form-modal-mod > div > div:nth-child(6) > i');
            }
            else{
                malo('.formu-reg > div:nth-child(2) > div:nth-child(7)  i');
            }
            break;
        case "1":
            alert("no se puede completar esa operacion");
            break;
        case "0":
            location.href = '../html/usuarios.php';
            break;
    }
}

/*
*@registrar_usu
*Manda la informacion del usuario por ajax(registrar usuario)
*Parametro: Ninguna
*Return: Ninguno
*/
function registrar_usu(){
    var v = "acti=reg_usu"
        +"&usuario="+formu_reg_usu.usuario.value
        +"&password="+formu_reg_usu.contrasena.value
        +"&nombre="+formu_reg_usu.nombre.value
        +"&apellidoP="+formu_reg_usu.apellidoP.value
        +"&apellidoM="+formu_reg_usu.apellidoM.value
        +"&rfc="+formu_reg_usu.rfc.value
        +"&curp="+formu_reg_usu.curp.value
        +"&noemp="+formu_reg_usu.noemp.value
        +"&correo="+formu_reg_usu.correo.value
        +"&fechaingre="+formu_reg_usu.fechaingre.value
        +"&puesto="+formu_reg_usu.puesto2.value;
    inicio(regis_alert,v);
}
// FIN REGISTRO.PHP
//-------------------------------------------------------------------------------------
// CUESTIONARIO.PHP
/*
*@cargar_cues
*Manda la informacion a ajax, para mostrar todos los cuestionarios
*Parametro: ninguno
*Return: Ninguno
*/
function cargar_cues(){
    var v = "acti=mos_cues";
    inicio(cargador_datos,v,"form_cues");
}

/*
*@eli_cues
*Manda la informacion a ajax, para eliminar un cuestionarios
*Parametro: ninguno
*Return: Ninguno
*/
function eli_cues(){
    var v = "acti=eli_cues"
        +"&valor="+val("idCues");
    inicio(prueba18,v);
}

/*
*@prueba18
*recibe la respuesta de ajax, y muestra alertas dependiendo de esta
*Parametro: ninguno
*Return: Ninguno
*/
function prueba18(){
    switch(this.peticion.responseText){
        case "1":
            notifi('.malo');
            break;
        case "0":
            notifi('.bueno');
            cargar_cues();
            break;
    }
}
// FIN CUESTIONARIO.PHP
//----------------------------------------------------------------------------------------
// MODIFICAR_CUESTIONARIO.PHP
/*
*@cargar_pregun
*Manda la informacion a ajax, para mostrar las preguntas
*Parametro: String
*acti, modificar o visualizar
*Return: Ninguno
*/
function cargar_pregun(acti){
    var valor = acti.substr(1);
    var acti = acti.substr(0,1);
    var v = "acti="+acti
        +"&valor="+valor;
    inicio(cargador_datos,v,"papa_pregunta");
}

/*
*@modal_preg
*Manda la informacion a ajax, para mostrar la ventana modal de preguntas
*Parametro: String
*valor, id de la pregunta
*Return: Ninguno
*/
function modal_preg(valor){
    var v = "acti=modal_preg"
        +"&valor="+valor;
    inicio(cargador_datos,v,"papa_modi_preg")
}

/*
*@modi_preg
*Manda la informacion a ajax, para modificar la pregunta
*Parametro: ninguno
*Return: Ninguno
*/
function modi_preg(){
    var v = "acti=modi_preg"
        +"&id="+val("mIdPreg")
        +"&pregunta="+val("mPreg");
    inicio(prueba20,v)
}

/*
*@prueba20
*recibe la informacion de ajax y muestra alertas
*Parametro: ninguno
*Return: Ninguno
*/
function prueba20(){
    switch(this.peticion.responseText){
        case "0":
            notifi('.bueno');
            cargar_pregun(val("papa_cuest"));
            document.getElementById("nPreg").value = "";
            break;
        case "1":
            notifi('.malo');
            break;
    }
}

/*
*@notifi
*muestra una noticiacion
*Parametro: String
*valor,clase de donde mostrara
*Return: Ninguno
*/
function notifi(valor){
    $(valor+' i').css({'transform':'scale(1)'});
    setTimeout(function(){
        $(valor+' p').css({'right':'0','width':'300px','opacity':'1'});
    },700);
    setTimeout(function(){
        $(valor+' p').removeAttr('style');
    },1500);
    setTimeout(function(){
        $(valor+' i').removeAttr('style');
    },2300);
}

/*
*@eli_preg
*Manda la informacion a ajax, para mostrar eliminar una pregunta
*Parametro: ninguno
*Return: Ninguno
*/
function eli_preg(){
    var v = "acti=eli_preg"
        +"&id="+val("eIdPreg");
    inicio(prueba20,v);
}

/*
*@nuev_preg
*Manda la informacion a ajax, para mostrar agregar una pregunta
*Parametro: ninguno
*Return: Ninguno
*/
function nuev_preg(){
    var v = "acti=nuev_preg"
        +"&id="+val("papa_cuest")
        +"&preg="+val("nPreg");
    inicio(prueba20,v);
}
// FIN MODIFICAR_CUESTIONARIO.PHP
//-------------------------------------------------------------------------------------------
// VISUALIZAR_CUESTIONARIO.PHP
/*
*@pasar_valor_eliminar
*pasa valor para eliminar la pregunta
*Parametro: String
*id, id de la pregunta
*Return: Ninguno
*/
function pasar_valor_eliminar(id){
    var id_pre = document.getElementsByName("pregunta"+id);
    document.getElementById("eli").value = id_pre[0].value;
}
// FIN VIZUALIZAR_CUESTIONARIO.PHP
// AGREGAR CUESTIONARIO
/*
*@nombre_cues
*Asigna el nombre al cuestionario
*Parametro: String
*nom_cues, nombre del cuestionario
*Return: Ninguno
*/
function nombre_cues(nom_cues){
    document.getElementById("mTitulo").value = nom_cues;
}

/*
*@modificaar_titulo
*modificar el titulo
*Parametro: ninguno
*Return: Ninguno
*/
function modificar_titulo(){
    var ti = document.getElementById("mTitulo").value;
    document.getElementById("titulo").innerHTML = ti;
    document.getElementById("nom_cues").value=ti;
}

/*
*@agregar_pregunta
*agrega una pregunta con DOM
*Parametro: ninguno
*Return: Ninguno
*/
function agregar_pregunta(){
    var pregunta = document.getElementById("pregunta");
    if (pregunta.value != ""){
        var fila = document.createElement("tr");
        var columna1 = document.createElement("td");
        var espacio = document.createElement("br");
        var columna2 = document.createElement("td");
        var cuadro_pregunta = document.getElementById("cuadro_pregunta");
        var nodoPregunta = document.createElement("span");
        columna1.innerHTML = "</br>";
        nodoPregunta.setAttribute("name", contador_pregunta);
        var cadenaPregunta = pregunta.value + "</br>" ;
        nodoPregunta.innerHTML = cadenaPregunta;
        var nodoHidden = document.createElement("input");
        nodoHidden.setAttribute("type", "hidden");
        nodoHidden.setAttribute("value", pregunta.value);
        nodoHidden.setAttribute("name", contador_pregunta);
        columna1.appendChild(nodoPregunta);
        columna1.appendChild(nodoHidden);
        columna1.appendChild(espacio);
        columna1.setAttribute("class","col-md-11");
        fila.appendChild(columna1);
        document.getElementById("final").value =contador_pregunta;
        var botones = '<div class="grupo-btn-cues"><button type="button" class="btn btn-primary" data-toggle="modal" data-target= "#modal_pregunta" value = "'+contador_pregunta+'" onclick = "modificar_pregunta_modal(this.value)">'
                +'<span class = "glyphicon glyphicon-pencil"></span>'
                +'</button>'
                +'<button type="button" id="'+contador_pregunta++ +'" class="btn btn-danger" onclick = "eliminar(this.id)">'
                +'<span class = "glyphicon glyphicon-remove"></span>'
                +'</button></div>';
        columna2.innerHTML = botones;
        fila.appendChild(columna2);
        columna2.setAttribute("class","col-md-1");
        cuadro_pregunta.appendChild(fila);
        pregunta.value = "";
    }
}
/*
*@eliminar
*Elimina al nodo del argumento
*Parametro: String
*nodo, nodo a eliminar
*Return: Ninguno
*/
function eliminar(nodo){
    var padre = document.getElementById(nodo).parentNode.parentNode.parentNode;
    padre.parentNode.removeChild(padre);
}

/*
*@modificar_pregunta_modal
*Mnada la informacion para mostrar en la ventana modal la pregunta a modificar
*Parametro: id
*id, id de la pregunta a modificar
*Return: Ninguno
*/
function modificar_pregunta_modal(id){
    var paquete = document.getElementsByName(id);
    document.getElementById("mPregunta").value = paquete[1].value;
    document.getElementById("modi").value = id;
}

/*
*@modificar_pregunta
*Modifica la pregunta
*Parametro: id
*id, de pregunta a modificar
*Return: Ninguno
*/
function modificar_pregunta(id){
    var valor = document.getElementById("mPregunta").value;
    var paquete = document.getElementsByName(id);
    paquete[0].innerHTML = valor + "</br>";
    paquete[1].value = valor;
}
// FIN AGREGAR_CUESTIONARIO.PHP
//------------------------------------------------------------------------
// EVALUACION.PHP
/*
*@carga_evados
*Manda la informacion a ajax, para mostrar los usuarios evaluados
*Parametro: String
*valor, id de la evaluacion
*Return: Ninguno
*/
function cargar_evados(valor){
    var v = "acti=mos_evados"
        +"&valor="+valor;
    inicio(cargador_datos,v,"papa_evados");
}
/*
*@busqueda_evados
*Manda la informacion a ajax, para mostrar los usuarios evaluados buscados
*Parametro: Ninguno
*Return: Ninguno
*/
function busqueda_evados(){
    var v = "acti=bus_evados"
        +"&valor="+val("buscar")
        +"&id="+val("eIdEva");
    inicio(cargador_datos,v,"papa_evados");
}
// FIN EVALUACION.PHP
//-----------------------------------------------------------------------------------------
// DEPARTAMENTO.PHP
/*
*@nuev_depa
*Manda la informacion a ajax, para agregar una nueva area adscrita
*Parametro: Ninguno
*Return: Ninguno
*/
function nuev_depa(){
  var v = "acti=nuev_depa"
      +"&valor="+val("nuevo_depa")
      +"&pass="+val("apss");
    inicio(alert_depa,v);
}

/*
*@nuev_puest
*Manda la informacion a ajax, para agregar un nuevo puesto
*Parametro: Ninguno
*Return: Ninguno
*/
function nuev_puest(){
    var v = "acti=nuev_puest"
        +"&puesto="+val("npuesto")
        +"&nivel="+val("nivel")
        +"&depa="+val("npdepar")
        +"&mando="+val("puesto1")
        +"&pass="+val("ppss");
    inicio(alert_depa,v);
}

/*
*@alert_depa
*recibe la respuesta de ajax, y muestra alertas
*Parametro: Ninguno
*Return: Ninguno
*/
function alert_depa(){
  switch(this.peticion.responseText){
    case "0":
      notifi('.bueno');
      document.getElementById("nuevo_depa").value = "";
      document.getElementById("apss").value = "";
      document.getElementById("npuesto").value = "";
      document.getElementById("nivel").value = "";
      document.getElementById("npdepar").value = "";
      document.getElementById("puesto1").value = "";
      document.getElementById("ppss").value = "";
      if (window.location.pathname == "/sistema-evaluacion/html/departamentos.php"){
        cargar_depa();
      }
      else {
        location.href = '../html/departamentos.php';
      }
      break;
    case "1":
      document.getElementById("apss").value = "";
      document.getElementById("ppss").value = "";
      notifi('.malo');
      if (!(window.location.pathname == "/sistema-evaluacion/html/departamentos.php")){
        location.href = '../html/departamentos.php';
      }
      break;
  }
}

/*
*@cargar_depa
*Manda la informacion a ajax, para mostrar todos los departamentos
*Parametro: Ninguno
*Return: Ninguno
*/
function cargar_depa(){
  var v = "acti=mos_depas";
  inicio(cargador_datos,v,"papa_depas");
}
// FIN DEPARTAMENTO.PHP
//------------------------------------------------------------------------------
// METAS.PHP
/*
*@mas_cpmp
*habilita o desabilita el boton compromisos, tambien crea mas compromisos don DOM
*Parametro: String
*id, del compromiso
*Return: Ninguno
*/
function mas_comp(id){
    var boton = document.getElementById(id);
    if (contador_comp == 2){
        var beli = document.getElementById("ra1");
        if (beli != null){
            beli.className = "btn btn-danger";
            beli.setAttribute("onclick","eliminarc(this.id)");
        }
    }
    if(contador_comp < 4){
        crear_compromiso(contador_nombre);
        if (contador_comp == 3){
            boton.className = "btn btn-default disabled";
        }
    }
    if(contador_comp == 4){
        boton.setAttribute("data-target","#no_mas_com");
        boton.setAttribute("data-toggle","modal");
    }
    if (contador_comp <= 3){
        contador_comp++;
    }
}

/*
*@eliminarc
*elimina un compromiso
*Parametro: String
*nodo, compromiso a eliminar
*Return: Ninguno
*/
function eliminarc(nodo){
    var boton = document.getElementById("bcomp");
    boton.className = "btn btn-default";
    boton.setAttribute("data-target","#no");
    boton.setAttribute("data-toggle","mo");
    var padre = document.getElementById(nodo).parentNode;
    padre.parentNode.removeChild(padre);
    contador_comp--;
}

/*
*@crear_compromiso
*Crea un nuevo compromiso
*Parametro: numerico
*nombre, identificador de compromiso 
*Return: Ninguno
*/
function crear_compromiso(nombre){
    var div = document.createElement("div");
    div.className = "col-md-7";
    var cadena =
        '<div class="form-group col-md-8 col-sm-6">'
        +'<label class="control-label" for="comp">Compromisos</label>'
        +'<textarea name="compromiso'+nombre+'" class="form-control" pattern="[A-Za-z ñ0-9]*" placeholder="Inserte sus Compromisos" rows="2" required></textarea>'
        +'</div>'
        +'<div class="form-group datepicker-group z-ind-cale col-md-4 col-sm-6">'
        +'<label class="control-label col-md-12" for="fecha">Fecha</label>'
        +'<input class="calendario col-md-12" name="fecha'+nombre+'" type="text" required onfocus="calendario()">'
        +'<span class="glyphicon glyphicon-calendar cale-usu-1" aria-hidden="true"></span>'
        +'</div>'
        +'<button type= "button" class = "btn btn-danger" id = "ra'+contador_eli+'" onclick="eliminarc(this.id)">'
        +'<span class="glyphicon glyphicon-remove"></span>'
        +'</button>';
    div.innerHTML = cadena;
    var padre = document.getElementById("compromisos");
    padre.appendChild(div);
    document.getElementById("final").value = contador_nombre;
    contador_eli++;
    contador_nombre++;
}
// FIN METAS.PHP
//---------------------------------------------------------------------------
