//desplazamiento de datos-evaluados
$(document).ready(function(){
    //vista previa de contraseña
    var contra = 0;
    $('.ver-pass').click(function(){
        if(contra == 0){
            $('#password').removeAttr('type');
            $('.ver-pass').removeClass('glyphicon-eye-open');
            $('.ver-pass').addClass('glyphicon-eye-close');
            contra = 1;
        }else if(contra == 1){
            $('#password').attr('type','password');
            $('.ver-pass').removeClass('glyphicon-eye-close');
            $('.ver-pass').addClass('glyphicon-eye-open');
            contra = 0;
        }
    })
    //movimiento de seccion de datos del evaluado
    var direccion = window.location.pathname;
    if(direccion == "/sistema-evaluacion/html/calificar_metas.php" || direccion == "/sistema-evaluacion/html/calificar_cues.php"){
        $(window).scroll(function(){
            if(($(this).scrollTop() >= $('.titu-sele').offset().top && $(this).scrollTop() < $('.btn-primary').offset().top -300)){
                $('.datos-evaluado').css({'top':'40px'});
                $('.datos-evaluado h4').css({'border-radius':'0px'});
                $('.datos-evaluado').css({'opacity':'1'});
            }else if($(this).scrollTop() < $('h2').offset().top){
                $('.datos-evaluado').removeAttr('style');
                $('.datos-evaluado h4').removeAttr('style');
            }else if($(this).scrollTop() > $('.btn-primary').offset().top -300){
                $('.datos-evaluado').css({'opacity':'0'});
            }
        })
    }else{
        //console.log("todo bien");
    }
    //efecto de icono de perfil del header en secciones del usuario
    if(direccion == "/sistema-evaluacion/html/calificar_metas.php" || direccion == "/sistema-evaluacion/html/calificar_cues.php" || direccion == "/sistema-evaluacion/html/seleccion.php" || direccion == "/sistema-evaluacion/html/metas.php"){
        $('.perf').mouseenter(function(){
            $('.foto').css({'transform':'scale(2)','top':'30px','right':'-5%'})
            $('.foto > a').css({'display':'block','left':'-25px','font-size':'20px'})
            $('.perf').mouseleave(function(){
                $('.foto').removeAttr('style');
                $('.foto > a').removeAttr('style');
            })
        })
    }
    //efecto en iconos en registro de usuario
    $('.formu-reg > div > div  input').focus(function(){
        $(this).siblings('i').addClass('fa-spinner');
        $(this).siblings('i').addClass('fa-pulse');
        $(this).siblings('i').removeClass('fa-check-square');
        $(this).siblings('i').removeClass('fa-window-close');
    })
    $('.formu-reg > div > div  input').focusout(function(){
        $(this).siblings('i').removeClass('fa-spinner');
        $(this).siblings('i').removeClass('fa-pulse');
    })
})
//errores de inicio de sesion
function mostrar(){
    $('.correcto').css({'top':'180px'});
}
function mostrar2(){
    $('.incorrecto').css({'top':'180px'});
    setTimeout(function(){
        $('.incorrecto').css({'top':'-250px'});
    },3000)
};
//guia de ayuda en seccion de usuarios
$('.ayuda').click(function(){
    console.log('hola');
    $('.personal-evaluar').css({'filter':'blur(5px)'});
    $('header').css({'filter':'blur(5px)'});
    $('aside > div > a').css({'filter':'blur(5px)'});
    $('.perf').css({'filter':'blur(5px)'});
    $('.area').css({'filter':'blur(5px)'});
    $('.ayuda').css({'filter':'blur(5px)'});
    $('.presentacion').css({'display':'block'})
    $('.pri').click(function(){
        $('.personal-evaluar').removeAttr('style');
        $('.presentacion').removeAttr('style');
        $('.pre-eva').css({'display':'block'});
        $('.personal-evaluar').css({'box-shadow':'0px 0px 40px black'});
    })
    $('.seg').click(function(){
        $('.personal-evaluar').removeAttr('style');
        $('.personal-evaluar').css({'filter':'blur(5px)'});
        $('.pre-eva').removeAttr('style');
        $('.pre-met').css({'display':'block'});
        $('aside > div > a').removeAttr('style');
        $('aside > div > a').css({'transform':'scale(1.2)','box-shadow':'0px 0px 20px black'});
    })
    $('.ter').click(function(){
        $('aside > div > a').removeAttr('style');
        $('aside > div > a').css({'filter':'blur(5px)'});
        $('.pre-met').removeAttr('style');
        $('.pre-per').css({'display':'block'});
        $('.perf').removeAttr('style');
        $('.perf').css({'top':'40px'});
    })
    $('.cua').click(function(){
        $('.perf').removeAttr('style');
        $('.perf').css({'filter':'blur(5px)'});
        $('.pre-per').removeAttr('style');
        $('.pre-fin').css({'display':'block'});
    })
    $('.fin-pre').click(function(){
        $('.personal-evaluar').removeAttr('style');
        $('header').removeAttr('style');
        $('aside > div > a').removeAttr('style');
        $('.perf').removeAttr('style');
        $('.area').removeAttr('style');
        $('.ayuda').removeAttr('style');
        $('.pre-fin').removeAttr('style');
    })
})
//animacion del icono en los desplegables
function mano(id){
    var estado = ($('#'+id).css('transform'));
    var clave = 'matrix(-1, 1.22465e-16, -1.22465e-16, -1, 0, 0)';
    var clave2 = 'matrix(1, 0, 0, 1, 0, 0)';
    var clavemoz = 'matrix(-1, 0, 0, -1, 0, 0)';
    if(estado == clave || estado == clavemoz){
        $('#'+id).css({'transform':'rotate(0deg)'});
    }else if(estado == clave2){
        $('#'+id).css({'transform':'rotate(180deg)'});
    }
};

function foco(id){
    $('#'+id).siblings('i').addClass('fa-spinner');
    $('#'+id).siblings('i').addClass('fa-pulse');
    $('#'+id).siblings('i').removeClass('fa-check-square');
    $('#'+id).siblings('i').removeClass('fa-window-close');
}
function desenfoque(id){
    $('#'+id).siblings('i').removeClass('fa-spinner');
    $('#'+id).siblings('i').removeClass('fa-pulse');
}
