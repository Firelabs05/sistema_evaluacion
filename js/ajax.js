// AJAX
var superPeticion = new Object();

superPeticion.NO_INICIADO=0;
superPeticion.CARGANDO=1;
superPeticion.CARGADO=2;
superPeticion.INTERACTUANDO=3;
superPeticion.COMPLETO=4;

superPeticion.cargaContenido = function(url,metodo,funcion,valor,destino) {
    this.url = url;
    this.peticion = null;
    this.onload = funcion;
    if(destino != undefined)
    {
        this.destino = destino;
    }
    this.jugosidad(url,metodo,valor);
}

superPeticion.cargaContenido.prototype = {
    jugosidad: function(url,metodo,valor) {
        this.peticion = new XMLHttpRequest();
        if(this.peticion) {
            try {
                var superClon = this;
                this.peticion.onreadystatechange = function() {
                    superClon.onReadyState.call(superClon);
                }
                this.peticion.open(metodo, url, true);
                if(metodo == "POST")
                {
                    this.peticion.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                }
                this.peticion.send(valor);
            } catch(err) {
                this.error.call(this);
            }
            }
    },
    onReadyState: function() {
        var peticion = this.peticion;
        var listo = peticion.readyState;
        if(listo == superPeticion.COMPLETO) {
            var estado = peticion.status;
            if(estado == 200 || estado == 0) {
                this.onload.call(this);
            }else {
                this.error.call(this);
            }
        }
    },
    error: function() {
        alert("Se ha producido un error al obtener los datos"
        + "\n\nreadyState:" + this.peticion.readyState
        + "\nstatus: " + this.peticion.status
        + "\nheaders: " + this.peticion.getAllResponseHeaders());
    }
}
function inicio(final,valor,destino)
{
    if(destino != undefined)
    {
        var cargador = new superPeticion.cargaContenido("http://localhost/sistema-evaluacion/php/ajax.php","POST",final,valor,destino)
    }
    else
    {
        var cargador = new superPeticion.cargaContenido("http://localhost/sistema-evaluacion/php/ajax.php","POST",final,valor)
    }

}
