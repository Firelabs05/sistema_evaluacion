<?php
/*
Finalidad: Modulo que contiene la funciones sobre la seguridad y acceso

Implementacion: mostrar.php

Resumen: Libreria de funciones referente a la seguridad del sistema
*/
/*
*@seguridad_admin
*Revisa si el usuario es administrador
*Parametros: Ninguno 
*Return: Ninguno
*/
function seguridad_admin(){
    session_start();
    if ($_SESSION["admin"] != "SI"){
        header("Location: ../index.php");
    }
}

/*
*@seguridad_evaluador
*Revisa si el usuario es evaluador
*Parametros: Ninguno 
*Return: Ninguno
*/
function seguridad_evaluador(){
    session_start();
    if ($_SESSION["evador"] != "SI"){
        header("Location: ../index.php");
    }
}

/*
*@seguridad_evaluado
*Revisa si el usuario es evaluador
*Parametros: Ninguno 
*Return: Ninguno
*/
function seguridad_evaluado(){
    session_start();
    if ($_SESSION["evado"] != "SI"){
        header ("Location: ../index.php");
    }
}
?>
