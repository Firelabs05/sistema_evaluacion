<?php
/*
Finalidad: Modulo que contiene la funciones principales referentes a mostrar informacion de forma dinamica,
es el pilar central de todo el sistema

Implementacion: ajax.php, calificar_cues.php, calificar_metas.php, metas.php, nave_admin.php, reporte_g.php,
reporte.php, selección.php, insertar_meta.php, modi_meta.php

Resumen: Libreria de funciones referente a mostrar informacion de forma dinamica, tambien contiene algunas funciones generales
*/
require ('buscar.php'); // Sin estos no funciona
require ('seguridad.php'); 
//------------------------------------------------------------------------------------------------------------------------------
// GENERAL
/*
*@comp_fe
*Compara dos fechas, si la primera es mayor o igual regresa true si no, false
*Parametros: String 
*$f_a, Primera fecha a comparar
*$f_b, Segunda fecha a comparar
*Return: boolean
*/
function comp_fe($f_a,$f_b){
    $f_a = explode("-",$f_a);
    $f_b = explode("-",$f_b);
    foreach ($f_a as $key => $valor) {
        $f_a[$key] = intval($valor);
    }
    foreach ($f_b as $key => $valor) {
        $f_b[$key] = intval($valor);
    }
    if ($f_a[0] > $f_b[0]){
        return true;
    }
    else if($f_a[0] == $f_b[0]){
        if ($f_a[1] > $f_b[1]){
            return true;
        }
        else if ($f_a[1] == $f_b[1]){
            if ($f_a[2] < $f_b[2]){
                return false;}
            else{
                return true;
            }
        }
        else{
            return false;
        }
    }
    else{
        return false;
    }
}

/*
*@comp_fe_m
*Compara dos fechas, si la primera es mayor regresa true si no, false
*Parametros: String 
*$f_a, Primera fecha a comparar
*$f_b, Segunda fecha a comparar
*Return: boolean
*/
function comp_fe_m($f_a,$f_b)
{
    $f_a = explode("-",$f_a);
    $f_b = explode("-",$f_b);
    foreach ($f_a as $key => $valor) {
        $f_a[$key] = intval($valor);
    }
    foreach ($f_b as $key => $valor) {
        $f_b[$key] = intval($valor);
    }

    if ($f_a[0] > $f_b[0]){
        return true;
    }
    else if($f_a[0] == $f_b[0]){
        if ($f_a[1] > $f_b[1]){
            return true;
        }
        else if ($f_a[1] == $f_b[1]){
            if ($f_a[2] > $f_b[2]){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
    else{
        return false;
    }
}

/*
*@gregoriano
*Convierte una fecha en formato del calendario gregoriano
*Parametros: String 
*$fecha, fecha a convertir
*Return: String fecha
*/
function gregoriano($fecha){
    $fecha = explode("-",$fecha);
    $m = "";
    switch($fecha[1]){
        case "01":
            $m = "Enero";
            break;
        case "02":
            $m = "Febrero";
            break;
        case "03":
            $m = "Marzo";
            break;
        case "04":
            $m = "Abril";
            break;
        case "05":
            $m = "Mayo";
            break;
        case "06":
            $m = "Junio";
            break;
        case "07":
            $m = "Julio";
            break;
        case "08":
            $m = "Agosto";
            break;
        case "09":
            $m = "Septiembre";
            break;
        case "10":
            $m = "Octubre";
            break;
        case "11":
            $m = "Noviembre";
            break;
        case "12":
            $m = "Diciembre";
            break;
    }
    $fecha = $fecha[2]." de ".$m." del ".$fecha[0];
    return $fecha;
}

/*
*@nombre
*Crea una cadena con el nombre de un usuario
*Parametros: array 
*$e, empleado que se le desea convertir su nombre
*Return: String $nombre, cadena con el nombre del empleado
*/
function nombre($e){
    $nombre = $e['NOMBRE_EMP'].' '.$e['APELLIDO_P'].' '.$e['APELLIDO_M'];
    return $nombre;
}

/*
*@cabecera_panel
*Crea la parte de la cabecera de la mayoria de los pnales utilizados en el sistema
*Parametros: String
*$g, clases de stilos que se le quieran añadir
*$n_c, Numero identificador de la cabecera
*$n, Texto que desea que muestre la cabecera 
*Return: String $cadena
*/
function cabecera_panel($n_c,$n,$g){
    $cadena =
        '<div class="panel-group ficha-collapse '.$g.'" id="'.$n_c.'">
        <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="panel-title text-center">
        <a data-parent="#'.$n_c.'" data-toggle="collapse" href="#panel-'.$n_c.'" aria-expanded="true" aria-controls="panel-'.$n_c.'">
          '.$n.'
        </a>
        </h4>
        <button type="button" id = "mano'.$n_c.'"class="glyphicon glyphicon-eject collapsed mano" onclick = "mano(this.id)" data-parent="#'.$n_c.'" data-toggle="collapse" href="#panel-'.$n_c.'"></button>
        </div>
        <div class="panel-collapse collapse" id="panel-'.$n_c.'">
        <div class="panel-body mos-eva">
        <table class="table table-responsive table-bordered">';
        return $cadena;
}

/*
*@ma_cero
*Verifica que sea mayor que 0
*Parametros: Numerico
*$valor, numero a comparar
*Return: String 1 verdadero, 0 falso
*/
function ma_cero($valor){
    if($valor > 0){
        return "1";
    }
    else{
        return "0";
    }
}
// FIN GENERAL
// ----------------------------------------------------------------------------------------
// BARRA_NAV.PHP
/*
*@seleccionar_cues
*Muestra los cuestionarios que hay en un select
*Parametros: String
*$v, 0 o ID de un cuestionario
*Return: String $op, cadena con codigo HTML
*/
function seleccionar_cues($v){
    $cues = select(seleccionar_2_columnas("cuestionarios","ID_CUESTIONARIO", "NOMBRE_CUES"));
    $op = "";
    for($contador = 1; $contador <= mysqli_num_rows($cues); $contador++){
        $c = mysqli_fetch_assoc($cues);
        if($v!=0){
            if($c['ID_CUESTIONARIO']== $v){
                 $op .= '<option selected value = "'.$c['ID_CUESTIONARIO'].'">'.$c['NOMBRE_CUES'].'</option>';
            }
            else{
                $op .= '<option value = "'.$c['ID_CUESTIONARIO'].'">'.$c['NOMBRE_CUES'].'</option>';
            }
        }
        else{
            $op .= '<option value = "'.$c['ID_CUESTIONARIO'].'">'.$c['NOMBRE_CUES'].'</option>';
        }
    }
    return $op;
}

/*
*@seleccionar_dep
*Muestra las areas adscritas que hay en un select
*Parametros: String
*$v, 0 o ID de un area adscrita(departamento)
*Return: String $op, cadena con codigo HTML
*/
function seleccionar_dep($v){
    $re = select(seleccionar_todo("departamentos"));
    $op = "";
    for($contador = 1; $contador <= mysqli_num_rows($re); $contador++){
        $de = mysqli_fetch_assoc($re);
        if($v !== 0){
            if ($de['DEPARTAMENTO'] == $v){
                $op .= '<option selected value = "'.$de['ID_DEPARTAMENTO'].'">'.$de['DEPARTAMENTO'].'</option>';
            }
            else{
                $op .= '<option value = "'.$de['ID_DEPARTAMENTO'].'">'.$de['DEPARTAMENTO'].'</option>';
            }
        }
        else{
            $op .= '<option value = "'.$de['ID_DEPARTAMENTO'].'">'.$de['DEPARTAMENTO'].'</option>';
        }

    }
    return $op;
}
// FIN BARRA_NAV
// ---------------------------------------------------------------------------------
// ADMINISTRADOR.PHP
/*
*@mostrar_evaluaciones
*Busca la informacion de todas las evaluaciones y manda a llamar a quien las mostrara
*Parametros: Ninguno
*Return: Ninguno
*/
function mostrar_evaluaciones(){
    $evaluaciones = select(seleccionar_todo("evaluaciones"));
    for($contador = 1; $contador <= mysqli_num_rows($evaluaciones);$contador++){
        $eva = mysqli_fetch_assoc($evaluaciones);
        cuadro_evaluacion($contador,$eva);
    }
}

/*
*@cuadro_evaluaciones
*Muestra todas las evaluaciones existentes
*Parametros:
*String
*$nombre_cuadro, nombre del cuadro donde mostrara la informacion
*Array
*$eva, Informacion de la evaluacion
*Return: codigo HTML
*/
function cuadro_evaluacion($nombre_cuadro,$eva){
    // Parametros
    $nombre = $eva['NOMBRE_EVA']. " </br>Periodo: ".gregoriano($eva['FECHA_INICIO'])." / ".gregoriano($eva['FECHA_FIN']);
    $cues_o = mysqli_fetch_assoc(select(buscar_columna("NOMBRE_CUES", "cuestionarios", "ID_CUESTIONARIO", $eva['CUESTIONARIO_O'])));
    $cues_m = mysqli_fetch_assoc(select(buscar_columna("NOMBRE_CUES", "cuestionarios", "ID_CUESTIONARIO", $eva['CUESTIONARIO_M'])));
    $grilla = "col-md-12";
    // Fin parametros
    echo cabecera_panel($nombre_cuadro,$nombre,$grilla); // Manda a llamar a la cabecera
    echo'<tr>
          <td class="info-basi" colspan = "2"><p>Informacion basica</p></td>
          </tr>
          <tr>
          <td>Cuestionario de mando</td><td>'.$cues_m['NOMBRE_CUES'].'</td>
          </tr>
          <tr>
          <td>Cuestionario de operativo</td><td>'.$cues_o['NOMBRE_CUES'].'</td>
          </tr>
          </table>';
    if($eva['ESTADO_EVA'] == '0'){ // Verifica el estado de la evaluacion, para saber que botones mostrar
        echo '<div class="botones-visu btn-group col-xs-12" role="group">
        <button type = "button" value = "'.$eva['ID_EVALUACION'].'"  data-toggle="modal" data-target = "#modificar_evaluacion" onclick = "modi_eva(this.value)" class = "btn btn-primary col-xs-12">
        <span class = "glyphicon glyphicon-pencil"></span>
        Modificar
        </button>
        <button type ="button" class="btn btn-danger col-xs-12" value = "'.$eva['ID_EVALUACION'].'" data-toggle="modal" data-target="#eliminar" onclick = "pasar(\'eva_eli\',this.value)">
        <span class = "glyphicon glyphicon-remove"></span>
        Eliminar
        </button>
        </div>';
    }
    else if ($eva['ESTADO_EVA'] == '3'){
        echo '<div class="btn-solo"><button type="submit" value = "'.$eva['ID_EVALUACION'].'" name = "eva" class="btn btn-primary boton-visu">
              <span class = "glyphicon glyphicon-eye-open"></span>
              Reportes
              </button></div>';
    }
    else if ($eva['ESTADO_EVA'] == '1'){
        echo '<button type="button" data-toggle="modal" data-target="#modal_detalles" value = "'.$eva['ID_EVALUACION'].'" onclick="modal_deta(this.value);modal_deta2(this.value)" class="btn btn-primary boton-visu">
              <span class = "glyphicon glyphicon-eye-open"></span>
              Detalles
              </button>';
    }
    echo '</div>
          </div>
          </div>
          </div>
          </div>';
}
// FIN ADMINISTRADOR.PHP
//----------------------------------------------------------------------------
// USUARIOS.PHP
/*
*@mostrar_usuarios
*Busca la informacion de todas los usuarios y manda a llamar a quien las mostrara
*Parametros: String $tipo, que tipo de usuario debe de mostrar
*Return: Ninguno
*/
function mostrar_usuarios($tipo){
    $var = 0;
    if($tipo == "activo")
        $var = 1;
    $empleados = select(tabla_empleados());
    for($contador = 1; $contador <= mysqli_num_rows($empleados); $contador++){
        $empleado = mysqli_fetch_assoc($empleados);
        switch($tipo){
            case "todos":
                cuadro_usuario($contador, $empleado,$empleado['ESTADO']);
                break;
            default:
                if ($empleado['ESTADO'] == $var){
                    cuadro_usuario($contador,$empleado, $var);
                }
                break;
        }
    }
}

/*
*@cuadro_usuarios
*Muestra todos los usuarios pedidos
*Parametros:
*String
*$nombre_cuadro, nombre del cuadro donde mostrara la informacion
*Array
*$empleado, Informacion del empleado
*$tipo, tipo de empleado a mostrar
*Return: codigo HTML
*/
function cuadro_usuario($nombre_cuadro, $empleado,$tipo){
    echo '<div class="panel-group ficha-collapse " id="'.$nombre_cuadro.'">
        <div class="panel panel-default">
        <div class="panel-heading">';
    if($tipo == 1)
       echo '<p class="bola"></p>';
    else
        echo '<p class="bola-roj"></p>' ;

    echo '<h4 class="panel-title text-center">
        <a data-parent="#'.$nombre_cuadro.'" data-toggle="collapse" href="#panel-'.$nombre_cuadro.'" aria-expanded="true" aria-controls="panel-'.$nombre_cuadro.'">
          '.nombre($empleado).'
        </a>
        </h4>
        <button type="button" id = "mano'.$nombre_cuadro.'"class="glyphicon glyphicon-eject collapsed mano" onclick = "mano(this.id)" data-parent="#'.$nombre_cuadro.'" data-toggle="collapse" href="#panel-'.$nombre_cuadro.'"></button>
        </div>
        <div class="panel-collapse collapse" id="panel-'.$nombre_cuadro.'">
        <div class="panel-body mos-eva">
        <table class="table table-responsive table-bordered">
        <tr>
          <td class="info-basi" colspan = "4"><p>Datos del empleado</p></td>
          </tr>
          <tr>
          <td>No. empleado</td><td>'.$empleado['NO_EMPLEADO'].'</td>
          </tr>
          <tr>
          <td>Puesto</td><td>'.$empleado['PUESTO'].'</td>
          </tr>
          </table>
          <div class="btn-group botones-visu" role="group">'; //Llama a nombre para completar el nombre de empleado
    // Verifica que tipo de empleado es para mostrar diferentes botones
    if($tipo == 1){
        echo '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_modi_usu" onclick = "modi_usu(\''.$empleado['NO_EMPLEADO'].'\'); foco_btn()">
              <span class = "glyphicon glyphicon-pencil"></span>
              Modificar
              </button>
              <button type="button" class="btn btn-danger" value ="'.$empleado['NO_EMPLEADO'].'" data-toggle="modal" data-target="#modal_confirmar" onclick = "acti_usu(this.value,\'baja_usu\')">
              <span class = "glyphicon glyphicon-remove"></span>
              Baja
              </button>';
    }
    else if ($tipo == 0){
        echo '<button type="button" class="btn btn-primary boton-visu" value ="'.$empleado['NO_EMPLEADO'].'" data-toggle="modal" data-target="#modal_confirmar" onclick = "acti_usu(this.value,\'acti_usu\')">
              <span class = "glyphicon glyphicon-ok"></span>
              Activar
              </button>';
    }
    echo '</div>
          </div>
          </div>
          </div>
          </div>';
}

/*
*@busqueda_usu
*Busca en la base de datos al empleado que solicito el usuario
*Parametros: String 
*$cadena, usuario a buscar
*$tipo, que tipo de usuario debe de mostrar
*Return: Ninguno
*/
function busqueda_usu($cadena,$tipo){
    $var = 0;
    if($tipo == "activo")
        $var = 1;
    $empleados = select(tabla_empleados());
    for($contador = 1; $contador <= mysqli_num_rows($empleados); $contador++){
        $empleado = mysqli_fetch_assoc($empleados);
        switch($tipo){
            case "todos":
                if (preg_match("/$cadena/i", nombre($empleado))){
                    cuadro_usuario($contador, $empleado,$empleado['ESTADO']);
                }
                break;
            default:
                if ($empleado['ESTADO'] == $var){
                    if (preg_match("/$cadena/i", nombre($empleado))){
                        cuadro_usuario($contador,$empleado, $var);
                    }
                }
                break;
        }
    }
}
// FIN USUARIOS.PHP
//-----------------------------------------------------------------------------------
// CUESTIONARIOS.PHP
/*
*@mostrar_cuestionarios
*Busca la informacion de todas los cuestionarios y manda a llamar a quien las mostrara
*Parametros: Ninguno
*Return: Ninguno
*/
function mostrar_cuestionarios(){
    $cuestionarios = select(seleccionar_todo("cuestionarios"));
    for($contador = 1; $contador <= mysqli_num_rows($cuestionarios);$contador++){
        $cuestionario = mysqli_fetch_assoc($cuestionarios);
        cuadro_cuestionario($contador,$cuestionario);
    }
}

/*
*@cuadro_usuarios
*Muestra todos los usuarios pedidos
*Parametros:
*String
*$nombre_cuadro, nombre del cuadro donde mostrara la informacion
*Array
*$cues, Informacion del cuestionario
*Return: codigo HTML
*/
function cuadro_cuestionario($nombre_cuadro,$cues){
    echo cabecera_panel($nombre_cuadro,$cues['NOMBRE_CUES'],""); // Llama a al cabecera

    echo '<tr>
          <td class="info-basi" colspan ="2"><p>Informacion</p></td>
          </tr>
          <tr>
          <td>Fecha de creación</td><td>'.gregoriano($cues['FECHA_ING']).'</td>
          </tr>
          </table>
          <div class="btn-group botones-cues" role="group">'; // Llama a gregoriano para las fechas
    // Verifica el estado del cuestionario para saber que botones mostrar
    if($cues['ESTADO_CUES'] == '0'){
        echo '<button type="submit" value = "'. 0 .$cues['ID_CUESTIONARIO'].'" name = "tipo" class="btn btn-primary btn-uno">
              <span class = "glyphicon glyphicon-pencil"></span>
              Modificar
              </button>
              <button type = "button" value = "'.$cues['ID_CUESTIONARIO'].'" class="btn btn-danger btn-dos" onclick= "pasar(\'idCues\',this.value)" data-toggle="modal" data-target="#modal_eli_cues">
              <span class = "glyphicon glyphicon-remove"></span>
              Eliminar
              </button>';
    }
    echo '<button type="submit" value = "'. 1 .$cues['ID_CUESTIONARIO'].'" name = "tipo" ';
    if($cues['ESTADO_CUES'] == '0'){
        echo 'class="btn btn-primary btn-tres">';
    }
    else{
        echo 'class="btn btn-primary visu-cues-btn">';
    }
    echo '<span class = "glyphicon glyphicon-eye-open"></span>
          Visualizar
          </button>
          </div>
          </div>
          </div>
          </div>
          </div>';
}
// FIN CUESTIONARIO.PHP
//-------------------------------------------------------------
// VISUALIZAR_CUESTIONARIO.PHP, CALIFICAR_CUES.PHP

/*
*@mostrar_pregunta
*Busca la informacion de las preguntas y manda a llamar a quien mostrar las pregunta
*Parametros: String
*$id_cues, Id del cuestionario del que se mostraran las preguntas
*$codigo, decide si mostrarlo con las respuestas o con botones para modificarlas
*Return: Ninguno
*/
function mostrar_pregunta($id_cues,$codigo){
    $preguntas = select(buscar_2_columnas("preguntas", "ID_PREGUNTA", "PREGUNTA_CUES","ID_CUESTIONARIO",$id_cues));
    for($contador = 1; $contador<=mysqli_num_rows($preguntas); $contador++){
        $pregunta = mysqli_fetch_assoc($preguntas);
        echo "<div><span class='col-md-1'>".$contador.".-</span><span class='col-md-11' name = ".$contador."> ".$pregunta['PREGUNTA_CUES']."</span>";
        echo "<input type ='hidden' name = 'pregunta".$contador."' value = '".$pregunta['ID_PREGUNTA']."'>";
        if ($codigo == 1){
            mostrar_respuesta($contador); // Manda a llamar las respuestas
        }
        else{

            mostrar_boton($pregunta['ID_PREGUNTA']); // Manda a llamar los botones
        }
    }
    if($codigo != 1){
        echo '<button class="fin-modif-cues btn btn-primary" data-toggle="modal" data-target="#modal_nuevo" type = "button">Agregar pregunta</button>';
    }
}

/*
*@mostrar_respuestas
*Muestra unas respuestas predifinas con valores predifinidos
*Parametros: String
*$valor, ayuda a diferenciar las respuestas de otras
*Return: codigo html
*/
function mostrar_respuesta($valor){
    echo <<<EOT
    <div class="radio-califi">
    <label>
    <input type = "radio" name = "respuesta$valor" value = "10">10
    </label>
    <label>
    <input type = "radio" name = "respuesta$valor" value = "9" >9
    </label>
    <label>
    <input type = "radio" name = "respuesta$valor" value = "8">8
    </label>
    <label>
    <input type = "radio" name = "respuesta$valor" value = "7">7
    </label>
    <label>
    <input type = "radio" name = "respuesta$valor" value = "6">6
    </label>
    <label>
    <input type = "radio" name = "respuesta$valor" value = "5">5
    </label>
    <label>
    <input type = "radio" name = "respuesta$valor" value = "4">4
    </label>
    <label>
    <input type = "radio" name = "respuesta$valor" value = "3">3
    </label>
    <label>
    <input type = "radio" name = "respuesta$valor" value = "2">2
    </label>
    <label>
    <input type = "radio" name = "respuesta$valor" value = "1"  required>1
    </label>
    </div>
    <hr class="red">
    </div>
EOT;
}

/*
*@mostrar_boton
*Muestra botones para poder interactuar con las preguntas, modificar, eliminar
*Parametros: String
*$valor, Id de la pregunta
*Return: codigo html
*/
function mostrar_boton($valor){
    echo <<<EOT
    <div class="btn-modi-visu btn-group">
    <button type="button" class="btn btn-primary col-md-2 col-md-offset-5" data-toggle="modal" value = "$valor" data-target= "#modal_modificar" onclick = "modal_preg(this.value)">
    <span class = "glyphicon glyphicon-pencil"></span>
    Modificar
    </button>
    <button type="button" class="btn btn-danger col-md-2 col-md-offset-5" value = "$valor" data-toggle="modal" data-target="#modal_eliminar" onclick = "pasar('eIdPreg',this.value)">
    <span class = "glyphicon glyphicon-remove"></span>
    Eliminar
    </button>
    </div>
    <hr class="red col-md-12">
    </div>
EOT;
}
// FIN VISUALIZAR_CUESTIONARIO.PHP
//-----------------------------------------------------------------
// DEPARTAMENTO.PHP
/*
*@mostrar_departamentos
*Busca la informacion de todas los departamentos y manda a llamar a quien las mostrara
*Parametros: Ninguno
*Return: Ninguno
*/
function mostrar_departamentos(){
    $cadena = "SELECT DEPARTAMENTO FROM departamentos";
    $departamentos = select($cadena);
    for($contador = 1; $contador <= mysqli_num_rows($departamentos);$contador++){
        $departamento = mysqli_fetch_assoc($departamentos);
        cuadro_departamentos($contador,$departamento); // Manda a llamar quien los mostrara
    }
}

/*
*@cuadro_departamentos
*Muestra todos los departamentos, manda a llamar quien mostrara los puestos del departamento
*Parametros:
*String
*$nombre_cuadro, nombre del cuadro donde mostrara la informacion
*Array
*$departamento, Informacion del cuestionario
*Return: codigo HTML
*/
function cuadro_departamentos($nombre_cuadro,$departamento){
    // SE OCUPA
    echo cabecera_panel($nombre_cuadro,$departamento['DEPARTAMENTO'],""); // Llama a la cabecera del panel
    echo cuadro_puestos($departamento['DEPARTAMENTO']).'
          </table>
          </div>
          </div>
          </div>
          </div>'; // Manda a llamar quien mostrara todos los puestos
}

/*
*@cuadro_puestos
*Muestra todos los puestos y su informacion pertenecientes a el departamento
*Parametros:
*String
*$departamento, departamento al que pertenecen los puestos
*Return: String $puestos, Codigo HTML
*/
function cuadro_puestos($departamento){
    $puestos = "";
    $mando = "Mando";
    $tabla = select(tabla_puesto_departamento());
    for ($contador = 1; $contador <= mysqli_num_rows($tabla); $contador++){
        $fila = mysqli_fetch_assoc($tabla);
        if ($fila['NIVEL_MANDO'] == 0){
            $mando = "Operativo";
        }
        if ($fila['DEPARTAMENTO'] == $departamento){
            $puestos .= '<tr>
                <td class="info-basi" colspan = "2"><p>Puesto</p></td>
                </tr>
                <tr>
                <td>Puesto</td><td>'.$fila['PUESTO'].'</td>
                </tr>
                <tr>
                <td>Nivel de mando</td><td>'.$mando.'</td>
                </tr>
                <tr class="borde-abajo">
                <td>Puesto superior</td><td>'.$fila['PUESTO_SUP'].'</td>
                </tr>';
        }
    }
    return $puestos;
}
// FIN DEPARTAMENTO.PHP
//---------------------------------------------------------------------
// EVALUACION.PHP
/*
*@mostrar_eva_busqueda
*Busca al empleado evaluado solicitado por el usuario, y manda a llamar quien mostrara la informacion
*Parametros: String
*$id, Id de la evaluadio
*$cadena, usuario a buscar
*Return: Ninguno
*/
function mostrar_eva_busqueda($id,$cadena){
    $eva = select(tabla_evaluados($id));
    for($contador = 1; $contador <= mysqli_num_rows($eva); $contador++){
        $evaluado = mysqli_fetch_assoc($eva);
        if (preg_match("/$cadena/i", nombre($evaluado))){
            cuadro_evaluados($contador, $evaluado,$id); // Manda a llamar a quien lo mostrara
        }
    }
}

/*
*@mostrar_todos_evaluados
*Busca a todos los evaluados de una evaluacion y manda a llamar a quien los mostrara
*Parametros: String
*$id_eva, Id de la evaluadio
*Return: Ninguno
*/
function mostrar_todos_evaluados($id_eva){
    $eva = select(tabla_evaluados($id_eva));
    for($contador = 1; $contador <= mysqli_num_rows($eva); $contador++){
        $evaluado = mysqli_fetch_assoc($eva);
        cuadro_evaluados($contador, $evaluado,$id_eva); // Llama a quien lo mostrara
    }
}

/*
*@cuadro_evaluados
*Muestra los usuarios que hayan sido evaluados.
*Parametros:
*String
*$nombre_cuadro, nombre del cuadro donde mostrara la informacion
*Array
*$emp, Informacion del empleado
*$id, Id de la evaluacion
*Return: codigo HTML
*/
function cuadro_evaluados($nombre_cuadro, $emp, $id){
    if ($emp['TIPO_EMP'] == '1'){ // Dependiendo del tipo de empleado mostrara las metas
        $cadena = "SELECT CALIF_META FROM resultados_metas WHERE ID_EVALUACION = '$id' AND NO_EMPLEADO = '".$emp['NO_EMPLEADO']."'";
        $c = mysqli_fetch_assoc(select($cadena));
    }
    echo cabecera_panel($nombre_cuadro,nombre($emp),""); // Llama a cabecera
    echo '<tr>
          <td>No. de Empleado</td><td>'.$emp['NO_EMPLEADO'].'</td>
          </tr>
          <tr>
          <td>Puesto</td><td>'.$emp['PUESTO'].'</td>
          </tr>
          <tr>
          <td>Tipo de empleado</td><td>';
    // Dependiendo del tipo de empleado muestra informacion diferente
    if ($emp['TIPO_EMP']== '1'){
        echo "Mando";
    }
    else{
        echo "Operativo";
    }
    echo '</td>
          </tr>
          <tr>
          <td>Calificación Cuestionario</td><td>'.$emp['CALIF_CUES'].'</td>
          </tr>';
    if($emp['TIPO_EMP'] == '1'){
        echo '<tr>
              <td>Calificación Metas</td><td>'.$c['CALIF_META'].'</td>
              </tr>';
    }
    echo '</table>
          <div class="btn-evaluados col-xs-12 col-sm-12 col-md-12 col-lg-12" role="group">
          <button type="submit" name = "noemp" value = "1'.$emp['NO_EMPLEADO'].'" class="btn btn-primary">
          Reporte Cuestionario
          </button>';
    if($emp['TIPO_EMP'] == '1'){
        echo '<button type="submit" name = "noemp" value = "0'.$emp['NO_EMPLEADO'].'" class="btn btn-primary">
              Reporte Metas
              </button>';
    }
    echo '</div>
          </div>
          </div>
          </div>
          </div>';
}
// FIN EVALUACION.PHP
//------------------------------------------------------------------------
// SELECCION.PHP
/*
*@evaluador
*Verifica la seguridad y busca la informacion del usuario si existe,
*manda a llamar a las funciones para mostrar la barra
*Parametros: Ninguno
*Return: String
*/
function evaluador()
{
    if(isset($_SESSION['evador'])){
      seguridad_evaluador(); // Llama seguridad
    }
    if(isset($_SESSION['evado'])){
      seguridad_evaluado();  // Llama seguridad
    }
    session_start();
    $evador = mysqli_fetch_assoc(select(tabla_evaluador($_SESSION['noemp'])));
    barra_evaluador($evador,$_SESSION['noemp']); // Llama al que mostrara la informacion
    return $evador['PUESTO'];
}

/*
*@barra_evaluador
*Muestra la barra de informacion del usuario
*Parametros:
*Array
*$evador, arreglo con la informacion del empleado
*$noemp, numero de empleado
*Return: Codigo html
*/
function barra_evaluador($evador,$noemp){
    echo '<div class="area">
            <div>
            </div>
            <h1 class="titu-sele">Sistema de evaluación</h1>
            <div>
            </div>
          </div>
          <div class="perf">
            <div>
                <h5>'.nombre($evador).'</h5>
            </div>
            <div>
                <div>
                    <p>Puesto:</p>
                    <h5>'.$evador['PUESTO'].'</h5>
                </div>
                <div>
                    <p>No. empleado:</p>
                    <h5>'.$noemp.'</h5>
                </div>
                <div>
                    <p>Adscrito a:</p>
                    <h5>'.$evador['DEPARTAMENTO'].'</h5>
                </div>
            </div>
            <i class="glyphicon glyphicon-user foto"><a class="glyphicon glyphicon-log-out" href="../php/cerrar.php"></a></i>
          </div>';
}

/*
*@mostrar_subordinados
*busca la informacion de los empleados a evaluar y manda a llamar a quien lo mostrara
*Parametros:
*String
*$jefe, contiene el puesto del evaluador
*Return: Ninguno
*/
function mostrar_subordinados($jefe){
    $subordinados = select(tabla_subordinados($jefe));
    for($contador = 1; $contador <= mysqli_num_rows($subordinados); $contador++){
        $subordinado = mysqli_fetch_assoc($subordinados);
        cuadro_subordinado($subordinado); // Llama a quien mostrara la informacion
    }
}

/*
*@cuadro_subordinado
*busca la informacion de los empleados a evaluar y manda a llamar a quien lo mostrara
*Parametros:
*Array
*$empleado, Array con la informacion del empleado(subordinado)
*Return: codigo html
*/
function cuadro_subordinado($empleado){
    echo '<div>
          <p>'.nombre($empleado).'</p>
          <div>
          <table class="table table-responsive table-bordered">
          <tr>
          <td>No. empleado</td><td>'.$empleado['NO_EMPLEADO'].'</td>
          </tr>
          <tr>
          <td>Puesto</td><td>'.$empleado['PUESTO'].'</td>
          </tr>
          <tr>
          <td>Tipo de empleado</td><td>';
    if($empleado['TIPO_EMP'] == '1'){ // Dependiendo del tipo de empleado muestra informacion diferente
        echo "Mando";
    }
    else{
        echo "Operativo";
    }
    echo '</td>
          </table>
          </div>';
    switch($empleado['ESTADO_EMP']){
        case '0':
            if($empleado['TIPO_EMP'] == '1'){
                echo '<div class="btn-group-vertical boton-subor">
                      <button name = "cuest" type="submit" value = "'.$empleado['NO_EMPLEADO'].'" class="btn btn-primary">Cuestionario</button>
                      <button name = "meta" type="submit" value = "'.$empleado['NO_EMPLEADO'].'" class="btn btn-primary">Metas</button>
                      </div>';
            }
            else{
                echo '<div class="btn-group-vertical boton-subor">
                      <button name = "cuest" type="submit" value = "'.$empleado['NO_EMPLEADO'].'" class="btn btn-primary">Cuestionario</button>
                      </div>';
            }
            break;
        case '1':
            echo '<div>
              <p>Ya ha sido evaluado</p>
              </div>';
            break;
        case '2':
            echo '<div class="btn-group-vertical boton-subor">
                  <button name = "cuest" type="submit" value = "'.$empleado['NO_EMPLEADO'].'" class="btn btn-primary">Cuestionario</button>
                  <button type="button" class="btn btn-primary disabled" data-toggle="modal" data-target= "#mensaje">Metas</button>
                  </div>';
            break;
        case '3':
            echo '<div class="btn-group-vertical boton-subor">
                  <button type="button" class="btn btn-primary disabled" data-toggle="modal" data-target= "#mensaje">Cuestionario</button>
                  <button name = "meta" type="submit" value = "'.$empleado['NO_EMPLEADO'].'" class="btn btn-primary">Metas</button>
                  </div>';
            break;
        default:
            echo "ERROR ERROR";
    }
    echo '</div>';
}

/*
*@mostrar_cal
*Busca las calificaciones de los empleados y manda a llamar a quien lo muestra
*Parametros: Ninguno
*Return: Ninguno
*/
function mostrar_cal(){
    // nombre de la evaluacion, fechas,  nombre del cuestionario y calificacion
    $cue = select(resultados($_SESSION['noemp']));
    for($con = 1; $con <= mysqli_num_rows($cue); $con++){
        $c = mysqli_fetch_assoc($cue);
        cuadro_calif($c); // Llama a quien mostrara las calificaciones
    }
}

/*
*@cuadro_calif
*Muestra las calificaciones
*Parametros:
*Array
*$c, Array que contiene la informacion del cuestionario
*Return: Codigo html
*/
function cuadro_calif($c){
    echo '<div class="cuadro_calif">
    <p><span>'.$c['NOMBRE_EVA'].'</span></br>
    Periodo: '.gregoriano($c['FECHA_INICIO']).' / '.gregoriano($c['FECHA_FIN']).'</p>
    <p>Cuestionario</p>
    <table class="table table-responsive table-bordered">
    <tr>
    <td>Nombre del cuestionario:</td><td>'.$c['NOMBRE_CUES'].'</td>
    </tr>
    <tr>
    <td>Calificación cuestionario:</td><td>'.$c['CALIF_CUES'].'</td>
    </tr>
    <tr>
    <td>Fecha de calificación</td><td>'.gregoriano($c['FECHA_REA_CUES']).'</td>
    </tr>
    </table>';
    $m = select(resultados_m($_SESSION['noemp'],$c['ID_EVALUACION'])); // Verifica si tiene calificacion de metas
    if(mysqli_num_rows($m) > 0){
        $me = mysqli_fetch_assoc($m);
        echo '<p>Metas</p>
        <table class="table table-responsive table-bordered">
        <tr>
        <td>Califacación de las metas:</td><td>'.$me['CALIF_META'].'</td>
        </tr>
        <tr>
        <td>Fecha de calificación:</td><td>'.gregoriano($me['FECHA_CALIF']).'</td>
        </tr>
        </table>';
    }
    echo '</div>
    </div>';
}
// FIN DE SELECCION
//--------------------------------------------------------------------------------------------------------------
// METAS.PHP, CALIFICAR_METAS.PHP
/*
*@mostrar_metas
*Busca las metas de los empleados y manda a llamar a quien lo muestra
*Parametros: String
*$noemp, numero del empleado
*$valor, si sera para mostrar o para calificar, 0 o 1
*Return: Integer, numero de metas
*/
function mostrar_metas($noemp,$valor){
    $metas = select(buscar_metas($noemp));
    for ($contador = 1; $contador <= mysqli_num_rows($metas); $contador++){
        $meta = mysqli_fetch_assoc($metas);
        cuadro_metas($contador,$meta,$valor); // Llama a quien lo mostrara
    }
    return mysqli_num_rows($metas);
}

/*
*@cuadro_metas
*Muestra las metas del empleado, llama a quien mostrara los compromisos
*Parametros: String
*$nombre, numero de meta
*$valor, 0 o 1
*Array
*$meta, Contiene toda la informacion de la meta  
*Return: Ninguno
*/
function cuadro_metas($nombre,$meta,$valor){
    echo '<div class="metas-conjunto"><div class="meta-mostrada">
          <p><span>Meta '.$nombre.'<br></span>'.$meta['META'].'</p>';
    echo '</div>
          <div class="compromiso-mostrado">
          '.mostrar_compromisos($meta['ID_META'],$valor).'
          </div>'; // Llama para mostrar los compromisos de la meta
    if ($valor == 0){ // Dependiendo del valor muestra botones o no{
        echo '<div class="btn-group-vertical">
              <button type="button" class="btn btn-primary" data-toggle="modal" value = "'.$meta['ID_META'].'" data-target= "#modi_meta" onclick = "pasar(\'b_modi_meta\',this.value)">
              <span class = "glyphicon glyphicon-pencil"></span>
              </button>
              <button type="button" class="btn btn-danger" value = "'.$meta['ID_META'].'" data-toggle="modal" data-target="#eli_meta" onclick = "pasar(\'b_eli_me\',this.value)">
              <span class = "glyphicon glyphicon-remove"></span>
              </button>
              </div>';
    }
    else{
        echo '<div>
              '.res_meta($nombre).'
              <input type= "hidden" name = "id_meta'.$nombre.'" value = "'.$meta['ID_META'].'">
              </div>'; // Llama para mostrar las respuestas
    }
    echo '</div>';
}

/*
*@mostrar_compromisos
*Muestra los compromisos de la meta y las fechas
*Parametros: String
*$id, id de la meta,
*$v, 1 o 0
*Return: String, codigo html
*/
function mostrar_compromisos($id,$v){
    $cuadro = "";
    $comps = select(select_comp($id));
    for($contador = 1; $contador <= mysqli_num_rows($comps);$contador++){
        $comp = mysqli_fetch_assoc($comps);
        $cuadro .= '<div><div>
                    <p><span>Compromiso '.$contador.'<br></span>'.$comp['COMPROMISO'].'</p>';
        if ($v == 0){
            $cuadro .= '<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" value = "'.$comp['ID_COMPROMISO'].'" data-target= "#modi_comp" onclick = "pasar(\'b_modi_comp\',this.value)">
                        <span class = "glyphicon glyphicon-pencil"></span>
                        </button>';
        }
        $cuadro .= '</div>
                    <div>
                    <p>'.gregoriano($comp['FECHA_COMP']).'</p>';

        if($v == 0){
            $cuadro .= '<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" value = "'.$comp['ID_COMPROMISO'].'" data-target= "#modi_fe" onclick = "pasar(\'b_modi_fe\',this.value)">
                        <span class = "glyphicon glyphicon-pencil"></span>
                        </button>
                        <button type="button" class="btn btn-danger ';
            if(mysqli_num_rows($comps) == 1){
                $cuadro .= 'disabled">';
            }
            else{
                $cuadro .= '" value = "'.$comp['ID_COMPROMISO'].'" data-toggle="modal" data-target="#eli_comp" onclick = "pasar(\'b_eli_c\',this.value)">';
            }

                $cuadro .= '<span class = "glyphicon glyphicon-remove"></span>
                            </button>';
        }
        $cuadro .= '</div>
                    </div>';
    }
    if($v == 0){
        $cuadro .= '<div>
                    <button type="button" class="btn btn-primary';
        if(mysqli_num_rows($comps) == 3){
            $cuadro .= ' disabled" >';
        }
        else{
            $cuadro .= '" data-toggle="modal" value = "'.$id.'" data-target= "#agre_comp" onclick = "pasar(\'b_agre_comp\',this.value)">';
        }
            $cuadro .= '<span class = "glyphicon glyphicon-plus"></span>
                        </button>
                        </div>';
    }
    return $cuadro;
}

/*
*@res_meta
*Muestra las respuestas para calificar las metas
*Parametros: String
*$valor, para identificar las respuestas
*Return: String, codigo html
*/
function res_meta($valor){
     $respu = <<<EOT
    <label>
        <input type = "radio" name = "respuesta$valor" value = "5" required>Sobresaliente
    </label>
    <label>
        <input type = "radio" name = "respuesta$valor" value = "4">Satisfactorio
    </label>
    <label>
        <input type = "radio" name = "respuesta$valor" value = "3">Mínimo Aprobatorio
    </label>
    <label>
        <input type = "radio" name = "respuesta$valor" value = "2">No Aprobatorio
    </label>
    <label>
        <input type = "radio" name = "respuesta$valor" value = "1">No Aplica
    </label>
    <hr class="red">
EOT;
    return $respu;
}
// FIN METAS.PHP
?>
