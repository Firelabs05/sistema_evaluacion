<?php
/*
Finalidad: Cerrar la sesion abierta
Implementacion: nave_admin.php, mostrar.php

Resumen: Cierra la sesion abierta y retonar al index.php
*/
session_start();
session_destroy();
header ("Location: ../index.php");
?>