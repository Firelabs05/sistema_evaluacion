<?php
/*
Finalidad: Modulo que recibe la informacion del login del usuario y revisa que tipo de usuario es

Implementacion: ajax.php

Resumen: Verifica si el usuario es valido y a que tipo pertenece
*/
require ('tiempo.php'); // Sin este no funciona
comprobar(); // Comprueba si hay una evaluacion activa
session_start();
// Parametros
$pas = $_POST['password'];
$usu = select(tabla_usuario($_POST['usuario']));
$eva = select(buscar_columna("ID_EVALUACION", "evaluaciones", "ESTADO_EVA", '1'));
// Fin parametros
if (mysqli_num_rows($usu)> 0){
    $usu = mysqli_fetch_assoc($usu);
    if (password_verify($pas, $usu['PASSWORD']) && $usu['ESTADO'] == '1'){
        if ($usu['TIPO_EMP'] == '1'){ // Evaluador
            if(mysqli_num_rows($eva) > 0){
                $eva = mysqli_fetch_assoc($eva);
                $_SESSION['id_eva'] = $eva['ID_EVALUACION'];
            }
            $_SESSION['evador'] = "SI";
            $_SESSION['noemp'] = $usu['NO_EMPLEADO'];
            echo "2";
        }
        else{ // Evaluado
            $_SESSION['evado'] = "SI";
            $_SESSION['noemp'] = $usu['NO_EMPLEADO'];
            echo "2";
        }                
    }
    else{        
        echo "1";
    }
}
else{
    $admin = select(buscar_columna("PASSWORD","usuarios_admin","USUARIO",$_POST['usuario']));
    if($admin){ // Administrador
        $admin = mysqli_fetch_assoc($admin);
        if ($_POST['password'] == $admin['PASSWORD']){
            $_SESSION['admin'] = "SI";
            echo "3";
        }
        else{
            echo "1";
        }
    }
    else{
        echo "1";
    }
}
?>