<?php
/*
Finalidad: Modulo que tiene todas las funciones referentes a buscar informacion en la base de datos
Implementacion: agregar_calificacion.php ,calif_meta.php ,insertar_cuestionario.php, mostrar.php 

Resumen: Es una libreria de funciones que se ocupan solo para buscar informacion o referentes a la busqueda de esta.
Tambien tiene funciones para insertar datos
*/
require ('conexion.php'); // Sin esto no funciona

/*
*@insertar
*Inserta informacion en la base de datos
*Parametros: String $insert codigo SQL
*Return: String 1 error, 0 todo correcto
*/
function insertar($insert){
   
    $conexion = conexion_abrir();
    mysqli_query($conexion, $insert);

    if(mysqli_error($conexion)){
       return "1";
    }
    else{
        return "0";
    }
}

/*
*@insertar_con
*Inserta informacion en la base de datos
*Parametros: String $insert codigo SQL
*Return: boolean false, $conexion todo correcto
*/
function insertar_con($insert){
    $conexion = conexion_abrir();
    mysqli_query($conexion, $insert);

    if(mysqli_error($conexion)){
       return false;
    }
    else{
        return $conexion;
    }
}

/*
*@seleccionar_todo
*Devuelve una cadena de codigo SQL
*Parametros: String $tabla, tabla de la base de datos
*Return: String $resultado, codigo SQL
*/
function seleccionar_todo($tabla){
    // SE OCUPA PUESTOS.PHP, REGISTRO.PHP,MODFIICAR_USUARIO.PHP,
    $resultado = "SELECT * FROM $tabla";

    return $resultado;
}

/*
*@seleccionar_2_columnas
*Devuelve una cadena de codigo SQL
*Parametros: String 
*$tabla tabla de la base de datos
*$columna1,$columna2, columnas a seleccionar de la base de datos
*Return: String $resultado, codigo SQL
*/
function seleccionar_2_columnas($tabla,$columna1,$columna2){
    // SE OCUPA PUESTOS MOSTRAR PUESTOS, VISUALIZAR PREGUNTAS, MOSTRAR CUESTIONARIOS, INSERTAR_EVALUACION.PHP, MODIFICAR_EVA.PHP
    $resultado = "SELECT $columna1, $columna2 FROM $tabla";

    return $resultado;
}

/*
*@select
*Busca la informacion en la base de datos
*Parametros: String $busqueda, codigo SQL
*Return: Array $resultado 
*/
function select($busqueda){
    $conexion = conexion_abrir();
    $resultado = mysqli_query($conexion, $busqueda);

    if($resultado == false)
        echo mysqli_error($conexion);
    return $resultado;
}

/*
*@buscar_columna
*Devuelve una cadena de codigo SQL
*Parametros: String $pedir_columna, columna a buscar en la BD
*$tabla, tabla de la BD
*$columna, columna para comparar la informacion
*$dato, informacion a buscar
*Return: String $resultado, codigo SQL
*/
function buscar_columna($pedir_columna,$tabla,$columna,$dato){
    // SE OCUPA EN VISUALIZAR_CUESTIONARIO.PHP, ADMINISTRADOR.PHP, SESION.PHP, AGREGAR_CALIFICACION.PHP, CALIF_METAS.PHPs
    $resultado = "SELECT $pedir_columna FROM $tabla WHERE $columna = '$dato'";

    return $resultado;
}

/*
*@tabla_empleados
*Devuelve una cadena de codigo SQL
*Parametros: Ninguno
*Return: String $resultado, codigo SQL
*/
function tabla_empleados(){
    // SE OCUPA USUARIOS.PHP
    $resultado = naap()." e.NO_EMPLEADO, u.ESTADO ".Fep()." INNER JOIN usuarios u ON e.ID_PUESTO = p.ID_PUESTO AND e.NO_EMPLEADO = u.NO_EMPLEADO";

    return $resultado;
}

/*
*@tabla_modificar
*Devuelve una cadena de codigo SQL
*Parametros: String $noemp, numero de empleado
*Return: String $resultado, codigo SQL
*/
function tabla_modificar($noemp){
    // SE OCUPA MODIFICAR_USUARIOS.PHP
    $resultado = naap().crd().",d.ID_DEPARTAMENTO, e.ID_PUESTO, CORREO, FECHA_INGRESO, u.USUARIO ".Fep()." INNER JOIN usuarios u INNER JOIN departamentos d ON e.NO_EMPLEADO = '$noemp' AND p.ID_PUESTO = e.ID_PUESTO AND d.ID_DEPARTAMENTO = p.ID_DEPARTAMENTO AND u.NO_EMPLEADO = '$noemp'";

    return $resultado;
}

/*
*@buscar_2_columnas
*Devuelve una cadena de codigo SQL
*Parametros: String 
*$tabla, tabla de la BD
*$columna, columna a pedir
*$columna, columna a pedir
*$dato1, columna a comparar
*$dato2, informacion a comparar
*Return: String $resultado, codigo SQL
*/
function buscar_2_columnas($tabla,$columna1,$columna2,$dato1,$dato2){
    // SE OCUPA MODIFICAR_EVA.PHP,
    $resultado = "SELECT $columna1, $columna2 FROM $tabla WHERE $dato1 = '$dato2'";

    return $resultado;
}

/*
*@tabla_puesto_departamento
*Devuelve una cadena de codigo SQL
*Parametros: Ninguno
*Return: String $resultado, codigo SQL
*/
function tabla_puesto_departamento(){
    // SE OCUPA DEPARTAMENTO.PHP
    $resultado = "SELECT d.DEPARTAMENTO, p.PUESTO, p.NIVEL_MANDO, p.PUESTO_SUP FROM puestos p INNER JOIN departamentos d ON p.ID_DEPARTAMENTO = d.ID_DEPARTAMENTO";

    return $resultado;
}

/*
*@tabla_usuario
*Devuelve una cadena de codigo SQL
*Parametros: String $usuario, el usuario proporcionado por el usuario
*Return: String $resultado, codigo SQL
*/
function tabla_usuario($usuario){
    // SE OCUPA SESION.PHP
    $resultado = "SELECT e.NO_EMPLEADO, TIPO_EMP, u.USUARIO, PASSWORD, ESTADO FROM empleados e INNER JOIN usuarios u ON e.NO_EMPLEADO = u.NO_EMPLEADO AND u.USUARIO = '$usuario'";

    return $resultado;
}

/*
*@tabla_evaluador
*Devuelve una cadena de codigo SQL
*Parametros: String $noemp, Numero de empleado
*Return: String $resultado, codigo SQL
*/
function tabla_evaluador($noemp){
    // SE OCUPA SELECCION.PHP
    $resultado = naap()." d. DEPARTAMENTO ".Fep()." INNER JOIN departamentos d ON NO_EMPLEADO = '$noemp' AND e.ID_PUESTO = p.ID_PUESTO AND p.ID_DEPARTAMENTO = d.ID_DEPARTAMENTO";

    return $resultado;
}

/*
*@tabla_subordinados
*Devuelve una cadena de codigo SQL
*Parametros: String $puesto, Puesto del jefe
*Return: String $resultado, codigo SQL
*/
function tabla_subordinados($puesto){
    // SE OCUPA SELECCION.PHP
    $resultado = naap()." e.NO_EMPLEADO, ESTADO_EMP, TIPO_EMP ".Fep()." INNER JOIN usuarios u ON p.PUESTO_SUP = '$puesto' AND e.NO_EMPLEADO = u.NO_EMPLEADO AND e.ID_PUESTO = p.ID_PUESTO AND u.ESTADO = '1'";

    return $resultado;
}

/*
*@subordinado
*Devuelve una cadena de codigo SQL
*Parametros: String $noemp, Numero de empleado
*Return: String $resultado, codigo SQL
*/
function subordinado($noemp){
    // SE OCUPA CALIFICAR_CUES.PHP
    $resultado = naap()." e.ID_PUESTO, d.DEPARTAMENTO,TIPO_EMP ".Fep()." INNER JOIN departamentos d ON e.NO_EMPLEADO = '$noemp' AND e.ID_PUESTO = p.ID_PUESTO AND p.ID_DEPARTAMENTO = d.ID_DEPARTAMENTO";

    return $resultado;
}

/*
*@selec_cues
*Devuelve una cadena de codigo SQL
*Parametros: String $id, id de la evaluacion
*$cues, dos valores posibles M,O
*Return: String $resultado, codigo SQL
*/
function selec_cues($id,$cues){
    // SE OCUPA CALIFICAR_CUES.PHP
    $resultado = "SELECT NOMBRE_CUES, c.ID_CUESTIONARIO FROM cuestionarios c, evaluaciones e WHERE e.ID_EVALUACION = '$id' AND e.CUESTIONARIO_$cues = c.ID_CUESTIONARIO";

    return $resultado;
}

/*
*@selec_comp
*Devuelve una cadena de codigo SQL
*Parametros: String $id, id de la meta
*Return: String $resultado, codigo SQL
*/
function select_comp($id){
    // SE OCUPA METAS.PHP
    $resultado = "SELECT ID_COMPROMISO, COMPROMISO, FECHA_COMP FROM compromisos WHERE ID_META = '$id'";

    return $resultado;
}

/*
*@selec_cues
*Devuelve una cadena de codigo SQL
*Parametros: String $noemp, numero de empleado
*Return: String $resultado, codigo SQL
*/
function buscar_metas($noemp){
    // SE OCUPA METAS.PHP
    $resultado = "SELECT ID_META, META FROM metas WHERE NO_EMPLEADO = '$noemp' AND ESTADO_META = '0'";

    return $resultado;
}

/*
*@buscar_eva
*Devuelve una cadena de codigo SQL
*Parametros: String $id, id de la evaluacion
*Return: String $resultado, codigo SQL
*/
function buscar_eva($id){
    // SE OCUPA EVALUACION.PHP
    $resultado = "SELECT NOMBRE_EVA, FECHA_INICIO, FECHA_FIN, ESTADO_EVA, CUESTIONARIO_O, CUESTIONARIO_M FROM evaluaciones WHERE ID_EVALUACION = '$id'";

    return $resultado;
}

/*
*@tabla_evaluados
*Devuelve una cadena de codigo SQL
*Parametros: String $id, id de la evaluacion
*Return: String $resultado, codigo SQL
*/
function tabla_evaluados($id){
    // SE OCUPA EVALUACION.PHP
    $resultado = naap()." e.NO_EMPLEADO, TIPO_EMP, CALIF_CUES FROM empleados e INNER JOIN resultados_cues r_c INNER JOIN puestos p WHERE e.NO_EMPLEADO = r_c.NO_EMPLEADO AND e.ID_PUESTO = p.ID_PUESTO AND r_c.ID_EVALUACION = '$id'";

    return $resultado;
}

/*
*@tabla_reporte
*Devuelve una cadena de codigo SQL
*Parametros: String $id_eva, id de la evaluacion
*$noemp, numero de empleado
*Return: String $resultado, codigo SQL
*/
function tabla_reporte($id_eva,$noemp){
    // SE OCUPA REPORTE.PHP
    $resultado = naap().crd().", e.TIPO_EMP, ev.NOMBRE_EVA, ev.FECHA_INICIO, ev.FECHA_FIN, rc.ID_RESULTADO_CUES, c.NOMBRE_CUES, rc.CALIF_CUES, rc.FECHA_REA_CUES, rc.NO_EMP_EVA FROM evaluaciones ev INNER JOIN resultados_cues rc INNER JOIN cuestionarios c INNER JOIN empleados e INNER JOIN puestos p INNER JOIN departamentos d WHERE ev.ID_EVALUACION ='$id_eva' AND rc.ID_EVALUACION = '$id_eva' AND rc.NO_EMPLEADO = '$noemp' AND rc.ID_CUESTIONARIO = c.ID_CUESTIONARIO AND rc.ID_PUESTO = p.ID_PUESTO and p.ID_DEPARTAMENTO = d.ID_DEPARTAMENTO AND e.NO_EMPLEADO = '$noemp'";

    return $resultado;
}

/*
*@tabla_metas
*Devuelve una cadena de codigo SQL
*Parametros: String $id_eva, id de la evaluacion
*$noemp, numero de empleado
*Return: String $resultado, codigo SQL
*/
function tabla_metas($id_eva, $noemp){
    // SE OCUPA REPORTE.PHP
    $resultado = naap().crd().", e.TIPO_EMP, ev.NOMBRE_EVA, ev.FECHA_INICIO, ev.FECHA_FIN, rm.ID_RESULTADO_META, rm.CALIF_META, rm.FECHA_CALIF, rm.NO_EMP_EVA FROM evaluaciones ev INNER JOIN resultados_metas rm INNER JOIN empleados e INNER JOIN puestos p INNER JOIN departamentos d ON ev.ID_EVALUACION ='$id_eva' AND rm.ID_EVALUACION = '$id_eva' AND rm.NO_EMPLEADO = '$noemp' AND rm.ID_PUESTO = p.ID_PUESTO and p.ID_DEPARTAMENTO = d.ID_DEPARTAMENTO AND e.NO_EMPLEADO = '$noemp'";

    return $resultado;
}

/*
*@tabla_r_metas
*Devuelve una cadena de codigo SQL
*Parametros: String $id, id de los resultados de las metas
*Return: String $resultado, codigo SQL
*/
function tabla_r_metas($id){
    // SE OCUPA REPORTE.PHP
    $resultado = "SELECT m.META, rm.CALIF_META FROM respuestas_metas rm INNER JOIN metas m ON rm.ID_RESULTADO_META = '$id' AND rm.ID_META = m.ID_META";

    return $resultado;
}

/*
*@tabla_preguntas
*Devuelve una cadena de codigo SQL
*Parametros: String $id, id de los resultados de las preguntas
*Return: String $resultado, codigo SQL
*/
function tabla_preguntas($id){
    // SE OCUPA REPORTE.PHP
    $resultado = "SELECT p.PREGUNTA_CUES, r.RESULTADO FROM resultados_preguntas r INNER JOIN preguntas p ON r.ID_RESULTADO_CUES = '$id' AND r.ID_PREGUNTA = p.ID_PREGUNTA";

    return $resultado;
}

/*
*@tabla_evalor
*Devuelve una cadena de codigo SQL
*Parametros: String $noemp, numeor de empleado
*Return: String $resultado, codigo SQL
*/
function tabla_evalor($noemp){
    // SE OCUPA REPORTE.PHP
    $resultado = naap().crd()." ".Fep()." INNER JOIN departamentos d ON e.NO_EMPLEADO = '$noemp' AND e.ID_PUESTO = p.ID_PUESTO and p.ID_DEPARTAMENTO = d.ID_DEPARTAMENTO";

    return $resultado;
}

/*
*@naap
*Devuelve una cadena de codigo SQL
*Parametros: Ninguno
*Return: String $cadena, codigo SQL
*/
function naap(){
    $cadena = "SELECT e.NOMBRE_EMP, APELLIDO_P, APELLIDO_M, p.PUESTO,";
    return $cadena;
}

/*
*@crd
*Devuelve una cadena de codigo SQL
*Parametros: Ninguno
*Return: String $cadena, codigo SQL
*/
function crd(){
    $cadena = " e.CURP, e.RFC, d.DEPARTAMENTO";
    return $cadena;
}

/*
*@Fep
*Devuelve una cadena de codigo SQL
*Parametros: Ninguno
*Return: String $cadena, codigo SQL
*/
function Fep(){
    $cadena = "FROM empleados e INNER JOIN puestos p";
    return $cadena;
}

/*
*@prueba
*Devuelve una cadena de codigo SQL
*Parametros: Ninguno
*Return: String $cadena, codigo SQL
*/
function prueba(){
    $cadena = "SELECT e.NO_EMPLEADO FROM empleados e INNER JOIN usuarios u ON e.NO_EMPLEADO = u.NO_EMPLEADO AND u.ESTADO = '1'";
    return $cadena;
}

/*
*@prueba2
*Devuelve una cadena de codigo SQL
*Parametros: String $dato, numero de empleado
*Return: String $cadena, codigo SQL
*/
function prueba2($dato){
    $cadena = naap()." p.PUESTO_SUP ".Fep()." ON e.ID_PUESTO = p.ID_PUESTO AND e.NO_EMPLEADO = '$dato'";
    return $cadena;
}

/*
*@resultados
*Devuelve una cadena de codigo SQL
*Parametros: String $noemp, numero de empleado
*Return: String $cadena, codigo SQL
*/
function resultados($noemp){
    $cadena = "SELECT v.NOMBRE_EVA, v.FECHA_INICIO, v.FECHA_FIN, c.NOMBRE_CUES, rc.CALIF_CUES, rc.ID_EVALUACION, rc.FECHA_REA_CUES FROM resultados_cues rc INNER JOIN evaluaciones v INNER JOIN cuestionarios c ON rc.NO_EMPLEADO = '$noemp' AND rc.ID_CUESTIONARIO = c.ID_CUESTIONARIO AND rc.ID_EVALUACION = v.ID_EVALUACION";
    return $cadena;
}

/*
*@resultados_m
*Devuelve una cadena de codigo SQL
*Parametros: String $noemp, numero de empleado
*$id, id de la evaluacion
*Return: String $cadena, codigo SQL
*/
function resultados_m($noemp,$id){
    $cadena = "SELECT CALIF_META, FECHA_CALIF FROM resultados_metas WHERE ID_EVALUACION = '$id' AND NO_EMPLEADO = '$noemp'";
    return $cadena;
}

/*
*@super_reporte
*Devuelve una cadena de codigo SQL
*Parametros: String $id, id de la evaluacion
*Return: String $cadena, codigo SQL
*/
function super_reporte($id){
    $cadena = "SELECT e.NOMBRE_EMP, e.APELLIDO_P, e.APELLIDO_M, e.NO_EMPLEADO, c.NOMBRE_CUES, rc.CALIF_CUES, rc.FECHA_REA_CUES FROM resultados_cues rc INNER JOIN empleados e INNER JOIN cuestionarios c ON rc.NO_EMPLEADO = e.NO_EMPLEADO AND rc.ID_CUESTIONARIO = c.ID_CUESTIONARIO AND rc.ID_EVALUACION = '$id'";
    return $cadena;
}

/*
*@super_reporte
*Devuelve una cadena de codigo SQL
*Parametros: String $id, id de la evaluacion
*Return: String $cadena, codigo SQL
*/
function super_reporte_metas($id){
    $cadena = "SELECT e.NOMBRE_EMP, e.APELLIDO_P, e.APELLIDO_M, e.NO_EMPLEADO, rm.COMENTARIO_META, rm.CALIF_META, rm.FECHA_CALIF FROM resultados_metas rm INNER JOIN empleados e ON rm.NO_EMPLEADO = e.NO_EMPLEADO AND rm.ID_EVALUACION = '$id'";
    return $cadena;
}

/*
*@fechas_evas
*Devuelve una cadena de codigo SQL
*Parametros: Ninguno
*Return: String $cadena, codigo SQL
*/
function fechas_evas(){
    $cadena = "SELECT FECHA_FIN, FECHA_INICIO, ID_EVALUACION FROM evaluaciones";
    return $cadena;
}
?>
