<?php
/*
Finalidad: Agregar las metas en la base de datos
Implementacion: agregar_cuestionario.php

Resumen: Recibe la informacion de las metas del usuario, y lo guarda junto con sus compromisos
*/
require ('mostrar.php'); // Sin esto no funciona

$conexion = conexion_abrir();
session_start();
// Parametros
$meta = $_POST['meta'];
$fecha = date('Y-m-d');
$no_emp = $_SESSION['noemp'];
// Fin Parametros

// Introduce la meta en la base de datos
mysqli_query($conexion, "INSERT INTO metas (NO_EMPLEADO, META, FECHA_INTRO,ESTADO_META) VALUES ('$no_emp','$meta','$fecha','0')");

if (mysqli_error($conexion)){
    echo mysqli_error($conexion);
}
else{
    $id_meta = mysqli_insert_id($conexion);
    for($contador = 1; $contador <=$_POST['final']; $contador++){
        if (isset($_POST['compromiso'.$contador])){
            // Parametros
            $compromiso = $_POST['compromiso'.$contador];
            $fecha = $_POST['fecha'.$contador];
            // Fin parametros
            // Agrega compromisos de las metas
            mysqli_query($conexion, "INSERT INTO compromisos(ID_META, COMPROMISO, FECHA_COMP) VALUES ('$id_meta', '$compromiso','$fecha')");

            if (mysqli_error($conexion)){
                echo mysqli_error($conexion);
            }
        }
    }
    header('location: ../html/metas.php');
}
?>
