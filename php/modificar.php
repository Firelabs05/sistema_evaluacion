<?php
/*
Finalidad: Modulo que contiene la funciones principales referentes a modificar informacion
Implementacion: ajax.php

Resumen: Libreria de funciones referente a modificar informacion.
*/
/*
*@modi_usu_da_pe
*Modifica la informacion personal de un usuario
*Parametros: String 
*$nombre, Nombre del usuario
*$apellidoP, Apellido paterno del usuario
*$apellidoM, Apellido materno del usuario
*$rfc, RFC del usuario
*$curp, CURP del usuario
*$noemp, Numero de empleado
*$correo, correo electronico del usuario
*$valor, Numero del empleado a modificar
*Return: String 1 error, 0 todo correcto
*/
function modi_usu_da_pe($nombre,$apellidoP,$apellidoM,$rfc,$curp,$noemp,$correo,$valor){
$rfc = strtoupper($rfc);
$curp = strtoupper($curp);
$cadena = "UPDATE empleados SET NOMBRE_EMP = '$nombre', APELLIDO_P = '$apellidoP', APELLIDO_M = '$apellidoM', RFC = '$rfc' , CURP = '$curp', NO_EMPLEADO = '$noemp', CORREO = '$correo' WHERE NO_EMPLEADO = '$valor'";
return insertar($cadena);
}

/*
*@modi_usu_da_ta
*Modifica la informacion de trabajo de un usuario
*Parametros: String 
*$fechaingre, Fecha de ingreso
*$puesto, Puesto del usuario
*$valor, Numero del empleado a modificar
*Return: String 1 error, 0 todo correcto
*/
function modi_usu_da_ta($fechaingre,$puesto,$valor){
    $cadena = "SELECT NIVEL_MANDO FROM puestos WHERE ID_PUESTO = '$puesto'";
    $tipo = mysqli_fetch_assoc(select($cadena));
    if ($tipo['NIVEL_MANDO'] == '0'){
        $tipo_emp = 0;
    }
    else{
        $tipo_emp = 1;
    }
    $cadena = "UPDATE empleados SET TIPO_EMP = '$tipo_emp', FECHA_INGRESO = '$fechaingre', ID_PUESTO = '$puesto' WHERE NO_EMPLEADO = '$valor'";
    return insertar($cadena);
}

/*
*@acti_baja_usu
*Da de alta o baja un usuario
*Parametros: String 
*$noemp, Numero de empleado a modificar
*$pass, Contraseña del administrador
*$estado, alta o baja (1 o 0)
*Return: String 1 error, 0 todo correcto
*/
function acti_baja_usu($noemp,$pass,$estado){
    $comp = select(buscar_columna("PASSWORD","usuarios_admin","PASSWORD",$pass));
    if(mysqli_num_rows($comp) > 0){
        $cadena = "UPDATE usuarios SET ESTADO = '$estado' WHERE NO_EMPLEADO = '$noemp'";
        if(insertar($cadena)){
            return "1";
        }
        else{
            if($estado){
                return "activo";
            }
            else{
                return "baja";
            }
        }
    }
    else{
        return "1";
    }
}

/*
*@modificar_eva
*Modifica la informacion de una evaluacion
*Parametros: String 
*$nom_eva, Nombre de la evaluacion
*$cues_m, Id del cuestionario de mando
*$cues_o, Id del cuestionario operativo
*$id_eva, ID de la evaluacion a modificar
*$fecha_inicio, Fecha de inicio
*$fecha_fin, Fecha de fin
*Return: String 1 error, 0 todo correcto
*/
function modificar_eva($nom_eva,$cues_m,$cues_o,$id_eva,$fecha_inicio,$fecha_fin){
	$fe = mysqli_fetch_assoc(select(buscar_2_columnas("evaluaciones", "FECHA_INICIO", "FECHA_FIN", "ID_EVALUACION",$id_eva)));
	if($fe['FECHA_INICIO'] == $fecha_inicio and $fe['FECHA_FIN'] == $fecha_fin){
    	$cadena = "UPDATE evaluaciones SET NOMBRE_EVA = '$nom_eva', CUESTIONARIO_O = '$cues_o', CUESTIONARIO_M = '$cues_m' WHERE ID_EVALUACION = '$id_eva'";
    	return insertar($cadena);
	}
	else{
    	$detonador = true;
    	$fechas = select(fechas_evas());
    	for($contador = 1; $contador <= mysqli_num_rows($fechas); $contador++){
        	$periodo = mysqli_fetch_assoc($fechas);
        	if (!(comp_fe_m($fecha_inicio,$periodo['FECHA_FIN']) || comp_fe_m($periodo['FECHA_INICIO'],$fecha_fin))){
            	if(!($periodo['ID_EVALUACION'] == $id_eva)){
                	$detonador = false;
            	}
        	}
    	}
    	if ($detonador){
        	$cadena = "UPDATE evaluaciones SET NOMBRE_EVA = '$nom_eva' , FECHA_INICIO = '$fecha_inicio', FECHA_FIN = '$fecha_fin', CUESTIONARIO_M = '$cues_m', CUESTIONARIO_O = '$cues_o' WHERE ID_EVALUACION = '$id_eva'";
        	return insertar($cadena);
    	}
    	else{
            return "1";
    	}
	}
}

/*
*@modificar_preg
*Modifica la informacion de una pregunta
*Parametros: String 
*$pregunta, informacion de la pregunta
*$id, Id de la pregunta a modificar
*Return: String 1 error, 0 todo correcto
*/
function modificar_preg($pregunta,$id){
    $cadena = "UPDATE preguntas SET PREGUNTA_CUES = '$pregunta' WHERE ID_PREGUNTA = '$id'";
    return insertar($cadena);
}
?>
