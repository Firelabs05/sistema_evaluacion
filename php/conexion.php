<?php
/*
Finalidad: Conectar con la base de datos
Implementacion: buscar.php

Resumen: Funcion que conecta con la base de datos y devuelve la conexion.
*/
function conexion_abrir(){
    $servidor = "localhost";
    $usuario_base_datos = "root";
    $password_base_datos = "poncho";
    $base_datos = "sistema_evaluacion_3_0";
    $conexion = mysqli_connect($servidor,$usuario_base_datos,$password_base_datos);
    mysqli_select_db($conexion, $base_datos);

    if ($conexion -> connect_error){
        return "La conexion falló: " . $conexion->connect_error;
    }
    else{
        return $conexion;
    }
}
?>
