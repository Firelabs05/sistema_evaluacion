<?php
/*
Finalidad: Modulo que se encarga de modifica o elimina la informacion referente a las metas, compromisos y sus fechas
Implementacion: metas.php

Resumen: Dependiendo de la peticion del usuario es el caso que se realizara
*/
require('mostrar.php'); // Sin esto no funciona
$conexion = conexion_abrir();
switch ($_POST['acti']){ 
    case "modi_me":
        // Modifica una meta
        $meta = $_POST['meta'];
        $id = $_POST['id_meta'];
        mysqli_query($conexion, "UPDATE metas SET META = '$meta' WHERE ID_META = '$id'");
        if(mysqli_error($conexion)){
            header('location: ../html/metas.php?error=1');
        }
        header('location: ../html/metas.php');
        break;
    case "modi_comp":
        // Modifica un compromiso
        $comp = $_POST['comp'];
        $id = $_POST['id_comp'];
        mysqli_query($conexion, "UPDATE compromisos SET COMPROMISO = '$comp' WHERE ID_COMPROMISO = '$id'");
        if(mysqli_error($conexion)){
            header('location: ../html/metas.php?error=1');
        }
        header('location: ../html/metas.php');
        break;
    case "modi_fe":
        // Modifica la fecha de un compromiso
        $fe = $_POST['fe_comp'];
        $id = $_POST['id_comp'];
        mysqli_query($conexion, "UPDATE compromisos SET FECHA_COMP = '$fe' WHERE ID_COMPROMISO = '$id'");
        if(mysqli_error($conexion)){
            header('location: ../html/metas.php?error=1');
        }
        header('location: ../html/metas.php');
        break;
    case "eli_meta":
        // Elimina una meta
        $id = $_POST['id_meta'];
        mysqli_query($conexion, "DELETE FROM metas WHERE ID_META = '$id'");
        if(mysqli_error($conexion)){
            header('location: ../html/metas.php?error=1');
        }
        header('location: ../html/metas.php');
        break;
    case "eli_comp":
        // Elimina un compromiso
        $id = $_POST['id_comp'];
        mysqli_query($conexion, "DELETE FROM compromisos WHERE ID_COMPROMISO = '$id'");
        if(mysqli_error($conexion)){
            header('location: ../html/metas.php?error=1');
        }
        header('location: ../html/metas.php');
        break;
    case "agre_comp":
        // Agrega un compromiso
        $fe = $_POST['fe_comp'];
        $id = $_POST['id_meta'];
        $comp = $_POST['comp'];
        mysqli_query($conexion, "INSERT INTO compromisos (ID_META, COMPROMISO, FECHA_COMP) VALUES ('$id','$comp','$fe')");
        if(mysqli_error($conexion)){
            header('location: ../html/metas.php?error=1');
        }
        header('location: ../html/metas.php');
        break;
}
?>
