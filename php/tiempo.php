<?php
/*
Finalidad: Modulo con funciones referentes a comprobar y administrar las evaluaciones, activarlas y desactivarlas
dependiendo de la fecha

Implementacion: sesion.php

Resumen: Verifica la fecha actual y comprueba las evaluaciones y sus periodos
*/
/*
*@comprobar
*Verifica la fecha actual y comprueba las evaluaciones y sus periodos
*Parametros: Ninguno 
*Return: Ninguno
*/
function comprobar(){
    $conexion = conexion_abrir();
    $fe = select(buscar_2_columnas("evaluaciones","ID_EVALUACION","FECHA_FIN","ESTADO_EVA","1"));
    if (mysqli_num_rows($fe)>0){
        $in = mysqli_fetch_assoc($fe);
        $hoy = date('Y-m-d');
        if (comp_fe_m($hoy,$in['FECHA_FIN'])){
            $id = $in['ID_EVALUACION'];
            mysqli_query($conexion,"UPDATE evaluaciones SET ESTADO_EVA = '3' WHERE ID_EVALUACION = '$id'");
        }
    }
    else{
        $fe = select(buscar_2_columnas("evaluaciones","ID_EVALUACION","FECHA_INICIO","ESTADO_EVA","0"));
        if (mysqli_num_rows($fe)>0){   
            for($contador = 1; $contador <= mysqli_num_rows($fe); $contador++){
                $in = mysqli_fetch_assoc($fe);
                $hoy = date('Y-m-d');
                if(comp_fe($hoy,$in['FECHA_INICIO'])){
                    $id = $in['ID_EVALUACION'];
                    mysqli_query($conexion,"UPDATE evaluaciones SET ESTADO_EVA = '1' WHERE ID_EVALUACION = '$id'");
                    $c = mysqli_fetch_assoc(mysqli_query($conexion,"SELECT CUESTIONARIO_O, CUESTIONARIO_M FROM evaluaciones WHERE ID_EVALUACION = '$id'"));
                    $m = $c['CUESTIONARIO_M'];
                    $o = $c['CUESTIONARIO_O'];
                    mysqli_query($conexion,"UPDATE cuestionarios SET ESTADO_CUES = '1' WHERE ID_CUESTIONARIO = '$m' OR ID_CUESTIONARIO = '$o'") ;
                    mysqli_query($conexion,"UPDATE empleados SET ESTADO_EMP = '0'");
                }
            }
        }
    }
    mysqli_close($conexion);
}
?>