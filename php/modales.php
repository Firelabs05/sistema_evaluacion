<?php
/*
Finalidad: Modulo que contiene la funciones referentes a creacion de contenido en ventanas modales
Implementacion: ajax.php

Resumen: Libreria de funciones referente a ventanas modales
*/
//-----------------------------------------------------------------------------------------------------------
//ADMINISTRADOR.PHP
/*
*@modal_modi_eva
*Crea el contenido dinamico para modificar una evaluacion
*Parametros: String 
*$eva, array con la informacion de la evaluacion del base de datos
*$valor, ID de la evaluacion a modificar
*Return: Codigo HTML
*/
function modal_modi_eva($eva,$valor){
	echo '<input type = "hidden" id = "mId_eva" value = "'.$valor.'">
      	<label class="control-label" for="nom_eva">Nombre de la evaluacion:</label>
       	<input class="form-control" id = "mNomEva" value = "'.$eva['NOMBRE_EVA'].'" placeholder="Nombre evaluacion" type="text" required><br>
        <label for = "">Selecione el cuestionario para personal de mando</label>
        <select class="form-control" id = "mCueM" required>
		'.seleccionar_cues($eva['CUESTIONARIO_M']).'
        </select>
        <label for = "">Seleccione el cuestionario para personal operativo</label>
        <select class="form-control" id = "mCueO" required>
		'.seleccionar_cues($eva['CUESTIONARIO_O']).'
        </select><br>
        <div class="col-md-12" onmouseover ="calendario()">
        <div class="datepicker-group col-md-6">
        <label class="control-label col-md-12" for="fecha_inicio">Fecha de inicio:</label>
        <input class="sombra calendario col-md-10" id = "mFeIn" type="text" value = "'.$eva['FECHA_INICIO'].'" onchange = "val_fechas(this.value,this.id)" required onfocus="calendario()">
        <span class="glyphicon glyphicon-calendar cale" aria-hidden="true"></span>
        </div>
        <div class="datepicker-group col-md-6">
        <label class="control-label col-md-12" for="fecha_fin">Fecha de fin:</label>
        <input class="sombra calendario col-md-10" id = "mFeFin" type="text" value = "'.$eva['FECHA_FIN'].'" onchange = "val_fechas(this.value,this.id)" required onfocus="calendario()">
        <span class="glyphicon glyphicon-calendar cale" aria-hidden="true"></span>
        </div>
        </div>';
}

/*
*@modal_detalles
*Crea el contenido dinamico para mostrar la grafica y la informacion del boton detalles
*Parametros: String 
*$v, ID de la evaluacion
*$pag, solo dos valores "HOLA" o "0" decide que valor desea retornar
*Return: Codigo HTML, String
*/
function modal_detalles($v,$pag){
    // Parametros
    $detonador = false;
    $faltan = 0;
    $nin = 0;
    $primero = select(prueba());
    $todos = mysqli_num_rows($primero);
    $tabla = '<div class="tabla-grafi">';
    $segundo = select(buscar_columna("NO_EMPLEADO", "resultados_cues", "ID_EVALUACION",$v));
    // Fin parametros
    
    for($c = 1; $c <= mysqli_num_rows($primero); $c++){
        $pri = mysqli_fetch_assoc($primero);
        for ($d = 1; $d <= mysqli_num_rows($segundo);$d++){
            $seg = mysqli_fetch_assoc($segundo);
            if($pri['NO_EMPLEADO'] == $seg['NO_EMPLEADO']){
                $detonador = true;
            }
        }
    	if (!$detonador){
        	$emp = mysqli_fetch_assoc(select(prueba2($pri['NO_EMPLEADO'])));
        	if($emp['PUESTO_SUP']!= 'Ninguno'){
            	$tabla .= '<div>
                	<p>Empleado</p>
                    <div>
                	<p>Nombre:</p>
                	<p>'.nombre($emp).'</p>
                	<p>Puesto evaluador</p>
                	<p>'.$emp['PUESTO_SUP'].'</p>
                    </div>
                	</div>';

            	$faltan++;
        	}
        	else{
            	$nin++;
        	}
    	}
    	mysqli_data_seek($segundo, 0);
    	$detonador = false;
    }
    // Decide que retornar si la tabla con los empleados faltantes o la cantidad de empleados evaluados y faltantes
    switch($pag){
    	case "hola":
    		return $tabla .= '</div>';
    		break;
 		case "0":
    		$real = ($todos-$nin)-$faltan;
    		$cade = $real.",".$faltan;
    		return 	$cade;
    	break;
    }
}
//FIN ADMINISTRADOR.PHP
//----------------------------------------------------------------------------------------------------------------------------
//USUARIOS.PHP
/*
*@modal_modi_usu
*Crea el contenido dinamico para modificar la informacion de un usuario
*Parametros: String 
*$empleado, array con la informacion del empleado a modificar
*$valor, numero de empleado a modificar
*Return: Codigo HTML
*/
function modal_modi_usu($empleado,$valor){
	echo '<form name ="formu_modi_usu" onsubmit="return false" class="form-modal-mod" onmouseover="foco_btn()" onmouseout="quita_btn()">
	 	<input type = "hidden" id="valor" value ="'.$valor.'">
		<p>Datos Personales</p>
        <div>
        <div class="form-group col-md-4 col-sm-6 text-center">
        <label class="control-label" for="nombre">Nombre(s)</label>
        <input class="form-control" id="nombre" pattern="[A-Za-z ñ]*" name = "nombre" value = "'.$empleado['NOMBRE_EMP'].'"  type="text" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 text-center">
        <label class="control-label" for="apellidoP">Apellido P.</label>
        <input class="form-control" id="apellidoP" pattern="[A-Za-z ñ]*" name = "apellidoP" value = "'.$empleado['APELLIDO_P'].'" type="text" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 text-center">
        <label class="control-label" for="apellidoM">Apellido M.</label>
        <input class="form-control" id="apellidoM" name = "apellidoM" pattern="[A-Za-z ñ]*" value = "'.$empleado['APELLIDO_M'].'" type="text" required>
        </div>
        <div class="form-group col-md-6 col-sm-6 text-center">
        <label class="control-label" for="rfc">RFC</label>
        <input class="form-control" id="rfc" pattern="[A-Za-z0-9ñ]{13,13}" onblur = "valida_registro(this.id,\'m_rfc\')" onfocus="foco(this.id)" onfocusout="desenfoque(this.id)" value = "'.$empleado['RFC'].'" type="text" required>
        <i class="fa fa-1x fa-fw"></i>
        </div>
        <div class="form-group col-md-6 col-sm-6 text-center">
        <label class="control-label" for="curp">CURP</label>
        <input class="form-control" id="curp" pattern="[A-Za-z0-9ñ]{18,18}" value = "'.$empleado['CURP'].'" type="text" onfocus="foco(this.id)" onfocusout="desenfoque(this.id)"  onblur="valida_registro(this.id,\'m_curp\')" required>
        <i class="fa fa-1x fa-fw"></i>
        </div>
        <div class="form-group col-md-6 col-sm-6 text-center">
        <label class="control-label" for="noemp">No. empleado</label>
        <input class="form-control" id="noemp" pattern="[A-Za-z0-9]*" value = "'.$valor.'" type="text" onfocus="foco(this.id)" onfocusout="desenfoque(this.id)" onblur="valida_registro(this.id,\'m_noemp\')" required>
        <i class="fa fa-1x fa-fw"></i>
        </div>
        <div class="form-group col-md-6 col-sm-6 text-center">
        <label class="control-label" for="correo">Correo electrónico</label>
        <input class="form-control" id="correo" name = "correo" value = "'.$empleado['CORREO'].'" type="email" required>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <button class="btn btn-primary disabled col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2" data-dismiss="modal" type="button"  onclick = "da_pe_modi_usu()">Modificar</button>
        </div>
        </div>
        </form>';

    echo '<form name = "formu_modi_usudt" onsubmit="return false" class="form-modal-tra" onmouseover="foco_btn2()" onmouseout="quita_btn2()">
    	<p>Datos de trabajo</p>
        <div>
        <div class="col-md-6 col-sm-6 text-center">
        <label for="">Area adscrito</label>
        <select class="form-control" id = "dep" onchange="valida_registro(this.id,\'puesto\')"">
        '.seleccionar_dep($empleado['DEPARTAMENTO']).'
        </select>
        </div>
        <div class="col-md-6 col-sm-6 text-center">
        <label for="">Puesto</label>
        <select class="form-control" id = "puesto2" >
        ';
    puestos($empleado['ID_DEPARTAMENTO'],$empleado['ID_PUESTO']);
    echo '</select>
        </div>
        <div class="form-group datepicker-group col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 text-center">
        <label class="control-label col-md-12" for="fecha-ingre">Fecha de ingreso</label>
        <input class="sombra calendario col-md-12" type="text" id="fechaingre"  value = "'.$empleado['FECHA_INGRESO'].'" required onfocus="calendario()">
        <span class="glyphicon glyphicon-calendar cale-usu-1" aria-hidden="true"></span>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <button class="btn btn-primary disabled col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2" data-dismiss="modal"  type="button" onclick = "da_ta_modi_usu()" >Modificar</button>
        </div>
        </form>';
}
// FIN USUARIOS.PHP
//-----------------------------------------------------------------------------------------------------------------------
// VISUALIZAR_CUESTIONARIO.PHP
/*
*@modal_modi_oreg
*Crea el contenido dinamico para modificar una pregunta
*Parametros: String 
*$pregunta, Pregunta a modificar
*$id, id de la pregunta a modificar
*Return: Codigo HTML
*/
function modal_modi_preg($pregunta,$id){
	echo '<label>Modifique la pregunta:</label>
        <input type = "hidden" id="mIdPreg" value = "'.$id.'">
        <textarea id="mPreg"  class="form-control"  rows="5" value = "'.$pregunta.'">'.$pregunta.'</textarea><br><br>';
}
// FIN VISUALIZAR_CUESTIONARIO.PHP
//---------------------------------------------------------------------------------------------------------------------------
?>
