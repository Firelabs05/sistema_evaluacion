<?php
/*
Finalidad: Agrega la calificacion de cada meta realizado por el evaualuador
Implementacion: calificar_metas.php

Resumen: Recibe la informacion a traves del metodo POST y lo guarda en la base de datos.
*/
require ('buscar.php'); // Sin esto no funcion a
session_start();

// Parametros
$id_eva = $_SESSION['id_eva'];
$evaluador = $_SESSION['noemp'];
$noemp = $_POST['noemp'];
$fecha = getDate();
$fecha = $fecha['year']."-".$fecha['mon']."-".$fecha['mday'];
$comen = $_POST['comen'];
$id_p = $_POST['id_puesto'];
$contador = 1;
$suma = 0;
// Fin parametros

// Guarda el resultado en la base de datos
mysqli_query($conexion, "INSERT INTO resultados_metas (ID_EVALUACION,NO_EMPLEADO,ID_PUESTO,NO_EMP_EVA,COMENTARIO_META,FECHA_CALIF) VALUES ('$id_eva', '$noemp', '$id_p', '$evaluador', '$comen', '$fecha')");

if (mysqli_error($conexion)){
    echo mysqli_error($conexion);
}
else{
    $id_r = mysqli_insert_id($conexion);
    while (isset($_POST['respuesta'.$contador])){

        // Guarda las respuestas
        $calif = $_POST['respuesta'.$contador];
        $id_meta = $_POST['id_meta'.$contador];
        mysqli_query($conexion,"INSERT INTO respuestas_metas (ID_RESULTADO_META, CALIF_META, ID_META) VALUES ('$id_r','$calif','$id_meta')");
        
        if (mysqli_error($conexion)){
            echo mysqli_error($conexion);
        }

        // Pone la meta como calificada
        mysqli_query($conexion, "UPDATE metas SET ESTADO_META = '1' WHERE ID_META = '$id_meta'");
        if(mysqli_error($conexion)){
            echo mysqli_error($conexion);
        }
        $contador++;
        $suma +=$calif;
    }
    $suma = $suma/--$contador;
    $suma = round($suma,2);

    // Agrega la calificacion global de las metas
    mysqli_query($conexion, "UPDATE resultados_metas SET CALIF_META = '$suma' WHERE ID_RESULTADO_META = '$id_r'");
    if(mysqli_error($conexion)){
        echo mysqli_error($conexion);
    }
    else{
        $e = mysqli_fetch_assoc(select(buscar_columna("ESTADO_EMP","empleados","NO_EMPLEADO", $noemp)));
        if (mysqli_error($conexion)){
            echo mysqli_error($conexion);
        }
        else{
            $estado = "";
            switch ($e['ESTADO_EMP']){
                case '0':
                    $estado = '2';
                    break;
                case '3':
                    $estado = '1';
                    break;
            }
            // Modifica el estado del empleado
            mysqli_query($conexion, "UPDATE empleados SET ESTADO_EMP = '$estado' WHERE NO_EMPLEADO = '$noemp'");
            if(mysqli_error($conexion)){
                echo mysqli_error($conexion);
            }
            else{
                header('location: ../html/seleccion.php');
            }
        }
    }
}
?>