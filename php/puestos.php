<?php
/*
Finalidad: Modulo que contiene la funciones para mostrar los puestos que haya

Implementacion: ajax.php

Resumen: Libreria de funciones referente a mostrar informacion de forma dinamica, sobre los puestos de los empleados
*/

/*
*@puestos
*Muestra todos los puestos dependiendo del area adscrita
*Parametros: String 
*$id, Id del departamento
*$idP, id del puesto precargado
*Return: Codigo html
*/
function puestos($id,$idP){
	$cadena = "SELECT ID_PUESTO, PUESTO FROM puestos WHERE ID_DEPARTAMENTO = '$id'";
	$res = select($cadena);
	for($c = 1; $c <= mysqli_num_rows($res); $c++){
    	$p = mysqli_fetch_assoc($res);
			if($p['ID_PUESTO'] == $idP){
				echo "<option selected value = '".$p['ID_PUESTO']."'>" .$p['PUESTO']. "</option>";
			}
			else {
				echo "<option value = '".$p['ID_PUESTO']."'>" .$p['PUESTO']. "</option>";
			}
	}
}

/*
*@Npuestos
*Muestra todos los puestos dependiendo del area adscrita
*Parametros: String 
*$id, Id del departamento
*Return: Codigo html
*/
function Npuestos($id){
	$cadena = "SELECT PUESTO FROM puestos WHERE ID_DEPARTAMENTO = '$id'";
	$res = select($cadena);
	for($c = 1; $c <= mysqli_num_rows($res); $c++){
    	$p = mysqli_fetch_assoc($res);
    	echo "<option value = '".$p['PUESTO']."'>" .$p['PUESTO']. "</option>";
	}
}
?>
