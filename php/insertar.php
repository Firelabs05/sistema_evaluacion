<?php
/*
Finalidad: Modulo que contiene la funciones principales referentes a insertar informacion
Implementacion: ajax.php

Resumen: Libreria de funciones referente a agregar informacion.
*/
//-------------------------------------------------------------------------------------------------------
// NAVE_ADMIN.PHP
/*
*@insertar_evaluacion
*Inserta una nueva evaluacion en la base de datos
*Parametros: String 
*$nom_eva, Nombre de la evaluacion
*$cuestionario_m, Cuestionario de mando
*$cuestionario_o, Cuestionario de operativo
*$fecha_inicio, Fecha de inicio de la evaluacion
*$fecha_fin, Fecha fin de la evaluacion
*Return: String 1 error, 0 todo correcto
*/
function insertar_evaluacion($nom_eva,$cuestionario_m,$cuestionario_o,$fecha_inicio,$fecha_fin){
	// AGREGA UNA NUEVA EVALUACION
	$fechas = select(seleccionar_2_columnas("evaluaciones","FECHA_INICIO","FECHA_FIN"));
	$detonador = true;

	for($contador = 1; $contador <= mysqli_num_rows($fechas); $contador++){
    	$periodo = mysqli_fetch_assoc($fechas);

		// Corrobora que no haya una evaluacion con el mismo periodo
    	if (!(comp_fe_m($fecha_inicio,$periodo['FECHA_FIN']) || comp_fe_m($periodo['FECHA_INICIO'],$fecha_fin))){
        	$detonador = false;
    	}
	}
	if ($detonador){
    	$cadena = "INSERT INTO evaluaciones (NOMBRE_EVA, FECHA_INICIO,FECHA_FIN,CUESTIONARIO_M,CUESTIONARIO_O, ESTADO_EVA) VALUES ('$nom_eva' ,'$fecha_inicio' ,'$fecha_fin' ,'$cuestionario_m' ,'$cuestionario_o' ,'0')";
    	return insertar($cadena); // Agega la evaluacion
	}
	else{
    	return "1";
	}
}
// FIN NAVE_ADMIN.PHP
//----------------------------------------------------------------------------
// REGISTRO.PHP
/*
*@insertar_usuario
*Inserta una nueva evaluacion en la base de datos
*Parametros: String 
*$hash, Contraseña
*$usuario, Nombre de usuario
*$nombre, Nombre de empleado
*$apellidoP, Apellido paterno
*$apellidoM, Apellido materno
*$rfc, RFC del usuario
*$curp, CURP del usuario
*$noemp, Numero de empleado
*$correo, Correo electronico
*$fechaingre, Fecha de ingreso
*$puesto, Puesto del empleado
*Return: String 1 error, 0 todo correcto
*/
function insertar_usuario($hash,$usuario,$nombre,$apellidoP,$apellidoM,$rfc,$curp,$noemp,$correo,$fechaingre,$puesto){
	// AGREGA UN NUEVO USUARIO
	// Parametros
	$hash = password_hash($hash, PASSWORD_BCRYPT);
	$rfc = strtoupper($rfc);
	$curp = strtoupper($curp);
	// Fin parametros

	$cadena = "SELECT NIVEL_MANDO FROM puestos WHERE ID_PUESTO = '$puesto'";
	$tipo = mysqli_fetch_assoc(select($cadena));

	// Asigna que tipo de empleado sera dependiendo del puesto mandando
	if ($tipo['NIVEL_MANDO'] == '0'){
    	$tipo_emp = 0;
	}
	else{
    	$tipo_emp = 1;
	}

	$cadena = "INSERT INTO empleados (NO_EMPLEADO, NOMBRE_EMP, APELLIDO_P, APELLIDO_M, RFC, CURP, FECHA_INGRESO,CORREO, TIPO_EMP,ID_PUESTO,ESTADO_EMP) VALUES ('$noemp', '$nombre', '$apellidoP', '$apellidoM', '$rfc', '$curp', '$fechaingre', '$correo', '$tipo_emp', '$puesto','0')";
	if(insertar($cadena)){ // Agrega el empleado
		return "1";
	}
	else{
		$cadena = "INSERT INTO usuarios (NO_EMPLEADO, USUARIO, PASSWORD, ESTADO) VALUES ('$noemp', '$usuario', '$hash', '1')";
		if(insertar($cadena)){ // Agrega el usuario
			return "1";
		}
		else{
			return "0";
		}
	}
}
// FIN REGISTRO.PHP
//---------------------------------------------------------------------------------
// MODIFCAR_CUESTIONARIO.PHP
/*
*@insertar_preg
*Inserta una nueva pregunta a un cuestionario
*Parametros: String 
*$pre, Pregunta a introducir
*$id, ID del cuestionario al que pertecera
*Return: String 1 error, 0 todo correcto
*/
function insertar_preg($pre,$id){
	// AGREGA LA NUEVA PREGUNTA
    $cadena = "INSERT INTO preguntas (ID_CUESTIONARIO, PREGUNTA_CUES) VALUES ('$id','$pre')";
    return insertar($cadena);
}
// FIN MODIFICAR_CUESTIONARIO.PHP
//------------------------------------------------------------------
// DEPARTAMENTO.PHP
/*
*@n_depa
*Inserta una nueva area adscrita
*Parametros: String 
*$depa, Nueva area adscrita
*$pass, Contraseña del administrador
*Return: String 1 error, 0 todo correcto
*/
function n_depa($depa,$pass){
	$cad = "SELECT PASSWORD FROM usuarios_admin WHERE PASSWORD = '$pass'";
	if(mysqli_num_rows(select($cad)) > 0){
		$cadena = "INSERT INTO departamentos (DEPARTAMENTO) VALUES ('$departamento')";
		echo insertar($cadena);
	}
	else {
		echo "1";
	}
}

/*
*@n_puesto
*Inserta un nuevo puesto de empleado
*Parametros: String 
*$puesto, Nuevo puesto de empleado
*$nivel, Nivel del nuevo puesto
*$departamento, ID del departamento al que pertenecera
*$mando, Puesto del jefe inmediato
*$pass, Contraseña del administrador
*Return: String 1 error, 0 todo correcto
*/
function n_puesto($puesto,$nivel,$departamento,$mando,$pass){
	$cadena = "SELECT PASSWORD FROM usuarios_admin WHERE PASSWORD = '$pass'";
	if(mysqli_num_rows(select($cadena)) > 0){
			$cadena = "INSERT INTO puestos (ID_DEPARTAMENTO, PUESTO, NIVEL_MANDO,PUESTO_SUP) VALUE ('$departamento','$puesto','$nivel','$mando')";
			echo insertar($cadena);
	}
	else{
		echo "1";
	}
}
// FIN DEPARTAMENTO.PHP
?>
