<?php
/*
Finalidad: Agregar los cuestionarios en la base de datos
Implementacion: agregar_cuestionario.php

Resumen: Recibe la informacion del cuestionario del usuario, y lo guarda junto con sus preguntas.
*/
require ('buscar.php'); // Sin esto no funciona

if (isset ($_POST['nom_cues']) and $_POST['final']!= ""){
    // Parametros
    $nombre_cuestionario = $_POST['nom_cues'];
    $fecha = getDate();
    $fecha = $fecha['year']."-".$fecha['mon']."-".$fecha['mday'];
    // Fin parametros

    // Codigo SQL
    $cadena = "INSERT INTO cuestionarios (NOMBRE_CUES, ESTADO_CUES, FECHA_ING) VALUES ('$nombre_cuestionario', '0', '$fecha')";

    // Se inserta en la base de datos
    $conexion = insertar_con($cadena);
    if( $conexion == false){
        header("location: ../html/cuestionario.php?error=1");
    }
    else{
        $id_cuest = mysqli_insert_id($conexion);
        for ($contador = 1; $contador<=$_POST['final'];$contador++){
            if (isset($_POST[$contador])){
                $pregunta = $_POST[$contador];
                $cadena = "INSERT INTO preguntas (ID_CUESTIONARIO, PREGUNTA_CUES) VALUES ('$id_cuest','$pregunta')";
                insertar($cadena); // Se insertan las preguntas
            }
        }
        header("location: ../html/cuestionario.php?error=0");
    }
}
else{
    header("location: ../html/cuestionario.php?error=1");
}
?>
