<?php
/*
Finalidad: recibir la peticion ajax. realizar la accion solicitada.
Implementacion: ajax.js

Resumen: Recibe la informacion a traves del metodo POST, la variable ['acti'], siempre sera el identificador.
Dependiendo de la actividad se agrega su caso, y sus acciones a realizar.
*/
require('mostrar.php'); // Sin esto no funciona
if(isset($_POST['valor']))
{
	$id = $_POST['valor'];
}
switch($_POST['acti'])
{
	// Peticiones de index.php
    case "sesion":
   		// Se encarga de validar el inicio de sesion
       	include('sesion.php');
        break;
    // Fin index.php
    //--------------------------------------------------------------------------------------
	// Peticiones de administrador.php
	case "modi_eva":
		// Crea la ventana para modificar evaluacion
		include('modales.php');
		$eva = mysqli_fetch_assoc(select(buscar_eva($_POST['valor'])));
		modal_modi_eva($eva,$_POST['valor']);
        break;
    case "m_eva":
    	// Modifica la evaluacion
    	include('modificar.php');
    	echo modificar_eva($_POST['nom_eva'],$_POST['cues_m'],$_POST['cues_o'],$_POST['id_eva'],$_POST['fecha_inicio'],$_POST['fecha_fin']);
    	break;
    case "nu_eva":
    	// Agrega nuevas evaluaciones
        include('insertar.php');
        echo insertar_evaluacion($_POST['nom_eva'],$_POST['cues_m'],$_POST['cues_o'],$_POST['fecha_inicio'],$_POST['fecha_fin']);
       	break;
    case "mos_evas":
    	// Muestra todas las evaluaciones
       	mostrar_evaluaciones();
       	break;
    case "eli_eva":
    	// Elimina una evaluacion
       	$cadena = "DELETE FROM evaluaciones WHERE ID_EVALUACION = '$id'";
		echo insertar($cadena);
       	break;
    case "moda_deta":
		// Crea la ventana modal de detalles
    	include('modales.php');
    	echo modal_detalles($_POST['valor'],$_POST['pag']);
    	break;
    // Fin de administrador.php
    //--------------------------------------------------------------------------------------------
	// Peticiones de usuarios.php
	case "mos_usu":
    	// Muestra todos los usuarios
    	mostrar_usuarios($_POST['valor']);
    	break;
    case "baja_usu":
    	// Da de baja a un usuario
    	include('modificar.php');
    	echo acti_baja_usu($_POST['noemp'],$_POST['pass'],0);
    	break;
    case "acti_usu":
    	// Activa o da de baja un usuario
    	include('modificar.php');
		echo acti_baja_usu($_POST['noemp'],$_POST['pass'],1);
    	break;
    case "bus_usu":
    	// Busca a los usuarios
    	busqueda_usu($_POST['valor'],$_POST['estado']);
    	break;
    case "modi_usu":
    	// Crea la ventana modal para modificar un usuario
    	include('modales.php');
    	include('puestos.php');
    	$empleado = mysqli_fetch_assoc(select(tabla_modificar($_POST['valor'])));
    	modal_modi_usu($empleado,$_POST['valor']);
    	break;
	case "m_curp":
		// Comprueba el curp
		$re = select(buscar_2_columnas("empleados","CURP", "NO_EMPLEADO", "CURP", $_POST['valor']));
		if(ma_cero(mysqli_num_rows($re)))
		{
			$re = mysqli_fetch_assoc($re);
			if($_POST['noemp'] == $re['NO_EMPLEADO'])
			{
				echo "curp0";
			}
			else
			{
				echo "curp1";
			}
		}
		else
		{
			echo "curp0";
		}
		break;
	case "m_rfc":
		// Comprueba el rfc
		$re = select(buscar_2_columnas("empleados","RFC", "NO_EMPLEADO", "RFC", $_POST['valor']));
		if(ma_cero(mysqli_num_rows($re)))
		{
			$re = mysqli_fetch_assoc($re);
			if($_POST['noemp'] == $re['NO_EMPLEADO'])
			{
				echo "rfc0";
			}
			else
			{
				echo "rfc1";
			}
		}
		else
		{
			echo "rfc0";
		}
		break;
	case "m_noemp":
		// Comprueba el numero de empleado
		$re = select(buscar_columnas("NO_EMPLEADO","empleados","NO_EMPLEADO",$_POST['valor']));

		if(ma_cero(mysqli_num_rows($re)))
		{
			$re = mysqli_fetch_assoc($re);
			if($_POST['noemp'] == $re['NO_EMPLEADO'])
			{
				echo "noemp0";
			}
			else
			{
				echo "noemp1";
			}
		}
		else
		{
			echo "noemp0";
		}
		break;
	case "modi_dp":
		// Modifica la informacion personal de un usuario
		include('modificar.php');
		echo modi_usu_da_pe($_POST['nombre'],$_POST['apellidoP'],$_POST['apellidoM'],$_POST['rfc'],$_POST['curp'],$_POST['noemp'],$_POST['correo'],$_POST['valor']);
		break;
	case "modi_dt":
		// Modifica la informacion de trabajo de un usuario
		include('modificar.php');
		echo modi_usu_da_ta($_POST['fechaingre'],$_POST['puesto'],$_POST['valor']);
		break;
   	// Fin de usuarios.php
   	//----------------------------------------------------------------------
	// Peticiones de registrar.php
	case "r_usuario":
		// Comprueba el usuario
		$re = select(buscar_columna("USUARIO", "usuarios", "USUARIO", $_POST['valor']));
		echo "usua".ma_cero(mysqli_num_rows($re));
		break;
	case "r_curp":
		// Comprueba el curp
		$re = select(buscar_columna("CURP","empleados","CURP",$_POST['valor']));
		echo "curp".ma_cero(mysqli_num_rows($re));
		break;
	case "r_rfc":
		// Comprueba el rfc
		$re = select(buscar_columna("RFC","empleados","RFC",$_POST['valor']));
		echo "rfc".ma_cero(mysqli_num_rows($re));
		break;
	case "r_noemp":
		// Comprueba el numero de empleado
		$re = select(buscar_columna("NO_EMPLEADO","empleados","NO_EMPLEADO",$_POST['valor']));
		echo "emp".ma_cero(mysqli_num_rows($re));
		break;
	case "puesto":
		// Manda los puestos dependiendo de area adscrita
		include('puestos.php');
		puestos($id);
		break;
	case "reg_usu":
		// Registra al usuario
		include('insertar.php');
		echo insertar_usuario($_POST['password'],$_POST['usuario'],$_POST['nombre'],$_POST['apellidoP'],$_POST['apellidoM'],$_POST['rfc'],$_POST['curp'],$_POST['noemp'],$_POST['correo'],$_POST['fechaingre'],$_POST['puesto']);
		break;
	// Fin de registrar.php
	//-----------------------------------------------------------------------------
	// Peticiones de cuestionarios.php
    case "mos_cues":
    	// Muestra todos los cuestionarios
    	mostrar_cuestionarios();
    	break;
    case "eli_cues":
    	// Elimina un cuestionario
    	$cadena = "DELETE FROM cuestionarios WHERE ID_CUESTIONARIO = '$id'";
		echo insertar($cadena);
		break;
    // Fin de cuestionarios.php
    //-----------------------------------------------------------------------------------------
    // Peticiones de visualizar_cuestionario.php
	case "0":
		// Muestra el cuestionario con los botones para iteractuar
		mostrar_pregunta($_POST['valor'],"0");
		break;
	case "1":
		// Muestra el cuestionario como el evaluador lo veria
		mostrar_pregunta($_POST['valor'],"1");
		break;
	case "modal_preg":
		// Rellena la ventana modal de modificar pregunta
		include('modales.php');
		$pregun = mysqli_fetch_assoc(select(buscar_columna("PREGUNTA_CUES","preguntas","ID_PREGUNTA",$_POST['valor'])));
		modal_modi_preg($pregun['PREGUNTA_CUES'],$_POST['valor']);
		break;
	case "modi_preg":
		// Modifica la pregunta
		include('modificar.php');
		echo modificar_preg($_POST['pregunta'],$_POST['id']);
		break;
	case "eli_preg":
		// Elimina la pregunta
		$cadena = "DELETE FROM preguntas WHERE ID_PREGUNTA = '".$_POST['id']."'";
		echo insertar($cadena);
		break;
	case "nuev_preg":
		// Agrega una nueva pregunta
		include('insertar.php');
		echo insertar_preg($_POST['preg'],$_POST['id']);
		break;
	// Fin de visualizar_cuestionario.php
	//----------------------------------------------------
	// Peticiones de evalaucion.php 
	case "mos_evados":
		// Muestra todos los evaluados
		mostrar_todos_evaluados($_POST['valor']);
		break;
	case "bus_evados";
		// Busca entre los evaluados
		mostrar_eva_busqueda($_POST['id'],$_POST['valor']);
		break;
	case "Npuesto":
		// Manda los puestos dependiendo del departamento
		include('puestos.php');
		Npuestos($id);
		break;
	case "mos_depas":
		// Manda todos los departamentos y sus puestos
		mostrar_departamentos();
		break;
	case "nuev_depa":
		// Crea un nuevo departamento
		include('insertar.php');
		n_depa($id,$_POST['pass']);
		break;
	case "nuev_puest":
		// Agrega un nuevo puesto
		include('insertar.php');
		n_puesto($_POST['puesto'],$_POST['nivel'],$_POST['depa'],$_POST['mando'],$_POST['pass']);
		break;
	// Fin evaluaciones.php
	//--------------------------------------------------------------------------------------------------
    default:
    	echo $_POST['acti'];
    	break;
}
?>
