<?php
/*
Finalidad: Agrega la calificacion de cada pregunta y del cuestionario realizado por el evaualuador
Implementacion: calificar_cues.php

Resumen: Recibe la informacion a traves del metodo POST y lo guarda en la base de datos.
*/
require ('buscar.php');
$conexion = conexion_abrir(); // Conecta con la base de datos

// Parametros resibidos
$no_emp = $_POST['subor'];
$no_emp_eva = $_POST['evaluador'];
$id_puesto = $_POST['id_pue'];
$id_eva = $_POST['id_eva'];
$fecha = getDate();
$fecha = $fecha['year']."-".$fecha['mon']."-".$fecha['mday'];
$id_cues = $_POST['id_cues'];
$tipo = $_POST['tipo'];
// Final de parametros

// Guarda la informacion referente a la evaluacion
mysqli_query($conexion, "INSERT INTO resultados_cues (NO_EMPLEADO, ID_EVALUACION, ID_CUESTIONARIO, ID_PUESTO, FECHA_REA_CUES, NO_EMP_EVA) VALUES ('$no_emp', '$id_eva', '$id_cues', '$id_puesto', '$fecha', '$no_emp_eva')");

if(mysqli_error($conexion)){
    echo mysqli_error($conexion);
}
else{
    $id_resultado = mysqli_insert_id($conexion);
    $contador = 1;
    $suma = 0;
    while (isset($_POST['respuesta'.$contador])){
        // Parametros de las preguntas-respuestas
        $respuesta = $_POST['respuesta'.$contador];
        $pregunta = $_POST['pregunta'.$contador];
        $suma += $respuesta;
        // Fin parametros

        // Guarda los resultados
        mysqli_query($conexion, "INSERT INTO resultados_preguntas (ID_RESULTADO_CUES,ID_PREGUNTA,RESULTADO) VALUES ('$id_resultado','$pregunta','$respuesta')");
        if (mysqli_error($conexion)){
            echo mysqli_error($conexion);
        }
        else{
            $contador++;
        }
    }
    $suma = $suma/--$contador;
    $suma = round($suma, 2);

    // Guarda la calificacion general del cuestinario
    mysqli_query($conexion, "UPDATE resultados_cues SET CALIF_CUES = '$suma' WHERE ID_RESULTADO_CUES = '$id_resultado'");
    
    if(mysqli_error($conexion)){
        echo mysqli_error($conexion);
    }
    else{
        // Verifica el estado del empleado
        $e = mysqli_fetch_assoc(select(buscar_columna("ESTADO_EMP","empleados","NO_EMPLEADO", $no_emp)));
        if (mysqli_error($conexion)){
            echo mysqli_error($conexion);
        }
        else{
            $estado = "";
            switch ($e['ESTADO_EMP']){
                case '0':
                    if($tipo == '1'){
                        $estado = '3';
                    }
                    else{
                        $estado = '1';
                    }
                    break;
                case '2':
                    $estado = '1';
                    break;
            }
            // Cambia su estado
            mysqli_query($conexion, "UPDATE empleados SET ESTADO_EMP = '$estado' WHERE NO_EMPLEADO = '$no_emp'");
            if(mysqli_error($conexion)){
                echo mysqli_error($conexion);
            }
            else{
                header('location: ../html/seleccion.php'); // Regresa
            }
        }
    }
}
?>