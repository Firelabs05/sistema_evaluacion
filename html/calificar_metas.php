<?php
require ('../php/mostrar.php');
$subor = mysqli_fetch_assoc(select(subordinado($_GET['no_emp'])));
if ($subor['TIPO_EMP'] == '1')
{
    $tipo = "Mando";
}
else
{
    $tipo = "Operativo";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Calificar metas</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="../js/jquery-3.1.1.js"></script>
</head>
<body>
    <?php
    evaluador();
    ?>
    <header class="col-md-12">
        <div class="col-md-3 col-sm-12">
            <ol class="breadcrumb">
              <li><a href="seleccion.php"><i class="icon icon-home"></i></a></li>
              <li class="active">Calificar metas</li>
            </ol>
        </div>
        <div class="col-md-6 col-sm-12">
            <h2 class="text-center titu-cali-cues">Calificar metas</h2>
        </div>
    </header>
    <div class="datos-evaluado col-md-3 col-sm-12">
        <h4 class="col-md-12 text-center">Datos del evaluado</h4>
        <div>
            <p class="col-md-12">Nombre:</p>
            <p class="col-md-12"><?php echo nombre($subor);?></p>
        </div>
        <div>
            <p class="col-md-12">Adscrito a:</p>
            <p class="col-md-12"><?php echo $subor['DEPARTAMENTO'];?></p>
        </div>
        <div>
            <p class="col-md-12">Puesto:</p>
            <p class="col-md-12"><?php echo $subor['PUESTO'];?></p>
        </div>
        <div>
            <p class="col-md-12">No. empleado:</p>
            <p class="col-md-12"><?php echo $_GET['no_emp'];?></p>
        </div>
        <div>
            <p class="col-md-12">Tipo de empleado:</p>
            <p class="col-md-12">
                <?php echo $tipo; ?>
            </p>
        </div>
    </div>
    <form action="../php/calif_meta.php" method = "POST" id="califi-metas" name = "formu" class="col-md-9 col-md-offset-3">
        <input type = "hidden" value = "<?php echo $_GET['no_emp']; ?>" name = "noemp">
        <input type = "hidden" name = "id_puesto" value = "<?php echo $subor['ID_PUESTO']; ?>">
        <?php
        mostrar_metas($_GET['no_emp'],1);
        ?>
        <div class="col-md-4">
            <label class="control-label" for="comen">Comentarios</label>
            <textarea name="comen" class="form-control" id="comen" placeholder="Inserte sus comentarios" rows="5" required></textarea>
        </div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmar">Finalizar</button>
    </form>
    <div class="modal fade" id = "confirmar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Finalizar</h4>
                </div>
                <div class="modal-body">
                    <p>Si continua ya no podra cambiar las respuestas</p>
                    <p>¿Esta seguro que desea finalizar?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" onclick = "formu.submit()" class = "btn btn-primary">Finalizar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="logo">
        <img src="../img/lo.png" alt="">
    </div>
    <div class="clearfix"></div>
    <script src="../js/main-query.js"></script>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
</body>
</html>
