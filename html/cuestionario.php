<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="../js/jquery-3.1.1.js"></script>
    <script src="../js/main-query.js"></script>
</head>
<body>
    <?php
    require ('nave_admin.php');
    ?>
    <h3>Cuestionarios</h3>
    <section class="buscar-usu col-lg-8 col-lg-offset-2 col-md-10 col-sm-10 col-xs-12">
            <form class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-2 col-xs-12" action="visualizar_cuestionario.php" role="form" method="POST"
            id = "form_cues">
            </form>
    </section>
    <form onsubmit="return false">
        <div class="modal fade" id = "modal_eli_cues">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Confirmación</h4>
                    </div>
                    <div class="modal-body">
                        <label>Al eliminar el cuestionario, tambien se eliminaran las evaluaciones relacionadas con este cuestionario.<br>¿Seguro desea continuar?
                        </label>
                        <br><br><br>
                        <input type = "hidden" id= "idCues">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" data-dismiss="modal" onclick = "eli_cues()" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
    <script type="text/javascript">
        window.onload = cargar_cues();
    </script>
</body>
</html>
