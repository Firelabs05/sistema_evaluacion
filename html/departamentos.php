<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Departamentos</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="../js/jquery-3.1.1.js"></script>
    <script src="../js/main-query.js"></script>
</head>
<body>
    <?php
    require ('nave_admin.php');
    ?>
    <h3>Areas adscritas</h3>
    <section class="buscar-usu col-lg-8 col-lg-offset-2 col-md-10 col-sm-10 col-xs-12">
       <form class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-2 col-xs-12" id = "papa_depas">
       </form>
    </section>
    <div class="clearfix"></div>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
    <script>
    window.onload = cargar_depa();
    </script>
</body>
</html>
