<?php
require('../php/mostrar.php');
seguridad_admin();
?>
<script src="https://framework-gb.cdn.gob.mx/assets/scripts/jquery-ui-datepicker.js"></script>
<script src="../js/main.js"></script>
<script src = "../js/ajax.js"></script>
<div class="hea-adm col-md-12 col-xs-12 col-sm-12 col-lg-12">
        <div class="col-md-1 col-sm-1 col-xs-1">
            <span id="menu-general" class="glyphicon glyphicon-menu-hamburger"></span>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <div></div>
        </div>
        <div class="col-md-1 col-sm-1 col-xs-1">
            <div></div>
        </div>
        <div class="cerr">
            <span class="glyphicon glyphicon-user"></span>
            <a href="../php/cerrar.php"><span class="glyphicon glyphicon-off"></span></a>
        </div>
        <div class="logo">
            <img src="../img/bar.png" alt="">
        </div>
</div>
<script>
    var btn = 0;
    window.addEventListener("load",calendario,false);
    function calendario(){
        $('.calendario').datepicker({changeYear: true,dateFormat: "yy-mm-dd"});
    }
    $('.cerr').mouseover(function(){
        $('.cerr > span').css({'transform':'scale(.3)','z-index':'-1'});
        $('.cerr a').css({'display':'block','opacity':'1','left':'-15px','transform':'scale(3)'});
        $('.cerr').mouseleave(function(){
            $('.cerr > span').removeAttr('style');
            $('.cerr a').removeAttr('style');
        })
    })
</script>
<aside class="menu-desplazado">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <span class="glyphicon glyphicon-user col-md-2 col-sm-2"></span>
        <p class="col-md-10 col-sm-10"><a onclick = "cargar_usu('todos')">Usuarios</a></p>
        <div class="col-md-12 col-sm-12">
            <span class="glyphicon glyphicon-plus col-md-2 col-sm-2"></span>
            <p class="col-md-10 col-sm-10"><a href="../html/registro.php">Nuevo usuario</a></p>
        </div>
        <div class="col-md-12 col-sm-12">
            <span class="glyphicon glyphicon-book col-md-2 col-sm-2"></span>
            <p class="col-md-10 col-sm-10"><a onclick = "cargar_usu('activo')">Usuarios activos</a></p>
        </div>
        <div class="col-md-12 col-sm-12">
            <span class="glyphicon glyphicon-book col-md-2 col-sm-2"></span>
            <p class="col-md-10 col-sm-10"><a onclick = "cargar_usu('baja')">Usuarios dados de baja</a></p>
        </div>
        <div class="col-md-12 col-sm-12">
            <span class="glyphicon glyphicon-book col-md-2 col-sm-2"></span>
            <p class="col-md-10 col-sm-10"><a onclick = "cargar_usu('todos')">Todos los usuarios</a></p>
        </div>
    </div>
    <form action = "usuarios.php" method = "POST" name = "form_nav_usu">
        <input type = "hidden" id = "naEstado" name = "estado">
    </form>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <span class="glyphicon glyphicon-folder-close col-md-2 col-sm-2"></span>
        <p class="col-md-10 col-sm-10"><a href="../html/administrador.php">Evaluaciones</a></p>
        <div class="col-md-12 col-sm-12">
            <span class="glyphicon glyphicon-file col-md-2 col-sm-2"></span>
            <p class="col-md-10 col-sm-10"><a href="" data-toggle="modal" data-target="#nueva_evaluacion">Nueva evaluación</a></p>
        </div>
        <div class="col-md-12 col-sm-12">
            <span class="glyphicon glyphicon-folder-open col-md-2 col-sm-2"></span>
            <p class="col-md-10 col-sm-10"><a href="../html/administrador.php">Todas las evaluaciones</a></p>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <span class="glyphicon glyphicon-list-alt col-md-2 col-sm-2"></span>
        <p class="col-md-10 col-sm-10"><a href="../html/cuestionario.php">Cuestionarios</a></p>
        <div class="col-md-12 col-sm-12">
            <span class="glyphicon glyphicon-file col-md-2 col-sm-2"></span>
            <p class="col-md-10 col-sm-10"><a href="" data-toggle="modal" data-target="#nuevo_cuestionario">Nuevo cuestionario</a></p>
        </div>
        <div class="col-md-12 col-sm-12">
            <span class="glyphicon glyphicon-book col-md-2 col-sm-2"></span>
            <p class="col-md-10 col-sm-10"><a href="../html/cuestionario.php">Todos los cuestionarios</a></p>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <span class="glyphicon glyphicon-briefcase col-md-2 col-sm-2"></span>
        <p class="col-md-10 col-sm-10"><a href = "../html/departamentos.php">Areas adscritas</a></p>
        <div class="col-md-12 col-sm-12">
            <span class="glyphicon glyphicon-lock col-md-2 col-sm-2"></span>
            <p class="col-md-10 col-sm-10"><a href="" data-toggle="modal" data-target="#nuevo_departamento">Nueva area adscrita</a></p>
        </div>
        <div class="col-md-12 col-sm-12">
            <span class="glyphicon glyphicon-wrench col-md-2 col-sm-2"></span>
            <p class="col-md-10 col-sm-10"><a href="" onclick = "npdep()" data-toggle="modal" data-target="#nuevo_puesto">Nuevo puesto</a></p>
        </div>
    </div>
</aside>
<div class="adm-ayuda col-md-12 col-sm-12">
    <span class="icon-infocircle col-md-2 col-sm-2"></span>
    <p class="col-md-10 col-sm-10">Ayuda</p>
</div>
<p class="flecha glyphicon glyphicon-arrow-left"></p>
<div class="bueno">
       <p>Todo salio bien</p>
       <i class="glyphicon glyphicon-ok"></i>
</div>
<div class="malo">
       <p>Ocurrio un error</p>
       <i class="glyphicon glyphicon-remove"></i>
</div>
<div class="ayuda-adm">
    <p></p>
    <p class="glyphicon glyphicon-chevron-right"></p>
</div>

<script>
    $('#menu-general').click(function(){
        var menu = ($('.menu-desplazado').css('left'));
        var compara = "-1000px";
        console.log(menu);
        if(menu == compara ){
            $('.menu-desplazado').css({'left':'0px'});
        } else if(menu == "0px" || menu == "-15px"){
            $('.menu-desplazado').css({'left':'-1000px'});
        }
    })
    //area de ayuda administrador
    $('.adm-ayuda').click(function(){
        $('.ayuda-adm > p:nth-child(1)').text('Bienvenido al Sistema de Evaluación, esta parte corresponde al área del administrador, este será un breve tutorial de cómo funciona el sistema');
        var elementos_blur = ['header','form','.hea-adm','.cerr','.logo','h3','h1','.dato-eva','section','.menu-desplazado','footer','.adm-ayuda'];
        var saludo = "";
        for(var ele = 0;ele < elementos_blur.length;ele++){
            $(elementos_blur[ele]).css({'filter':'blur(4px)'});
        }
        $('.ayuda-adm').css({'display':'block'});
        var nivel = 0;
        $('.ayuda-adm > p:nth-child(2)').click(function(){
            $('.ayuda-adm').addClass('efecto-ayuda');
            if(nivel == 0){
                $('.menu-desplazado').removeAttr('style');
                $('.menu-desplazado > div:nth-child(1)').css({'box-shadow':'0px 0px 20px black','transform':'scale(1.2)','z-index':'99','left':'20px'});
                $('.flecha').css({'opacity':'1'});
                $('.menu-desplazado > div:nth-child(3)').css({'filter':'blur(4px)'});
                $('.menu-desplazado > div:nth-child(4)').css({'filter':'blur(4px)'});
                $('.menu-desplazado > div:nth-child(5)').css({'filter':'blur(4px)'});
                $('.ayuda-adm > p:nth-child(1)').text('Esta es la sección de usuarios, aquí podrás crear usuarios, eliminarlos y modificarlos. También podrás dar de baja a los usuarios y volver activarlos en cualquier momento.');
                nivel = 1;
            }else if(nivel == 1){
                $('.menu-desplazado > div:nth-child(1)').removeAttr('style');
                $('.menu-desplazado > div:nth-child(3)').removeAttr('style');
                $('.menu-desplazado').removeAttr('style');
                $('.menu-desplazado > div:nth-child(3)').css({'box-shadow':'0px 0px 20px black','transform':'scale(1.2)','z-index':'99','left':'20px'});
                $('.flecha').css({'top':'360px'});
                $('.menu-desplazado > div:nth-child(1)').css({'filter':'blur(4px)'});
                $('.menu-desplazado > div:nth-child(4)').css({'filter':'blur(4px)'});
                $('.menu-desplazado > div:nth-child(5)').css({'filter':'blur(4px)'});
                $('.ayuda-adm > p:nth-child(1)').text('Esta es la sección de evaluaciones, aquí podrá crear evaluaciones, ver reportes de evaluaciones ya realizadas, el progreso de evaluaciones activas y modificar evaluaciones aun no activas.');
                nivel = 2;
            }else if(nivel == 2){
                $('.menu-desplazado > div:nth-child(3)').removeAttr('style');
                $('.menu-desplazado > div:nth-child(4)').removeAttr('style');
                $('.menu-desplazado').removeAttr('style');
                $('.menu-desplazado > div:nth-child(4)').css({'box-shadow':'0px 0px 20px black','transform':'scale(1.2)','z-index':'99','left':'20px'});
                $('.flecha').css({'top':'510px'});
                $('.menu-desplazado > div:nth-child(1)').css({'filter':'blur(4px)'});
                $('.menu-desplazado > div:nth-child(3)').css({'filter':'blur(4px)'});
                $('.menu-desplazado > div:nth-child(5)').css({'filter':'blur(4px)'});
                $('.ayuda-adm > p:nth-child(1)').text('Esta es la sección de cuestionarios, aquí podrás crearlos, modificarlos y eliminarlos. Los cuestionarios se agregan en cada evaluación que realices.');
                nivel = 3;
            }else if(nivel == 3){
                $('.menu-desplazado > div:nth-child(4)').removeAttr('style');
                $('.menu-desplazado > div:nth-child(5)').removeAttr('style');
                $('.menu-desplazado').removeAttr('style');
                $('.menu-desplazado > div:nth-child(5)').css({'box-shadow':'0px 0px 20px black','transform':'scale(1.2)','left':'20px'});
                $('.flecha').css({'top':'650px'});
                $('.adm-ayuda').css({'z-index':'-1'});
                $('.menu-desplazado > div:nth-child(1)').css({'filter':'blur(4px)'});
                $('.menu-desplazado > div:nth-child(3)').css({'filter':'blur(4px)'});
                $('.menu-desplazado > div:nth-child(4)').css({'filter':'blur(4px)'});
                $('.ayuda-adm > p:nth-child(1)').text('Esta es la sección de departamentos, aquí puedes crear departamentos, también puedes crear puestos para cada departamento, una vez creados no podrán ser eliminados.');
                nivel =4;
            }else if(nivel == 4){
                $('.menu-desplazado > div:nth-child(5)').removeAttr('style');
                $('.menu-desplazado > div:nth-child(5)').css({'filter':'blur(4px)'})
                $('.flecha').removeAttr('style');
                $('.adm-ayuda').removeAttr('style');
                $('.ayuda-adm > p:nth-child(1)').text('Has terminado el tutorial. Bienvenido al sistema.');
                nivel = 5;
            }else if(nivel == 5){
                $('.ayuda-adm').removeAttr('style');
                for(var ele = 0;ele < elementos_blur.length;ele++){
                    $(elementos_blur[ele]).removeAttr('style');
                    $('.menu-desplazado > div:nth-child(5)').removeAttr('style');
                    $('.menu-desplazado > div:nth-child(1)').removeAttr('style');
                    $('.menu-desplazado > div:nth-child(3)').removeAttr('style');
                    $('.menu-desplazado > div:nth-child(4)').removeAttr('style');
                }
                nivel = 0;
            }
            setTimeout(function(){
                $('.ayuda-adm').removeClass('efecto-ayuda');
            },500);
        })
    })
</script>
<form onsubmit="return false">
    <div class="modal fade" id = "nuevo_departamento">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nueva area adscrita</h4>
                </div>
                <div class="modal-body">
                    <label>Introduzca el nombre del area adscrita:</label>
                    <input class="form-control" type="text" id = "nuevo_depa" class="col-md-12" required>
                    <label>Introduzca su contraseña:</label>
                    <input class="form-control" type="password" id = "apss" required>
                    <div class="pedir-contra">
                      <p>Después de crear un area no podrá eliminarla o modificarla</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick = "nuev_depa()" data-dismiss="modal" > Nuevo </button>
                </div>
            </div>
        </div>
    </div>
</form>
<form onsubmit="return false">
    <div class="modal fade" id = "nuevo_puesto">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo puesto</h4>
                </div>
                    <div class="modal-body">
                        <label>Nombre del puesto</label>
                        <input class="form-control" type = "text" id = "npuesto" pattern="[A-Za-z ñ]*" placeholder = "Puesto" class="col-md-12" required>
                        <label>Nivel de mando (seleccione 0 de ser operativo)</label>
                        <select class = "form-control" id= "nivel" required>
                            <option value = "1">1 (Director General)</option>
                            <option value = "2">2 (Gerente)</option>
                            <option value = "3">3 (SubGerente)</option>
                            <option value = "4">4 (Jefe de Departamento)</option>
                            <option value = "0">0 (Operativo)</option>
                        </select>
                        <label for="">Seleccione un area adscrita</label>
                        <script>
                        </script>
                        <select class="form-control" onchange="npdep()" id = "npdepar" required>
                        <?php
                            echo seleccionar_dep(0);
                        ?>
                        </select>
                        <label for="">Seleccione el puesto de mando superior directo</label>
                        <select class="form-control" id = "puesto1" name = "puesto_superior" required>
                        </select>
                        <label>Introduzca su contraseña:</label>
                        <input class="form-control" type="password" id = "ppss" required>
                        <div class="pedir-contra">
                          <p>Después de crear un puesto no podrá eliminarlo o modificarlo</p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick = "nuev_puest()" data-dismiss="modal"> Nuevo </button>
                    </div>
            </div>
        </div>
    </div>
</form>
<form  role="form" onsubmit="return false">
    <div class="modal fade" id = "nueva_evaluacion">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nueva evaluación</h4>
                </div>
                <div class="modal-body col-md-12 col-lg-12">
                    <label class="control-label" for="nom_eva">Nombre de la evaluación:</label>
                     <input class="form-control" pattern="[A-Za-z0-9 ñ]*" id="nNomEva" onchange = "act_crear('n')" placeholder="Nombre evaluacion" type="text" required><br>
                     <label for = "">Seleccione el cuestionario para personal de mando</label>
                     <select class="form-control" id = "nCueM" required>
                     <?php
                        echo seleccionar_cues(0);
                     ?>
                     </select>
                     <label for = "">Seleccione el cuestionario para personal operativo</label>
                     <select class="form-control" id = "nCueO" required>
                     <?php
                        echo seleccionar_cues(0);
                     ?>
                     </select><br>
                     <div class="col-md-12 grupito_fechas">
                         <div class="datepicker-group col-md-6">
                             <label class="col-md-12" for="nFeIn">Fecha de inicio:</label>
                             <input class="sombra calendario col-md-10" type="text" id="nFeIn" onchange = "val_fechas(this.value,this.id)"required onfocus="calendario()">
                             <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                         </div>
                         <div class="datepicker-group col-md-6">
                             <label class="col-md-12" for="nFeFin">Fecha de fin:</label>
                             <input class="sombra calendario col-md-10" type="text" id="nFeFin" onchange = "val_fechas(this.value,this.id)" required onfocus="calendario()">
                             <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                         </div>
                     </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" id = "bnE" class="btn btn-primary disabled">Crear</button>
                </div>
            </div>
        </div>
    </div>
</form>
<form action = "agregar_cuestionario.php" method = "POST">
    <div class="modal fade" id = "nuevo_cuestionario">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nuevo cuestionario</h4>
                </div>
                <div class="modal-body">
                    <label>Introduzca el nombre del cuestionario:</label>
                    <input class="form-control" type="text" name = "titulo" pattern="[A-Za-z0-9 ]*" class="col-md-12" required><br><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class= "btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Crear</button>
                </div>
            </div>
        </div>
    </div>
</form>
