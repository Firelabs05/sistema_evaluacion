<?php
require ('../php/mostrar.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Selección de Personal</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
</head>
<body>
    <?php
    $id_puesto = evaluador();
    ?>
    <?php 
    if(isset($_SESSION['id_eva']))
    {
    ?>
    <section class="personal-evaluar col-md-8 col-md-offset-1 col-sm-12 col-xs-12">
        <h2>Seleccione al personal a evaluar</h2>
        <form action = "../php/comprobar.php" method = "POST">
        <?php
        mostrar_subordinados($id_puesto);
        ?>
        </form>
    </section>
    <aside class="insert-me col-md-1 col-md-offset-1 col-sm-2 col-sm-offset-5 col-xs-2 col-xs-offset-4">
        <div><a class="glyphicon glyphicon-th-list" href="metas.php"></a>
        </div>
    </aside>
    <div class="ayuda"><p>Ayuda</p></div>
    <div class="presentacion">
        <p>Bienvenido al Sistema de Evaluación</p>
        <p>Este es un breve tutorial del funcionamiento del sistema.</p>
        <i class="icon-arrow pri"></i>
    </div>
    <div class="pre-eva">
        <p>En esta sección se le mostraran a todos sus subordinados que tendra que evaluar, ya sean subordinados de mando o subordinados operativos.</p>
        <i class="icon-arrow seg"></i>
    </div>
    <div class="pre-met">
        <p>En este apartado usted podra insertar sus metas y compromisos para cada evaluación.</p>
        <i class="icon-arrow ter"></i>
    </div>
    <div class="pre-per">
        <p>En este apartado al pasar el mouse usted podra ver sus datos y asi mismo podra cerrar sesión.</p>
        <i class="icon-arrow cua"></i>
    </div>
    <div class="pre-fin">
        <p>Usted ah finalizado el tutorial.</p>
        <p>Bienvenido</p>
        <i class="icon-arrow fin-pre"></i>
    </div>
    <div class="logo">
        <img src="../img/lo.png" alt="">
    </div>
    <div class="modal fade" id = "mensaje">
        <div class="modal-dialog">            
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Evaluación</h4>
                </div>
                <div class="modal-body">
                    <p>Usted ya ha realizado esta evaluacion</p>
                    <p>Ya no se puede vovler a realizar</p>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" type="button" class="btn btn-primary">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
    <?php
    }
    else
    {
        mostrar_cal();
    }
    ?>
    <div class="clearfix"></div>
    <script src="../js/jquery-3.1.1.js"></script>
    <script src="../js/main-query.js"></script>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
</body>
</html>