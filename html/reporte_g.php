<!--PROBADA -->
<!DOCTYPE html>
<?php
require ('../php/mostrar.php');
$tipo = $_GET['tipo'];
$eva = mysqli_fetch_assoc(select(buscar_eva($_GET['eva'])));
$cuesm = mysqli_fetch_assoc(select(buscar_columna("NOMBRE_CUES","cuestionarios", "ID_CUESTIONARIO",$eva['CUESTIONARIO_M'])));
$cueso = mysqli_fetch_assoc(select(buscar_columna("NOMBRE_CUES","cuestionarios","ID_CUESTIONARIO",$eva['CUESTIONARIO_O'])));
if ($tipo == 1)
{
    $cues = select(super_reporte($_GET['eva']));
    
}
if ($tipo == 0)
{
    $meta = select(super_reporte_metas($_GET['eva']));
}
?>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Reporte</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/reporte.css">
</head>
<body>
    <header>
        <div>
            <img src= "../img/Logo-SCT-Horizontal.png" width="200px">
        </div>
        <div>
            <h1>Administración Portuaria Integral de Salina Cruz, S.A. de C.V.</h1>
        </div>
        <div>
            <img src= "../img/Logo-API-Salina-Cruz.png" width="100px">
        </div>
    </header>
    <section>
        <h2><?php echo $eva['NOMBRE_EVA'];?></h2>
        <div class="fechas">
            <p>Fecha de Inicio</p>
            <p>Fecha de Fin</p>
            <p><?php echo gregoriano($eva['FECHA_INICIO']);?></p>
            <p><?php echo gregoriano($eva['FECHA_FIN']);?></p>
            <p class="g">Cuestionario de personal de Mando</p>
            <p class="g">Cuestionario del personal Operativo</p>
            <p><?php echo $cuesm['NOMBRE_CUES']; ?></p>
            <p><?php echo $cueso['NOMBRE_CUES']; ?></p>
        </div>
        <h3 class="titu-g"><?php 
            if($tipo == 1)
                echo "Reporte general de los empleados";
            else
                echo "Metas";
            
            ?></h3>
        <div class="general">
            <?php
            if($tipo == 1)
                echo '<p>Nombre del empleado</p>
                      <p>No. de empleado</p>
                      <p>Nombre cuestionario</p>
                      <p>Fecha de realización</p>
                      <p>Calificación Final</p>';
            else
                echo '<p>Nombre del empleado</p>
                      <p>No. de empleado</p>
                      <p>Comentario de la meta</p>
                      <p>Fecha de realización</p>
                      <p>Calificación Final</p>';
            ?>
            <?php
            if($tipo == 1)
            {
                for($contador = 1; $contador <= mysqli_num_rows($cues);$contador++)
                {
                    $c = mysqli_fetch_assoc($cues);
                    echo
                        '<div><p>'.nombre($c).'</p>
                         <p>'.$c['NO_EMPLEADO'].'</p>
                         <p>'.$c['NOMBRE_CUES'].'</p>
                         <p>'.$c['FECHA_REA_CUES'].'</p>
                         <p>'.$c['CALIF_CUES'].'</p></div>';
                }
            }
            if($tipo == 0)
            {
                for($contador = 1; $contador <= mysqli_num_rows($meta);$contador++)
                {
                    $me= mysqli_fetch_assoc($meta);
                    echo
                        '<div><p>'.nombre($me).'.-</p>
                         <p>'.$me['NO_EMPLEADO'].'</p>
                         <p>'.$me['COMENTARIO_META'].'</p>
                         <p>'.$me['FECHA_CALIF'].'</p>
                         <p>'.$me['CALIF_META'].'</p></div>';
                }
            }
            ?>
        </div>
    </section>
</body>
</html>