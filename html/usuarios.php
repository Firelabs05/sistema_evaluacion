<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Area Administrativa - Usuarios</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="https://use.fontawesome.com/76c55944ed.js"></script>
    <script src="../js/jquery-3.1.1.js"></script>
</head>
<body>
    <?php
    require ('nave_admin.php');
    ?>
    <h3 id = "titulo">Usuarios
        <?php
        if(isset($_POST['estado']))
        {
            if($_POST['estado'] == "activo")
                echo " activos";
            if($_POST['estado'] == "baja")
                echo " de baja";
        }
        ?></h3>
    <div class="col-md-12 di-bus">
        <form role="form" class="col-md-6 col-md-offset-3 col-sm-8" onsubmit="busqueda_usu();return false">
            <input type = "hidden" id="uestado" value = "<?php if(isset($_POST['estado']))echo $_POST['estado']; ?>">
            <div class="form-group col-md-12 col-xs-12">
                <input class="col-md-6 bus col-sm-6 col-sm-offset-1 col-xs-7 col-xs-offset-1" id="buscar" placeholder="Introduzca el nombre a buscar" type="text">
                <button class="btn btn-primary col-md-3 col-sm-offset-1" type="submits" onclick="busqueda_usu()"><span class="glyphicon glyphicon-search"></span>Buscar</button>
            </div>
        </form>
    </div>
    <form onsubmit="return false">
        <section class="buscar-usu col-lg-8 col-lg-offset-2 col-md-10 col-sm-10 col-xs-12">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-2 col-xs-12" id = "papa_usuario" >
                <?php
                if(isset($_POST['estado']))
                    mostrar_usuarios($_POST['estado']);
                else
                    mostrar_usuarios("todos");
                ?>

            </div>
        </section>
    </form>
    <form onsubmit="return false">
        <div class="modal fade" id = "modal_confirmar">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Confirmacion</h4>
                    </div>
                    <div class="modal-body">
                        <label>Introduzca su contraseña de administrador para confirmar que se le dara de baja al usuario.</label>
                        <input class="col-md-12 form-control" type = "password" id = "pass" required><br><br><br><br>
                        <input type = "hidden" id= "cuNoemp" >
                        <input type = "hidden" id = "uacti" >
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" data-dismiss="modal" onclick = "b_a_usu()" class="btn btn-primary">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade" id = "modal_modi_usu">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Modificar usuario</h4>
                </div>
                <div class="modal-body" id = "papa_modi">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-mod-usu" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function foco_btn(){
            $('.form-modal-mod > div > div button').removeClass('disabled');
        }
        function quita_btn(){
            $('.form-modal-mod > div > div button').addClass('disabled');
        }
        function foco_btn2(){
            $('.form-modal-tra > div > div button').removeClass('disabled');
            calendario();
        }
        function quita_btn2(){
            $('.form-modal-tra > div > div button').addClass('disabled');
        }
    </script>
    <div class="clearfix"></div>
    <script src="../js/main-query.js"></script>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
</body>
</html>
