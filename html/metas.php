<?php
require ('../php/mostrar.php');
?>
<!DOCTYPE html>
<html>
<head>
   <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Insertar metas</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src = "../js/main.js"></script>
    <script src="../js/jquery-3.1.1.js"></script>
    <script src="../js/main-query.js"></script>
    <script src="https://framework-gb.cdn.gob.mx/assets/scripts/jquery-ui-datepicker.js"></script>
</head>
<body>
    <?php
    evaluador();
    ?>
    <header class="col-md-12">
        <div class="col-md-3 col-sm-12">
            <ol class="breadcrumb">
              <li><a href="seleccion.php"><i class="icon icon-home"></i></a></li>
              <li class="active">Insertar metas</li>
            </ol>
        </div>
        <div class="col-md-6 col-sm-12">
            <h1 class="inser-met-titu">Insertar metas</h1>
        </div>
    </header>
    <section class="col-md-12 col-sm-12 metas">
        <div>
        <?php $metas = mostrar_metas($_SESSION['noemp'],0); ?>
        </div>
        <form name = "formu" action="../php/insertar_meta.php" method="POST" role="form" class="col-md-12 col-sm-12 text-center">
            <input type= "hidden" name = "final" id = "final" value="1">
            <div id="formulario_metas">
                <div id="meta" class="col-md-12">
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        <label class="control-label" for="met">Metas</label>
                        <textarea name="meta" pattern="[A-Za-z ñ0-9]*" class="form-control" placeholder="Inserte sus Metas" rows="5" required></textarea>
                    </div>
                    <div id="compromisos">
                        <div class="col-md-7">
                            <div class="form-group col-md-8 col-sm-6">
                                <label class="control-label" for="comp">Compromisos</label>
                                <textarea name="compromiso1" class="form-control" pattern="[A-Za-z ñ0-9]*"  placeholder="Inserte sus Compromisos" rows="2" required></textarea>
                            </div>
                            <div class="form-group datepicker-group col-md-4 col-sm-6">
                                <label class="control-label col-md-12" for="fecha">Fecha</label>
                                <input class="calendario col-md-12" name="fecha1" type="text" required onfocus="calendario()">
                                <span class="glyphicon glyphicon-calendar cale-usu-1" aria-hidden="true"></span>
                            </div>
                        <button type= "button" class = "btn btn-danger disabled" id = "ra1">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                        </div>
                    </div>
                    <div class="form-group col-md-1 col-sm-2">
                        <button type="button" class="btn btn-default" id = "bcomp" onclick = "mas_comp(this.id)"><span class="glyphicon glyphicon-plus"></span></button>
                    </div>
                </div>
            </div>
            <div class="final col-md-2 col-md-offset-10 col-sm-2 col-sm-offset-5 col-xs-4 col-xs-offset-6">
                <?php
                if ($metas == 4)
                {
                    echo '<button type="button" class="btn btn-primary disabled" data-toggle="modal" data-target= "#no_mas"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>';
                }
                else
                {
                    echo '<button type="submit"class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>';
                }
                ?>
            </div>
        </form>
    </section>
    <div class="modal fade" id = "no_mas">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Metas</h4>
                </div>
                <div class="modal-body">
                    <p>Ya ha ingresado 4 metas, ese es el maximo permitido</p>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary" >Aceptar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id = "no_mas_com">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Compromisos</h4>
                </div>
                <div class="modal-body">
                    <p>Ya ha ingresado 3 compromisos, ese es el maximo permitido</p><br>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary" >Aceptar</button>
                </div>
            </div>
        </div>
    </div>
    <form action = "../php/modi_meta.php" method = "POST">
        <div class="modal fade" id = "modi_meta">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Modificar meta</h4>
                    </div>
                    <div class="modal-body">
                        <label>Introduzca la meta:</label>
                        <textarea class="form-control" pattern="[A-Za-z ñ0-9]*" class="col-md-12" name="meta" value="modi_me" rows="3" required></textarea><br><br>
                        <input type = "hidden" name = "acti" value = "modi_me">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id = "b_modi_meta" name = "id_meta" class="btn btn-primary"> Modificar </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form action = "../php/modi_meta.php" method = "POST">
        <div class="modal fade" id = "modi_comp">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Modificar compromiso</h4>
                    </div>
                    <div class="modal-body">
                        <label>Introduzca el compromiso:</label>
                        <textarea class="form-control" pattern="[A-Za-z ñ0-9]*" class="col-md-12" name="comp" value="modi_me" rows="3" required></textarea><br><br>
                        <input type = "hidden" name = "acti" value = "modi_comp">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id = "b_modi_comp" name = "id_comp" class="btn btn-primary"> Modificar </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form action = "../php/modi_meta.php" method = "POST">
        <div class="modal fade" id = "modi_fe">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Modificar fecha</h4>
                    </div>
                    <div class="modal-body datepicker-group">
                        <label class="col-md-5 col-md-offset-1">Introduzca la fecha:</label>
                        <input class="col-md-4 calendario" type="text" name = "fe_comp" required onfocus="calendario()"><br><br><br>
                        <span class="glyphicon glyphicon-calendar cale-usu" aria-hidden="true"></span>
                        <input type = "hidden" name = "acti" value = "modi_fe">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id = "b_modi_fe" name = "id_comp" class="btn btn-primary"> Modificar </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form action = "../php/modi_meta.php" method = "POST">
        <div class="modal fade" id = "agre_comp">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Nuevo compromiso</h4>
                    </div>
                    <div class="modal-body col-xs-12 datepicker-group me-mo-ca">
                        <label class="col-md-3">Compromiso</label>
                        <textarea pattern="[A-Za-z ñ0-9]*" class="col-md-9" name="comp" rows="3" required></textarea>
                        <label class="col-md-3">Fecha</label>
                        <input class="col-md-7 calendario" type="text" name = "fe_comp" onfocus="calendario()" required>
                        <span class="glyphicon glyphicon-calendar cale-usu" aria-hidden="true"></span>
                        <input type = "hidden" name = "acti" value = "agre_comp">
                    </div><br><br><br><br><br><br><br><br><br>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id = "b_agre_comp" name = "id_meta" class="btn btn-primary">Nuevo</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form action = "../php/modi_meta.php" method = "POST">
        <div class="modal fade" id = "eli_meta">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Eliminar meta</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro que desea eliminar esta meta?</p>
                        <input type = "hidden" name = "acti" value = "eli_meta">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id = "b_eli_me" name = "id_meta" class="btn btn-danger" name ="">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form action = "../php/modi_meta.php" method = "POST">
        <div class="modal fade" id = "eli_comp">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Eliminar compromiso</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro que desea eliminar este compromiso?</p>
                        <input type = "hidden" name = "acti" value = "eli_comp">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" id  = "b_eli_c" name="id_comp" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="logo">
        <img src="../img/lo.png" alt="">
    </div>
    <div class="clearfix"></div>
    <script>
    function calendario(){
        $('.calendario').datepicker({changeYear: true,dateFormat: "yy-mm-dd"});
    }
    </script>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
</body>
</html>
