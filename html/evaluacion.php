<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Area Administrativa - Evaluacion</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="../js/jquery-3.1.1.js"></script>
    <script src="../js/main-query.js"></script>
</head>
<body>
    <?php
    require ('nave_admin.php');
    
    $eva = mysqli_fetch_assoc(select(buscar_eva($_POST['eva'])));
    $m = mysqli_fetch_assoc(select(buscar_columna("NOMBRE_CUES", "cuestionarios", "ID_CUESTIONARIO", $eva['CUESTIONARIO_M'])));
    $o = mysqli_fetch_assoc(select(buscar_columna("NOMBRE_CUES", "cuestionarios", "ID_CUESTIONARIO",$eva['CUESTIONARIO_O'])));
    ?>
    <h3>Visualización</h3>
    <div class="col-md-12 eva-bus">
        <h1 class="text-center"><?php echo $eva['NOMBRE_EVA']; ?></h1>
        <form role="form" class="col-md-6 col-md-offset-3" onsubmit= "busqueda_evados();return false;">
            <div class="form-group col-md-12">
                <input type = "hidden" id = "eIdEva" value = "<?php echo $_POST['eva']; ?>">
                <input class="col-md-7 bus col-sm-6 col-sm-offset-1 col-xs-8" id="buscar" placeholder="Introduzca el nombre a buscar" type="text">
                <button class="btn btn-primary col-md-3 col-sm-offset-1 col-xs-offset-1" type="submit" ><span class="glyphicon glyphicon-search"></span>Buscar</button>
            </div>
        </form>
    </div>
    <form class="boton-repor col-md-12 col-sm-12" action = "reporte_g.php" target = "_blank" method = "GET">
        <input type = "hidden" name = "eva" value = "<?php echo $_POST['eva']; ?>">
        <button class="btn btn-default" type="submit" name = "tipo" value = "1">Reporte Gral. Cuestionarios</button>
        <button class="btn btn-default" type="submit" name = "tipo" value = "0">Reporte Gral. Metas</button>
    </form>
    <form action = "reporte.php" target = "_blank" method = "GET">
        <section class="mostrar-evalu col-md-12">
            <input type = "hidden" name = "eva" value = "<?php echo $_POST['eva']; ?>">
            <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1" id = "papa_evados">
            </div>
        </section>
    </form>
    <div class="dato-eva">
        <h4>Datos evaluación</h4>
        <p>Nombre</p>
        <p><?php echo $eva['NOMBRE_EVA'];?></p>
        <p>Fecha inicio</p>
        <p><?php echo gregoriano($eva['FECHA_INICIO']);?></p>
        <P>Fecha fin</P>
        <p><?php echo gregoriano($eva['FECHA_FIN']);?></p>
        <p>Cuestionario mando</p>
        <p><?php echo $m['NOMBRE_CUES'];?></p>
        <p>Cuestionario operativo</p>
        <p><?php echo $o['NOMBRE_CUES'];?></p>
        <p>Estado</p>
        <p>Finalizada</p>
    </div>
    <div class="clearfix"></div>
    <script type="text/javascript">
        window.addEventListener("load",cargar_evados("<?php echo $_POST['eva']; ?>"),false)
    </script>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
</body>
</html>