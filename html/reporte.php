<!--PROBADA -->
<!DOCTYPE html>
<?php
require ('../php/mostrar.php');
$tipo = substr($_GET['noemp'],0,1);
$noemp = substr($_GET['noemp'],1);
if ($tipo == 1)
{
    $eva = mysqli_fetch_assoc(select(tabla_reporte($_GET['eva'], $noemp)));
    $cues = select(tabla_preguntas($eva['ID_RESULTADO_CUES']));
    $evador = mysqli_fetch_assoc(select(tabla_evalor($eva['NO_EMP_EVA'])));
}
if ($tipo == 0)
{
    $eva = mysqli_fetch_assoc(select(tabla_metas($_GET['eva'], $noemp)));
    $me = select(tabla_r_metas($eva['ID_RESULTADO_META']));
    $evador = mysqli_fetch_assoc(select(tabla_evalor($eva['NO_EMP_EVA'])));
}
?>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Reporte</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/reporte.css">
</head>
<body>
    <header>
        <div>
            <img src= "../img/Logo-SCT-Horizontal.png" width="200px">
        </div>
        <div>
            <h1>Administración Portuaria Integral de Salina Cruz, S.A. de C.V.</h1>
        </div>
        <div>
            <img src= "../img/Logo-API-Salina-Cruz.png" width="100px">
        </div>
    </header>
    <section>
        <h2><?php echo $eva['NOMBRE_EVA'];?></h2>
        <div class="fechas">
            <p>Fecha de Inicio</p>
            <p>Fecha de Fin</p>
            <p><?php echo gregoriano($eva['FECHA_INICIO']);?></p>
            <p><?php echo gregoriano($eva['FECHA_FIN']);?></p>
        </div>
        <div class="datos">
            <p>Datos del Evaluado</p>
            <div>
                <p>Nombre:</p>
                <p><?php echo nombre($eva);?></p>
                <p>CURP:</p>
                <p><?php echo $eva['CURP'];?></p>
                <p>RFC:</p>
                <p><?php echo $eva['RFC'];?></p>
                <p>No. Empleado:</p>
                <p><?php echo $noemp;?></p>
                <p>Adscrito a:</p>
                <p><?php echo $eva['DEPARTAMENTO'];?></p>
                <p>Puesto:</p>
                <p><?php echo $eva['PUESTO'];?></p>
                <p>Tipo de Empleado:</p>
                <p><?php
                        if ($eva['TIPO_EMP']== '1')
                        {
                            echo "Mando";
                        }
                        else
                        {
                            echo "Operativo";
                        }
                        ?>
                </p>

            </div>
        </div>
        <h3 class="titu-cues"><?php
            if($tipo == 1)
                echo $eva['NOMBRE_CUES'];
            else
                echo "Metas";

            ?></h3>
        <div class="cuestionario">
            <?php
            if($tipo == 1)
                echo '<p>Pregunta</p>';
            else
                echo '<p>Metas</p>';
            ?>
            <p>Calificacion</p>
            <?php
            if($tipo == 1)
            {
                for($contador = 1; $contador <= mysqli_num_rows($cues);$contador++)
                {
                    $pre= mysqli_fetch_assoc($cues);
                    echo
                        '<div><p>'.$contador.'.-</p>
                         <p>'.$pre['PREGUNTA_CUES'].'</p>
                         <p>'.$pre['RESULTADO'].'</p></div>';
                }
            }
            if($tipo == 0)
            {
                for($contador = 1; $contador <= mysqli_num_rows($me);$contador++)
                {
                    $meta= mysqli_fetch_assoc($me);
                    echo
                        '<div><p>'.$contador.'.-</p>
                         <p>'.$meta['META'].'</p>
                         <p>'.$meta['CALIF_META'].'</p></div>';
                }
            }
            ?>
        </div>
        <table class="table table-bordered">
            <tr>
                <td>
                    <p>Calificacion Final:</p>
                </td>
                <td>
                    <p><?php
                        if($tipo == 1)
                            echo $eva['CALIF_CUES'];
                        else
                            echo $eva['CALIF_META'];
                        ?></p>
                </td>
                <td>
                    <p>Fecha de realizacion:</p>
                </td>
                <td>
                    <p><?php
                        if($tipo == 1)
                            echo gregoriano($eva['FECHA_REA_CUES']);
                        else
                            echo gregoriano($eva['FECHA_CALIF']);
                        ?></p>
                </td>
            </tr>
        </table>
        <div class="datos">
            <p>Datos del Evaluador</p>
            <div>
                <p>Nombre:</p>
                <p><?php echo nombre($evador);?></p>
                <p>CURP:</p>
                <p><?php echo $eva['CURP'];?></p>
                <p>RFC:</p>
                <p><?php echo $eva['RFC'];?></p>
                <p>No. Empleado:</p>
                <p><?php echo $eva['NO_EMP_EVA'];?></p>
                <p>Adscrito a:</p>
                <p><?php echo $eva['DEPARTAMENTO'];?></p>
                <p>Puesto:</p>
                <p><?php echo $eva['PUESTO'];?></p>
                <p>Tipo de Empleado:</p>
                <p>Mando</p>

            </div>
        </div>
    </section>
</body>
</html>
