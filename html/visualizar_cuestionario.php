<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Administrador</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="../js/jquery-3.1.1.js"></script>
    <script src="../js/main-query.js"></script>
</head>
<body>
    <?php
    require ('nave_admin.php');
    ?>
    <?php
    if(substr($_POST['tipo'], 0,1) == 0)
    {
        echo '<h3>Modificar Cuestionario</h3>';
    }
    else
    {
        echo '<h3>Visualizar cuestionario</h3>';
    }
    ?>
    
    <h1 class="col-md-12 text-center visu-pre"><?php 
        $id_cues = substr($_POST['tipo'], 1);
        $nombre = mysqli_fetch_assoc(select(buscar_columna("NOMBRE_CUES", "cuestionarios","ID_CUESTIONARIO",$id_cues)));
        echo $nombre['NOMBRE_CUES'];
        ?></h1>
    <form>
    <input type = "hidden" id= "papa_cuest" value = "<?php echo $_POST['tipo'];?>">
    </form>
    <section class="buscar-usu col-lg-8 col-lg-offset-2 col-md-10 col-sm-10 col-xs-12">
        <form class="col-lg-10 col-lg-offset-1 col-md-12" id ="papa_pregunta" >
        </form>
    </section> 
    <form onsubmit="return false">
        <div class="modal fade" id = "modal_modificar">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Modificar</h4>
                    </div>
                    <div class="modal-body" id = "papa_modi_preg">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button"  data-dismiss="modal" onclick = "modi_preg()" class="btn btn-primary">Modificar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form onsubmit="return false" >
        <div class="modal fade" id = "modal_eliminar">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Eliminar</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro que desea eliminar la pregunta?</p><br>
                        <input type ="hidden" id="eIdPreg">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" data-dismiss="modal" onclick = "eli_preg()" class="btn btn-danger">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form onsubmit="return false">
        <div class="modal fade" id = "modal_nuevo">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Nueva pregunta</h4>
                    </div>
                    <div class="modal-body">
                        <p>Introduzca la pregunta</p>
                        <textarea class="col-md-12" id ="nPreg" rows="3"></textarea><br><br><br><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" onclick = "nuev_preg()" class="btn btn-primary" data-dismiss="modal">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
    <script>
        window.addEventListener("load",cargar_pregun('<?php echo $_POST['tipo']; ?>'),false);
    </script>
    <script>
        $(window).scroll(function(){
            if($(this).scrollTop() > $('footer').offset().top - 700){
                $('.fin-modif-cues').css({'opacity':'0'});
            }else{
                $('.fin-modif-cues').removeAttr('style');
            }
        })
    </script>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
</body>
</html>