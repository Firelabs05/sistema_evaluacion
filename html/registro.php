<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Registro</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="https://use.fontawesome.com/76c55944ed.js"></script>
    <script src="../js/jquery-3.1.1.js"></script>
    <script src="../js/main-query.js"></script>
</head>
<body>
    <?php
    require ('nave_admin.php');
    ?>
    <h3>Nuevo Usuario</h3>
    <form role="form" class="col-lg-6 col-lg-offset-3 col-md-12 formu formu-reg col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1" name = "formu_reg_usu" onsubmit="registrar_usu();return false">
        <div>
            <p>Datos de usuario</p>
            <div class="form-group col-md-12 col-sm-12 text-center">
                <label class="control-label" for="usuario">Usuario</label>
                <input class="form-control" id="usuario" pattern="[A-Za-z0-9]*" placeholder="Usuario" type="text" onblur = "valida_registro(this.id,'r_usuario')" required>
                <i class="fa fa-1x fa-fw"></i>
            </div>
            <div class="form-group col-md-12 col-sm-12 text-center">
                <div class="col-md-6 form-group">
                    <label class="control-label" for="contrasena">Contraseña</label>
                    <input class="form-control" id="contrasena" placeholder="Contraseña (Minimo 6 caracteres)." type="password" required>
                </div>
                <div class="col-md-6 form-group">
                    <label class="control-label" for="contrasena">Confirmar contraseña</label>
                    <input class="form-control" placeholder="Confirmar Contraseña" type="password" onblur ="confirmar_pass(this.value)" required>
                    <i class="fa fa-1x fa-fw"></i>
                </div>
            </div>
        </div>
        <div>
            <p>Datos personales</p>
            <div class="form-group col-md-4 col-sm-6 text-center">
                <label class="control-label" for="nombre">Nombre(s)</label>
                <input class="form-control" id="nombre" pattern="[A-Za-z ñ]*" placeholder="Nombre" type="text" required>
            </div>
            <div class="form-group col-md-4 col-sm-6 text-center">
                <label class="control-label" for="apellidoP">Apellido paterno</label>
                <input class="form-control" id="apellidoP" pattern="[A-Za-z ñ]*" placeholder="Apellido Paterno" type="text" required>
            </div>
            <div class="form-group col-md-4 col-sm-6 text-center">
                <label class="control-label" for="apellidoM">Apellido materno</label>
                <input class="form-control" id="apellidoM" pattern="[A-Za-z ñ]*" placeholder="Apellido Materno" type="text" required>
            </div>
            <div class="form-group col-md-4 col-sm-6 text-center">
                <label class="control-label" for="rfc">RFC</label>
                <input class="form-control" id="rfc" pattern="[A-Za-z0-9ñ]{13,13}" onblur = "valida_registro(this.id,'r_rfc')" placeholder="RFC" type="text" required>
                <i class="fa fa-1x fa-fw"></i>
            </div>
            <div class="form-group col-md-5 col-sm-6 text-center">
                <label class="control-label" for="curp">CURP</label>
                <input class="form-control" onblur = "valida_registro(this.id,'r_curp')" id="curp" pattern="[A-Za-z0-9ñ]{18,18}"placeholder="CURP" type="text" required>
                <i class="fa fa-1x fa-fw"></i>
            </div>
            <div class="form-group col-md-3 col-sm-6 text-center">
                <label class="control-label" for="noemp">No. empleado</label>
                <input class="form-control" id="noemp" pattern="[A-Za-z0-9]*" onblur="valida_registro(this.id,'r_noemp')" placeholder="Numero" type="text" required>
                <i class="fa fa-1x fa-fw"></i>
            </div>
            <div class="form-group col-md-6 col-sm-6 text-center">
                <label class="control-label" for="correo">Correo electrónico</label>
                <input class="form-control" id="correo"  placeholder="Correo Electronico" type="email" required>
            </div>
            <div class="form-group datepicker-group col-md-6 col-sm-6 text-center">
                <label class="control-label col-md-12" for="fecha-ingre">Fecha de ingreso:</label>
                <input class="sombra calendario" type="text" id="fechaingre" required onfocus="calendario()">
                <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
            </div>
        </div>
        <div>
            <p>Datos de trabajo</p>
            <div class="form-group col-md-12 col-sm-12">
               <div class="col-md-6 col-sm-6 text-center">
                   <label for="">Area adscrito</label>
                   <select class="form-control" name = "departamento" id = "dep" onchange="valida_registro(this.id,'puesto')">
                    <?php
                      echo seleccionar_dep(0);
                    ?>
                   </select>
               </div>
               <div class="col-md-6 col-sm-6 text-center">
                   <label for="">Puesto</label>
                   <select class="form-control" id = "puesto2">
                   </select>
               </div>
            </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button class="btn btn-primary col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2" type="submit">Guardar Datos</button>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js">
    </script>
    <script>
        window.onload = valida_registro("dep","puesto");
    </script>
</body>
</html>
