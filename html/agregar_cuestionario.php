<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Agregar-Cuestionario</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="../js/jquery-3.1.1.js"></script>
    <script src="../js/main.js"></script>
</head>
<body>
    <?php
    require ('nave_admin.php');
    ?>
    <h3>Nuevo Cuestionario</h3>
    <section class="col-md-8 col-md-offset-3 cre-cue">
        <div class="cuestionario-nuevo-titu">
            <h1 id="titulo" class="col-md-12 text-center"><?php echo $_POST['titulo']; ?></h1>
            <button type = "button" value = "<?php echo $_POST['titulo']; ?>" onclick = "nombre_cues(this.value)" class = "btn btn-primary" data-toggle="modal" data-target= "#modal_titulo"><span class = "glyphicon glyphicon-pencil"></span></button>
        </div>
        <div class="form-group">
            <form class="col-md-12" action="../php/insertar_cuestionario.php" role="form" method="POST" id="form_cuestionario">
                <input type = "hidden" name = "final" id = "final">
                <div class="col-md-12">
                    <input type = "hidden" id = "nom_cues" name = "nom_cues" value = "<?php echo $_POST['titulo']; ?>">
                    <table id="cuadro_pregunta" class="col-md-12">
                    </table>
                </div>
                <div id="agregar_pregunta" class="col-md-12 col-sm-12">
                    <label class="col-md-12 col-lg-11 col-sm-10 col-sm-offset-1">Introduzca la pregunta (No es necesario agregar el numero de pregunta)</label>
                    <textarea id="pregunta" class="col-md-10 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1"  rows="3"></textarea>
                    <button type="button" class="btn btn-primary col-md-3 col-md-offset-2" onclick = "agregar_pregunta()" >Agregar</button>
                    <button type = "submit" class="btn btn-danger col-md-3 col-md-offset-2" id="boton-agre-fin" >Finalizar</button>
                </div>
            </form>
        </div>
    </section>
    <div class="modal fade" id = "modal_pregunta">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Pregunta</h4>
                </div>
                <div class="modal-body">
                    <label>Modifique la pregunta:</label>
                    <textarea class="col-md-12" id="mPregunta" rows="3"></textarea><br><br><br><br><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id = "modi" onclick = "modificar_pregunta(this.value)" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id = "modal_titulo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Nombre cuestionario</h4>
                </div>
                <div class="modal-body">
                    <label>Modifique el nombre del cuestionario:</label>
                    <input rows="4" type="text" id="mTitulo" class="col-md-12"><br><br><br><br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id = "modi" onclick = "modificar_titulo()" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
</body>
</html>
