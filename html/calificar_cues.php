<?php
require ('../php/mostrar.php');
$subor = mysqli_fetch_assoc(select(subordinado($_GET['no_emp'])));
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Calificar cuestionario</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="../js/jquery-3.1.1.js"></script>
</head>
<body>
    <?php
    evaluador();
    if ($subor['TIPO_EMP']== '1')
    {
        $cues = mysqli_fetch_assoc(select(selec_cues($_SESSION['id_eva'],"M")));
        $tipo = "Mando";
    }
    else
    {
        $cues = mysqli_fetch_assoc(select(selec_cues($_SESSION['id_eva'],"O")));
        $tipo = "Operativo";
    }
    ?>
    <header class="col-md-12">
        <div class="col-md-3 col-sm-12">
            <ol class="breadcrumb">
              <li><a href="seleccion.php"><i class="icon icon-home"></i></a></li>
              <li class="active">Evaluación</li>
            </ol>
        </div>
        <div class="col-md-6 col-sm-12">
            <h2 class="text-center titu-cali-cues">
            <?php echo $cues['NOMBRE_CUES'];?>
            </h2>
        </div>
    </header>
    <div class="datos-evaluado col-md-3 col-sm-12">
        <h4 class="col-md-12 text-center">Datos del evaluado</h4>
        <div>
            <p class="col-md-12">Nombre:</p>
            <p class="col-md-12"><?php echo nombre($subor);?></p>
        </div>
        <div>
            <p class="col-md-12">Adscrito a:</p>
            <p class="col-md-12"><?php echo $subor['DEPARTAMENTO'];?></p>
        </div>
        <div>
            <p class="col-md-12">Puesto:</p>
            <p class="col-md-12"><?php echo $subor['PUESTO'];?></p>
        </div>
        <div>
            <p class="col-md-12">No. empleado:</p>
            <p class="col-md-12"><?php echo $_GET['no_emp'];?></p>
        </div>
        <div>
            <p class="col-md-12">Tipo de empleado:</p>
            <p class="col-md-12">
                <?php echo $tipo; ?>
            </p>
        </div>
    </div>
    <form action="../php/agregar_calificacion.php" id="califi-cues" class="col-md-9 col-md-offset-3 col-sm-12" method = "POST">
        <input type = "hidden" name = "subor" value = "<?php echo $_GET['no_emp']; ?>">
        <input type = "hidden" name = "evaluador" value = "<?php echo $_SESSION['noemp']; ?>">
        <input type = "hidden" name = "id_pue" value = "<?php echo $subor['ID_PUESTO']; ?>">
        <input type = "hidden" name = "id_eva" value = "<?php echo $_SESSION['id_eva']; ?>">
        <input type = "hidden" name = "id_cues" value = " <?php echo $cues['ID_CUESTIONARIO']; ?>">
        <input type = "hidden" name ="tipo" value = "<?php echo $subor['TIPO_EMP']; ?>">
        <?php
        mostrar_pregunta($cues['ID_CUESTIONARIO'], 1, "C");
        ?>
        <button type ="submit" class = "btn btn-primary">Finalizar</button>
    </form>
    <div class="logo">
        <img src="../img/lo.png" alt="">
    </div>
    <div class="clearfix"></div>
    <script src="../js/main-query.js"></script>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
</body>
</html>
