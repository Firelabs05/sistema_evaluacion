<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
    <meta charset="UTF-8">
    <title>Area Administrativa</title>
    <link rel="stylesheet" href="https://framework-gb.cdn.gob.mx/qa/assets/styles/main.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="../js/jquery-3.1.1.js"></script>
    <script src = "../js/main-query.js"></script>
</head>
<body>
    <?php
    require ('nave_admin.php');
    ?>
    <h3 id = "tit" >Evaluaciones</h3>
    <section class="buscar-usu col-lg-8 col-lg-offset-2 col-md-10 col-sm-10 col-xs-12">
        <form  class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-2 col-xs-12" action="evaluacion.php" method="POST" id = "form_mos_eva">
        </form>
    </section>  
    <form role="form" onsubmit="return false">
        <div class="modal fade" id = "modificar_evaluacion">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Modificar evaluacion</h4>
                    </div>
                    <div class="modal-body col-md-12" id = "papa_modi">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <a href = "#tit"><button type = "button" class="btn btn-primary" id = "bmE" data-dismiss="modal" >Modificar</button></a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form role = "form" onsubmit="return false">
        <div class="modal fade" id = "eliminar">
            <div class="modal-dialog">            
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Confirmacion</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿Esta seguro que desea eliminar esta evaluacion?</p>
                        <p>¿Seguro que desea continuar?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button id = "eva_eli" data-dismiss = "modal" class="btn btn-danger" onclick = "eli_eva(this.value)">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="modal fade" id = "modal_detalles">
        <div class="modal-dialog">            
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detalles de la Evaluación</h4>
                </div>
                <div class="modal-body">
                    <div class="grafic">
                        <canvas id="papa_grafica"></canvas>
                    </div>
                    <div id = "papa_detalles"></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <script>
       window.onload = cargar_evas;  
       document.getElementById("bmE").addEventListener("click",val_mEva,false);
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.min.js"></script>
    <script src="https://framework-gb.cdn.gob.mx/qa/gobmx.js"></script>
</body>
</html>